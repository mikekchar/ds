# Chibi Shiro

Originally called "Dwarf Startup".  A Dwarf Fortress like game written
in Rust.  I think the best way to think of this is to think, "What if
I wrote Dwarf Fortress from scratch, but made different choices along
the way".  The idea is *not* to make the same game, but I'm using it
as a starting point to help me explore my own ideas. I'm still at a
very early stage of development, so expect everything to change
over time.

There is no playable game yet, although I'm currently working on
a series of mini games.  Over time I hope the mini games will meld
together into something representative of the final goal of the game.

## Screenshot

![](ds_demo.gif)

## Contact me

You can contact me at mikekchar @ the obvious google mail company.

## Streaming News

(Messages for the stream will appear here from time to time)

I got busy at work again :-(  I *will* be starting the stream up
again in the new year.

Current branch: fun (warning: broken)

### Where to see the stream

I'm currently streaming the development of this game on Twitch
most in the morning Japan time (GMT+9).  Feel free
to drop by: https://www.twitch.tv/urouroniwa

Twitch keeps previous videos for 2 weeks: https://www.twitch.tv/urouroniwa/videos

Keep in mind that I'm a relative novice with respect to Rust
development and game development in general, so at the moment the pace
is slow ;-) Hints and suggestions are always welcome, but I reserve
the right to make weird choices for no apparent reason (or because
it's !FUN!).

### Normal Stream Schedule

I don't tend to keep exact schedules, but usually I start
somewhere between 8:00 and 10:00 Tokyo time.  I try to stream
for about 2 hours a day, but it can be anywhere from 1-3 hours.
Very occasionally I stream making cheese.

    |---------------+-------------------+-------|
    | Location      | Day               |  Time |
    |---------------+-------------------+-------|
    | Tokyo         | Monday - Friday   | 09:00 |
    | London        | Monday - Friday   | 00:00 |
    | New York      | Sunday - Thursday | 19:00 |
    | San Francisco | Sunday - Thursday | 16:00 |
    |---------------+-------------------+-------|

## Current Status

When you first run the game, you are presented with a window
containing 3 sections: a map, a menu and at the bottom a display
describing what is under the cursor.  The cursor is always in the
center of the screen and by default looks like a semi-transparent
white square.  You can move the cursor around the map using an
extended set of the VI keys, as described in the menu pane.

By default the map is rendered using a graphical tile set.  The cursor
starts out in a room.  The room is empty (although there appears to be
mushrooms growing in the lower right and corner -- they are decorative
only).  When you move the cursor, the cursor remains in the center of
the screen and the entire map scrolls under it.  In the upper right
hand corner of the window you will see the current number of frames
per second (limited to 60 FPS).  In the upper right and corner you
will see a clock.  The clock counts the number of "game ticks" that
have elapsed.  At the start of the game, the clock is paused.

You can unpause the game by pressing the space bar.  If you do so, you
will notice the clock increasing.  If you press space again, the clock
will pause.  If you allow somewhere around 9 seconds elapse, a dwarf
will appear in the room.  He will wander randomly around the room.  As
time passes, he will get progressively thirsty, which you can see
because the tile he is standing on will get more and more red.  After
about 10 seconds he will die of thirst, leaving a corpse.  The game is
over.  Currently there is nothing else you can do.

While the default presentation is a tile set, you can also display an
ASCII representation of the map, etc.  The following keybindings are
present by default:

  - a: toggle ASCII characters on and off
  - d: toggle dwarf foreground graphics rendering on and off
  - c: toggle background tile terrain graphics on and off
  - b: toggle ASCII background fill colours on and off
  - g: toggle a grid overlay on and off

### Known issues

- Keypresses are not auto-repeated when held down.
- Moving by clicking on the mouse is jarring due to lack of animation

## TODO

Please see the TODO.org file for more information.  In the `journal`
directory there are specific TODOs for each development branch that
is active.  Usually these development branches are up to date with
master and sometimes they are exactly equal to master.  I'm currently
leaving them there to separate specific avenues of development.

# How to build

```
cargo build
```

# How to run
```
cargo run
```

## How to run the tests

```
cargo test

```

## Contributing

Suggestions, patches, pull requests, etc are very welcome.  There are
a few things you should keep in mind, though.

  - I will not always incorporate every change submitted.  Sometimes this
    will be a strange choice, but in my personal projects I'm often looking
    to try out weird ideas.  Please don't take it personally if I decide
    to go another way.  The code is free software specifically so that you
    can always incorporate your own ideas in a fork of the software.

  - I don't mind forks at all.  If you have an idea that you want to try out,
    or if you want to base your own game on the work I've started, you are
    heavily encouraged to do so.  I will cheer you on!  The only constraint
    is that the license is not changed.  The base rule is: "My fork is mine.
    Your fork is yours."

  - My goal is to build a project where everyone can feel safe contributing
    without worrying that the rules will change under them.  Technically I
    have the most power because I own the copyright for most of the code,
    however I wish to act in such a way that everyone can use the code
    with the same rights *and* restrictions -- an even playing field.

  - Before making *any* changes or suggestions, please read the
    LICENSE file.  If you don't agree with the license, make sure that
    you do not distribute any changes to the code you are making.
    I've written some reasoning behind my choice of the AGPL with this
    code at the bottom of this document, but I don't expect everyone
    to agree.  That's fine, but the distribution of this software is
    dependent upon the license.

  - All copyright for changes to this code belong with their authors.  I'm
    happy to accept changes and you will own the copyright for that change.
    Over time we will have a mosaic of changes from a variety of authors
    and it will be impossible to say "This copyright belongs to me".  This is
    the best way I can think of to guarantee that I will *never* change the
    license (except for the "and later versions" exception in the license).

  - This project has no published code of conduct.  In keeping with the idea
    of "My fork is mine", the code of conduct on the stream, in discussions
    via email, etc, etc will be guided by my choices.  I will try my best to
    be accommodating because I would like people to enjoy contributing to
    the project.  However, if I fail, your response should be to fork the
    project and show, through your actions, where I'm going wrong.

  - One of the goals of this project is for me to explore the idea of making
    money from a free software project.  If you are opposed to this, please
    do not contribute to the project.  Similarly, keep in mind that I am
    happy if you, in turn, make money from this project.  I will cheer you
    on!  Just keep in mind that you must follow the license.

To sum up, I don't want people to invest in something they later
regret.  The idea is that if you contribute, then your version of the
code is yours just as much as my version is mine.  Your ideas are just
as valid as my ideas and I want you to explore them to your hearts
content.  I would like you to feel the same way about what I'm doing.
The license imposes restrictions on what we can do with our code, so
make sure you are OK with that *before* starting on your journey.

## License

AGPL 3.0.  Please see the accompanying LICENSE.md file.

The DejaVuSerif.ttf font file is under this license:
https://dejavu-fonts.github.io/License.html
Eventually I'll find a better way to reference asset licenses.

The amazing StoneFloor.png tiles made by DriftonAloft is under
the AGPL 3.0 license.

### Why AGPL?

My goal is to create a free software game that I can get paid for.  I
think that copyleft licenses are good in that regard because it forces
all participants (including myself) to play by the same rules.  I'm
committed to writing only free software, but potential competitors are
not necessarily constrained by my choices.  I would prefer not to
compete against someone using my code that has more options than I do.
From that perspective, copyleft licenses are ideal for commercial
endeavours.

You might be wondering, why free software in the first place?  Isn't
that making life difficult for myself?  Yes, it is.  However, I've
gained a lot from the free software I've received.  Not only has it
been easy on my pocket book, but I've been able to learn a huge amount
simply by having the freedom to inspect and work on the software.

There is something about a game that just invites exploration and
customisation.  In some ways it's the perfect candidate for software
freedom.  On the other hand, most people believe that game software
simply can not be sold unless the base source code is witheld from
players.  I can understand that point of view, but I'd love it if
someone would find a way around it.

This project is as much about trying to find that way as it is about
trying to write a game.  I think it's pretty unlikely that I will
succeed, but I will have no chance of success if I don't try.  What's
the worst that can happen?
