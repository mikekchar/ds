# Dwarfy Memory

Allow the dwarf to remember water locations that it has stood next to.
Make it obvious that the dwarf has remembered a location.
Add highlights remembered locations.

When thirsty, path to the nearest remembered location.
Make it obvious that the dwarf is pathing to a remembered location.
Change the target color if the dwarf is pathing to a remembered location.

Expire memories over time (How does it do this?)

Add time scaling to the UI

## TODO

  - TODO: Add a key to follow the dwarf (toggle)
    - DONE: Add the key binding (maybe f)
    - DONE: It follows the dwarf when updating
    - DONE: It stops following the dwarf when moving the cursor
    - TODO: Make a new cursor when following
    - TODO: Continue following if using a menu
  - TODO: BUG Cancel impossible jobs when dwarf arrives at location
  - TODO: Highlight remembered location
  - TODO: Properly forget water locations after drinking the water
  - TODO: Initialize map structures using builder pattern
          new() should give you an empty layer/map
          User builder pattern to add on extras (like rooms, caverns, pools, etc)
  - TODO: Wrap the text in the panels if they don't fit
  - DONE: Make the dwarf thirsty earlier
  - DONE: Change target colour if the dwarf is pathing to a remembered location.
  - DONE: Put cause of death in the right place
  - DONE: Build a vector of locations
      When the dwarf is standing next to water, the water's location is pushed onto the vector
  - DONE: When the dwarf is thirsty and not next to water, path to remembered water
  - DONE: When the dwarf occasionally removes water from water memory
          If they drink the water, or notice that it is gone
  - DONE: Water memory should expire over time (they forget)
  - DONE: Make thirst levels an enum
          Have a single function that returns the enum
          Use a case statement to determine the state of the dwarf
  - DONE: Refactor so that the hydration levels are easy understand
