# Fun

  - Get to know the code base again
  - Clean it up
  - Write better tests

# TODO
include_str! for defaults that get overridden by files at some xdg path
museun
: const DEFAULT_CONFIG : &'static str = include_str!("config.toml");
museun
: then on start up, check for the user provided config or fallback to the default
museun
: and then maybe write out DEFAULT_CONFIG if they request it)
include_str "bakes" the string into the binary at build-time
include_bytes for the default tileset

  - Remove accessors from config
    - Write better tests
  - Organize AppConfig
  - Remove manifest-dir-error from config and simple get it from the locale
  - Hard code UI elements rather than having them in the config
  - Load fonts from OS rather directly from files
  - Remove Stum Macros
  - Add a test for set_default_filter(&mut ctx, Nearest) in main
