# Challenge of the Water

In this challenge, a dwarf will appear in a room.  The dwarf is alone.
Over time, the dwarf gets dehydrated.  When the dwarf gets too dehydrated
they die.

Our goal is to help the dwarf survive for as long as possible (or until
we get bored).  To survive, the dwarf must find water.  They can explore
the initial room.  If there is water, they can drink it.  If not, they need
to dig their way out of the room to find water.  The dwarf will explore
their surroundings and dig if they need to.

The user can influence the dwarfs decisions. The user is playing a role in
the game.  They are some kind of supernatural being.  They can't influence
the game directly. Instead, they need to convince some dwarfs to sign a
contract with them.  Once the contract is signed, the player gains energy
from the dwarf.  The player can give energy to the dwarf and the dwarf can
use it to do whatever they want to do (Later game play will allow the player
to communicate plans to contracted dwarfs -- the dwarfs may or may not
follow those plans to the letter).

## Todo
  * DONE Precalculate the position of the next dwarf
         put the cursor there at the start of the game
  * DONE Add a key to zoom to the dwarf (v)
  * DONE The dwarf can drink the water and it disappears
         If the dwarf is next to water, he drink.
    ** DONE If the dwarf drinks the water, the Pool changes to a Floor
  * DONE BUG: FPS only updates when not paused
  * DONE Refactor dwarf planning code
  * DONE Create an iterator for A*
  * DONE Flood Fill function
  *      Flood fill from a pathable tile.  Assign an ID (kind of like a color) to
  *      the tiles that are connected.
  * TODO Add a memory for the dwarf
  *      The dwarf will remember where water is when he stands next to it.
  * TODO If the dwarf drinks water, or sees it disappears, he removes it from his memory
  * TODO If the dwarf is thirsty, he paths to remembered water
  * TODO If the dwarf does not know of any water, he wanders randomly
  * TODO Chunk the map in rectangular areas
  * TODO Within a chunk, create a cache containing pathability.
         Start at a pathable location, flood fill as much as you can, each of
         those tiles a pathability ID,  Repeat for any pathable tile that doesn't
         have an id.
  * TODO Bust the cache when changing pathability in a chunk.
  * TODO When determining if two locations are pathable, compare the pathability ID
  * TODO Chunk the map according to DA's great idea
         - Chunk the map regularly
         - Flood fill to find all the places where the chunk can path to another chunk
         - Hierachically path the chunks before pathing inside the chunk
           - The dwarfs path will have a chunk path and a path inside the existing chunk only.
  * TODO Only allow the dwarf to wander to places they can path to
    * TODO Create a goal list
    * TODO Try to path to something on the goal list
    * TODO If you can't path to something on the goal list, path to something close
    * TODO Sleep if you've tried to path several times and failed
           Use a binary backoff strategy with a give up threshold
