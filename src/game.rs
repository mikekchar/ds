//! # DS Game Module
//!
//! This is the start of a reuse library for
//! making the game.

pub mod a_star;
pub mod chunk;
pub mod client;
pub mod command;
pub mod config;
pub mod coord;
pub mod dwarf;
pub mod event;
pub mod flood_fill;
pub mod grid;
pub mod layer;
pub mod location;
pub mod map;
pub mod player;
pub mod rng;
pub mod symbol;
pub mod trail;
pub mod water_map;
pub mod zone_map;

use ggez::{
    conf::{ModuleConf, WindowMode, WindowSetup},
    error::GameError,
    event::EventLoop,
    Context,
    ContextBuilder,
    GameResult,
};
use std::{env, path};

use coord::LossyFrom;

/// Initialise the game.
pub fn init(game_id: &'static str) -> GameResult<(Context, EventLoop<()>)> {
    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        let dimensions =
            config::Config::instance().app_config.window_dimensions;
        let window_title = &config::Config::instance().app_config.window_title;
        ContextBuilder::new(game_id, "mikekchar")
            .window_setup(WindowSetup::default().title(window_title))
            .window_mode(
                WindowMode::default()
                    .dimensions(
                        f32::lossy_from(dimensions.0),
                        f32::lossy_from(dimensions.1),
                    )
                    .resizable(true),
            )
            .modules(ModuleConf {
                gamepad: false,
                audio: false,
            })
            .add_resource_path(&path)
            .build()
    } else {
        Err(GameError::ConfigError(
            config::Config::instance()
                .app_config
                .manifest_dir_error
                .clone(),
        ))
    }
}

#[cfg(test)]
pub mod test_setup;
