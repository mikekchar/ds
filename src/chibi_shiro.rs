//! The interactive parts of the game.  Relies on the game module.

use ggez::{
    event::{self, KeyCode, KeyMods, MouseButton},
    graphics,
    Context,
    GameResult,
};

use crate::game::{
    self,
    client::{
        self,
        context,
        event::Event as ClientEvent,
        point::{self, Point},
        rect::Rect,
        tile::ElementType,
        time::{self, seconds, Time},
        window::Window,
    },
    command::Command,
    config::{app_config::AppConfig, Config},
    coord::LossyFrom,
    dwarf::{Conditions, Locations, Population},
    event::Event as GameEvent,
    layer::{self, Layer},
    location::Direction,
    player::Player,
};
use std::time::Duration;

/// Holds the main state of the game.
pub struct MainState {
    dirty: bool,
    client_events: Vec<client::event::Event>,
    window: Window,
    time: Time,
    layer: Layer,
    population: Population,
    player: Player,
    long_click_start: Option<Duration>,
    commands: Vec<Command>,
    game_events: Vec<game::event::Event>,
}

impl Default for MainState {
    fn default() -> Self {
        Self {
            dirty: true,
            client_events: Vec::with_capacity(10),
            window: Default::default(),
            time: Default::default(),
            layer: Default::default(),
            population: Default::default(),
            player: Default::default(),
            long_click_start: Default::default(),
            commands: Default::default(),
            game_events: Vec::with_capacity(10),
        }
    }
}

fn resolve_commands(commands: &mut Vec<Command>) {
    commands.retain(|command| match command {
        // TODO: Log that this shouldn't happen
        Command::OfferContract(_id) => false,
    });
}

fn resolve_events(events: &mut Vec<GameEvent>) {
    events.retain(|event| {
        match event {
            // Death is raised by the Dwarf and dealt with next tick
            GameEvent::Death(_id) => true,
            GameEvent::DistributePower(_power) => false,
        }
    });
}

impl MainState {
    /// Creates the initial Main State.
    pub fn from_config(
        ctx: &mut (impl context::Screen + context::Image + context::Font),
        app_config: &AppConfig,
    ) -> GameResult<MainState> {
        let mut layer = Layer::from_config(app_config);
        let mut population = Population::default();
        population.queue_spawn_location(&mut layer);
        let player_location = population
            .next_spawn_location
            .or_else(|| Some(layer::Location::new(0, 0)));
        let mut window = Window::from_config(ctx, app_config)?;
        window.move_grid_to(player_location);
        let s = MainState {
            dirty: true,
            client_events: Vec::with_capacity(10),
            window,
            layer,
            time: Time::from_config(app_config),
            population,
            player: Player::new(player_location),
            long_click_start: Default::default(),
            commands: Default::default(),
            game_events: Vec::with_capacity(10),
        };
        Ok(s)
    }

    /// Resizes the window.
    pub fn resize(
        &mut self,
        screen: &mut impl context::Screen,
        width: impl Into<point::Coord>,
        height: impl Into<point::Coord>,
    ) -> GameResult<bool> {
        let width = width.into();
        let height = height.into();
        let rect = Rect::new(0, 0, width, height);
        screen.set_coordinates(rect)?;

        self.window = self.window.resize(width, height);
        self.window.move_grid_to(self.player.location);

        Ok(true)
    }

    // Returns true if you can zoom from one size to another.
    fn valid_zoom(
        new_size: impl Into<point::Coord>,
        old_size: impl Into<point::Coord>,
    ) -> bool {
        let minimum_font_size = Config::instance().app_config.minimum_font_size;
        let maximum_font_size = Config::instance().app_config.maximum_font_size;
        let new_size = new_size.into();

        new_size >= minimum_font_size
            && new_size <= maximum_font_size
            && (new_size != old_size.into())
    }

    /// Zoomes the grid pane by increasing or decreasing the font/tile
    /// size.
    pub fn zoom(
        &mut self,
        amount: impl Into<point::Coord>,
    ) -> GameResult<bool> {
        let mut updated = false;
        let orig_size = self.window.grid_font().size;
        let amount = amount.into();

        let new_size = orig_size + amount;
        if Self::valid_zoom(new_size, orig_size) {
            self.window.change_grid_font(new_size);
            updated = true;
        }
        Ok(updated)
    }

    /// Moves the player one tile in the given direction
    /// Updates the grid to center on that location
    /// If the movement would take the player outside the layer,
    /// no movement is made.
    pub fn walk(&mut self, dir: Direction) -> GameResult<bool> {
        let moved = self.player.walk(&self.layer, dir);
        if moved {
            self.window.move_grid_to(self.player.location);
        }
        Ok(moved)
    }

    /// Moves the player position to a point on the screen
    /// without scrolling the grid
    /// If the position is outside the layer, nothing happens
    pub fn focus_player(&mut self, point: Point) -> GameResult<bool> {
        let location =
            self.window.layer_location_from_point(point, &self.layer);
        if location.is_some() {
            self.player.location = location;
        }
        Ok(location.is_some())
    }

    /// Recenter the grid on the player location
    pub fn recenter(&mut self) -> GameResult<bool> {
        if self.player.location.is_some() {
            self.window.move_grid_to(self.player.location);
        }
        Ok(self.player.location.is_some())
    }

    pub fn zoom_to_dwarf(&mut self) -> GameResult<bool> {
        let location = self.population.active_dwarf_location();
        if location.is_some() {
            self.player.location = location;
            self.recenter()
        } else {
            Ok(false)
        }
    }

    pub fn follow_dwarf(&mut self) -> GameResult<bool> {
        self.player.following = true;
        Ok(false)
    }

    /// Toggles the given element type from being drawn in the window.
    pub fn toggle_ui(&mut self, element: ElementType) {
        self.window.toggle_ui(element)
    }

    pub fn start_long_click(&mut self, ctx: &impl context::Timer) {
        self.long_click_start = Some(time::now(ctx));
    }

    pub fn stop_long_click(&mut self) {
        self.long_click_start = None;
    }

    /// Opens the context menu in the tile beside the x, y coordinates
    pub fn open_context_menu(&mut self, point: Point) -> GameResult<bool> {
        self.window.open_context_menu(point);
        Ok(true)
    }

    /// Closes the context menu potentially selecting a value at the
    /// location
    pub fn close_context_menu(&mut self) -> GameResult<bool> {
        let locations = Locations::new(&self.population);
        if let Some(command) =
            self.window.context_menu_item(&self.player, &locations)
        {
            self.commands.push(command);
        }
        self.window.close_context_menu();
        Ok(true)
    }

    pub fn handle_timers(
        &mut self,
        ctx: &(impl context::Timer + context::Mouse),
    ) -> GameResult<bool> {
        let mut updated = false;
        if let Some(start) = self.long_click_start {
            if (time::now(ctx) - start)
                > AppConfig::default().long_click_duration
            {
                self.window.open_context_menu(ctx.position());
                updated = true;
            }
        }
        updated |= self.time.health_check();
        Ok(updated)
    }

    /// Main game even handler.  The ggez event handlers add an event
    /// to the main state.  The event is handled on the ggez `update`
    /// call.  Returns true if the handling of the event requires
    /// a redraw.
    pub fn handle_events(&mut self, ctx: &mut Context) -> GameResult<bool> {
        let mut updated = false;

        while let Some(event) = self.client_events.pop() {
            match event {
                ClientEvent::Resize(point) => {
                    updated = self.resize(ctx, point.x(), point.y())?;
                }
                ClientEvent::Zoom(amount) => {
                    updated = self.zoom(amount)?;
                }
                ClientEvent::Quit => {
                    ggez::event::quit(ctx);
                }
                ClientEvent::Walk(dir) => {
                    updated = self.walk(dir)?;
                }
                ClientEvent::FocusCursor(point) => {
                    updated = self.focus_player(point)?
                }
                ClientEvent::Redraw => {
                    updated = true;
                }
                ClientEvent::Recenter() => {
                    updated = self.recenter()?;
                }
                ClientEvent::ZoomToDwarf => {
                    updated = self.zoom_to_dwarf()?;
                }
                ClientEvent::FollowDwarf => {
                    updated = self.follow_dwarf()?;
                }
                ClientEvent::TogglePause => {
                    self.time.toggle_pause();
                }
                ClientEvent::Pause => self.time.temporary_pause(),
                ClientEvent::Resume => self.time.temporary_resume(),
                ClientEvent::ToggleUI(element) => {
                    self.toggle_ui(element);
                    updated = true;
                }
                ClientEvent::ToggleAscii => {
                    self.toggle_ui(ElementType::Ascii);
                    self.toggle_ui(ElementType::Glyphs);
                    updated = true;
                }
                ClientEvent::CloseContextMenu => {
                    updated = self.close_context_menu()?;
                }
                ClientEvent::LongClickStart => self.start_long_click(ctx),
                ClientEvent::LongClickStop => self.stop_long_click(),

                ClientEvent::Continue => {}
            }
        }
        Ok(updated)
    }

    /// Update the game physics per frame
    fn update_game(&mut self, timer: &mut impl context::Timer) -> bool {
        let time = &mut self.time;
        let layer = &mut self.layer;
        let population = &mut self.population;
        let window = &mut self.window;
        let player = &mut self.player;
        let commands = &mut self.commands;
        let events = &mut self.game_events;

        time.increment_frames(timer);

        let updated = time.process_ticks(|updated| {
            let mut updated = updated;
            updated |= layer.tick(events);
            updated |= player.tick(events, commands);
            updated |= population.tick(events, commands, layer);
            updated |= window.tick();
            updated |= window.update_power(player.power);
            resolve_events(events);
            resolve_commands(commands);
            updated
        });
        time.settle_pause();

        updated
    }

    /// Sleep for the remainder of the frame
    fn sleep(&mut self, timer: &impl context::Timer) {
        let frame = seconds(1, self.time.desired_fps);
        let duration = ggez::timer::f64_to_duration(frame);

        timer.sleep(duration);
    }

    /// Update the FPS indication in the window.
    fn update_fps(
        &mut self,
        timer: &impl context::Timer,
        updated: bool,
    ) -> bool {
        if updated {
            self.window.update_fps(timer.fps());
            true
        } else {
            false
        }
    }

    fn update_handler(&mut self, ctx: &mut Context) -> GameResult<()> {
        let mut updated = false;

        if self.player.following {
            updated |= self.zoom_to_dwarf()?;
        }
        updated |= self.update_game(ctx);
        updated |= self.handle_timers(ctx)?;
        updated |= self.handle_events(ctx)?;
        updated |= self.update_fps(ctx, updated);

        self.dirty = updated;

        if !self.dirty {
            self.sleep(ctx);
        }

        Ok(())
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.update_handler(ctx)
    }

    fn focus_event(&mut self, _ctx: &mut Context, gained: bool) {
        if gained {
            self.client_events.push(ClientEvent::Redraw);
        }
    }

    fn resize_event(&mut self, _ctx: &mut Context, width: f32, height: f32) {
        self.client_events.push(ClientEvent::Resize(Point::new(
            point::Coord::lossy_from(width),
            point::Coord::lossy_from(height),
        )));
    }

    fn mouse_wheel_event(&mut self, _ctx: &mut Context, _x: f32, y: f32) {
        if y != 0.0 {
            let zoom_increment = Config::instance().app_config.zoom_increment;
            let amount = point::Coord::lossy_from(y / zoom_increment);
            if amount != 0.into() {
                self.client_events.push(ClientEvent::Zoom(amount));
            }
        }
    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        button: MouseButton,
        x: f32,
        y: f32,
    ) {
        if button == event::MouseButton::Left {
            self.client_events.push(ClientEvent::FocusCursor(Point::new(
                point::Coord::lossy_from(x),
                point::Coord::lossy_from(y),
            )));
            self.client_events.push(ClientEvent::Pause);
            self.client_events.push(ClientEvent::LongClickStart);
        }
    }

    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        button: MouseButton,
        _x: f32,
        _y: f32,
    ) {
        if button == event::MouseButton::Left {
            self.client_events.push(ClientEvent::CloseContextMenu);
            self.client_events.push(ClientEvent::LongClickStop);
            self.client_events.push(ClientEvent::Recenter());
            self.client_events.push(ClientEvent::Resume);
        }
    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        keycode: KeyCode,
        _keymod: KeyMods,
    ) {
        if let Some(event) =
            Config::instance().app_config.key_binding.get(keycode)
        {
            self.client_events.push(event);
        }
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.dirty {
            let locations = Locations::new(&self.population);
            let conditions = Conditions::new(&self.population);

            self.window.queue(
                ctx,
                &self.layer,
                &self.player,
                &locations,
                &conditions,
            )?;
            self.window.draw(
                ctx,
                &self.layer,
                &self.player,
                &locations,
                &conditions,
            )?;
            graphics::present(ctx)?;
            self.sleep(ctx);
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::client::{
        context::tests::{FakeScreen, FakeTimer},
        time::{GameSpeed, PauseState},
    };

    struct Setup {
        pub state: MainState,
    }

    impl Default for Setup {
        fn default() -> Self {
            Self {
                state: MainState::default(),
            }
        }
    }

    #[test]
    fn test_seconds() {
        assert_eq!(seconds(100, 100), 1.0_f64);
        assert_eq!(seconds(50, 100), 0.5_f64);
        assert_eq!(seconds(150, 100), 1.5_f64);
        assert_eq!(seconds(0, 100), 0.0_f64);
        assert_eq!(seconds(100, 0), 0.0_f64);
    }

    #[test]
    fn resize() {
        let mut setup = Setup::default();
        let state = &mut setup.state;
        let mut screen = FakeScreen::default();

        assert_eq!(state.resize(&mut screen, 2000, 2000).is_ok(), true);
        screen.assert_rect_is(&Rect::new(0, 0, 2000, 2000));
        assert_eq!(state.window.rect, Rect::new(0, 0, 2000, 2000));
    }

    #[test]
    fn test_valid_zoom() {
        // This is below the default mimimum size
        assert_eq!(MainState::valid_zoom(2, 32), false);
        // This is above the default maximum size
        assert_eq!(MainState::valid_zoom(15200, 32), false);
        // This is goldilocks size
        assert_eq!(MainState::valid_zoom(100, 32), true);
    }

    #[test]
    fn zoom() {
        let mut setup = Setup::default();
        let state = &mut setup.state;

        assert_eq!(state.window.font.size, 12.into());
        assert_eq!(state.window.grid_font().size, 12.into());
        assert_eq!(state.zoom(30).unwrap(), true);
        // The window font stays the same so that if we want
        // to reset some we can.
        assert_eq!(state.window.font.size, 12.into());
        // But the grid font is updated
        assert_eq!(state.window.grid_font().size, 42.into());
        // Should not go below the minimum
        assert_eq!(state.zoom(-40).unwrap(), false);
        assert_eq!(state.window.grid_font().size, 42.into());
        // But you can decrease it
        assert_eq!(state.zoom(-10).unwrap(), true);
        assert_eq!(state.window.grid_font().size, 32.into());
    }

    #[test]
    fn toggle_pause() {
        let mut setup = Setup::default();
        let state = &mut setup.state;

        // We start in paused mode
        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));

        // Toggling it off goes to Starting mode
        state.time.toggle_pause();
        assert_eq!(state.time.speed, GameSpeed::Starting);

        // To transition to running mode we need to call settle_pause
        state.time.settle_pause();
        assert_eq!(state.time.speed, GameSpeed::Running(1.0));

        // Toggling it on goes to Pause mode again
        state.time.toggle_pause();
        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));
    }

    #[test]
    fn pause_through_update_game() {
        let mut setup = Setup::default();
        let state = &mut setup.state;
        let mut timer = FakeTimer::default();

        // We start in paused mode
        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));
        assert_eq!(state.time.frames, 0);

        // Updating the game leaves us in Paused mode
        state.update_game(&mut timer);
        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));
        assert_eq!(state.time.frames, 1);

        // When in Staring mode it moves to running after updating.
        state.time.toggle_pause();
        state.update_game(&mut timer);
        assert_eq!(state.time.speed, GameSpeed::Running(1.0));
    }

    #[test]
    fn pause_edge_cases() {
        let mut setup = Setup::default();
        let state = &mut setup.state;

        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));
        state.time.toggle_pause();
        assert_eq!(state.time.speed, GameSpeed::Starting);
        state.time.temporary_pause();
        assert_eq!(
            state.time.speed,
            GameSpeed::Paused(PauseState::ResumeStarting)
        );
        state.time.toggle_pause();
        assert_eq!(
            state.time.speed,
            GameSpeed::Paused(PauseState::ResumeStarting)
        );
        state.time.temporary_resume();
        assert_eq!(state.time.speed, GameSpeed::Starting);

        state.time.toggle_pause();
        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));
        state.time.temporary_pause();
        assert_eq!(
            state.time.speed,
            GameSpeed::Paused(PauseState::ResumePaused)
        );
        state.time.toggle_pause();
        assert_eq!(
            state.time.speed,
            GameSpeed::Paused(PauseState::ResumePaused)
        );
        state.time.temporary_resume();
        assert_eq!(state.time.speed, GameSpeed::Paused(PauseState::Normal));
    }
}
