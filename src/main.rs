//! # Chibi Shiro
//!
//! My actual goal: a full fledged game that captures the spirit of
//! Dwarf Fortress, but with a different set of development
//! priorities.  I'm still at a very early stage of development, so
//! expect everything above to change over time.

use ggez::{
    event,
    graphics::{set_default_filter, FilterMode::Nearest},
    GameResult,
};
#[macro_use]
extern crate strum_macros;

pub mod chibi_shiro;
pub mod game;

use chibi_shiro::MainState;
use game::{config::app_config::AppConfig, init};

pub fn run(app_config: &'static AppConfig) -> GameResult<()> {
    let game_id = &app_config.game_id;
    let (mut ctx, events) = init(game_id)?;
    set_default_filter(&mut ctx, Nearest);
    let state = MainState::from_config(&mut ctx, app_config)?;

    event::run(ctx, events, state)
}

pub fn main() {
    let app_config = &game::config::Config::instance().app_config;
    if let Err(e) = run(app_config) {
        println!("{}", e);
    }
}
