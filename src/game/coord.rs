/// A newtype to hold coordinate types for locations on a grid or map
use std::ops::{Add, Div, Mul, Sub};

pub trait SaturatingOps {
    fn saturating_add(self, x: Self) -> Self;
    fn saturating_sub(self, x: Self) -> Self;
    fn saturating_mul(self, x: Self) -> Self;
}

pub trait MaxValue {
    fn max_value() -> Self;
}

pub trait CheckedOps<T: Sized>: Sized {
    fn checked_add(self, x: T) -> Option<Self>;
    fn checked_sub(self, x: T) -> Option<Self>;
}

pub trait LossyFrom<F>: Sized {
    fn lossy_from(x: F) -> Self;
}

pub trait LossyInto<F>: Sized {
    fn lossy_into(self) -> F;
}

pub trait CoordInner<T>:
    SaturatingOps + CheckedOps<T> + MaxValue + LossyFrom<f32> + LossyInto<f32>
{
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Coord<T: CoordInner<T>>(T);

impl<T> std::fmt::Display for Coord<T>
where
    T: CoordInner<T> + std::fmt::Display + Copy,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value())
    }
}

impl<T: CoordInner<T>> Add for Coord<T> {
    type Output = Self;

    #[inline]
    fn add(self, x: Coord<T>) -> Self {
        Coord(self.0.saturating_add(x.0))
    }
}

impl<T: CoordInner<T>> Add<T> for Coord<T> {
    type Output = Self;

    #[inline]
    fn add(self, x: T) -> Self {
        Coord(self.0.saturating_add(x))
    }
}

impl<T: CoordInner<T>> Sub for Coord<T> {
    type Output = Self;

    #[inline]
    fn sub(self, x: Coord<T>) -> Self {
        Coord(self.0.saturating_sub(x.0))
    }
}

impl<T: CoordInner<T>> Sub<T> for Coord<T> {
    type Output = Self;

    #[inline]
    fn sub(self, x: T) -> Self {
        Coord(self.0.saturating_sub(x))
    }
}

impl<T: CoordInner<T>> Div for Coord<T> {
    type Output = Self;

    // FIXME: Divide by zero!
    #[inline]
    fn div(self, x: Coord<T>) -> Self {
        Coord(T::lossy_from(
            (self.0.lossy_into() / x.0.lossy_into()).round(),
        ))
    }
}

impl<T: CoordInner<T>> Div<T> for Coord<T> {
    type Output = Self;

    #[inline]
    fn div(self, x: T) -> Self {
        Coord(T::lossy_from(
            (self.0.lossy_into() / x.lossy_into()).round(),
        ))
    }
}

impl<T: CoordInner<T>> Mul for Coord<T> {
    type Output = Self;

    #[inline]
    fn mul(self, x: Coord<T>) -> Self {
        Coord(self.0.saturating_mul(x.0))
    }
}

impl<T: CoordInner<T>> Mul<T> for Coord<T> {
    type Output = Self;

    #[inline]
    fn mul(self, x: T) -> Self {
        Coord(self.0.saturating_mul(x))
    }
}

impl<T: CoordInner<T>> Coord<T> {
    pub fn new(x: T) -> Self {
        Self(x)
    }

    pub fn max_value() -> Self {
        Self(T::max_value())
    }

    pub fn checked_add(self, x: Coord<T>) -> Option<Self> {
        self.0.checked_add(x.0).map(Self::new)
    }

    pub fn checked_sub(self, x: Coord<T>) -> Option<Self> {
        self.0.checked_sub(x.0).map(Self::new)
    }

    pub fn value(self) -> T {
        self.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::location;

    #[test]
    fn construction() {
        let a: Coord<location::coord::Internal> = Coord::new(42);
        let b: Coord<location::coord::Internal> = Coord::new(42);
        assert_eq!(a, b);
    }
}
