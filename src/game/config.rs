pub mod app_config;
pub mod locale;
pub mod resource;

use once_cell::sync::OnceCell;

use app_config::AppConfig;
use locale::Locale;
use resource::Resource;

use crate::game::{command::Command, player};

static CONFIG: OnceCell<Config> = OnceCell::new();

pub struct Config {
    pub app_config: AppConfig,
    pub resources: Vec<Resource>,
}

impl Default for Config {
    fn default() -> Self {
        let resources = vec![Resource::default()];
        Self {
            app_config: AppConfig::default(),
            resources,
        }
    }
}

impl Config {
    pub fn instance() -> &'static Self {
        CONFIG.get_or_init(Self::default)
    }

    fn default_locale(&self) -> Locale {
        Locale::new("en-US", self.resources.get(0).unwrap())
    }

    pub fn get_message(&self, key: &str) -> String {
        self.default_locale().get_message(key, None)
    }

    pub fn get_fps_message(&self, fps: u32) -> String {
        self.default_locale().get_fps_message(fps)
    }

    pub fn get_power_message(&self, power: player::Power) -> String {
        self.default_locale().get_power_message(power)
    }

    pub fn get_clock_message(&self, ticks: usize) -> String {
        self.default_locale().get_clock_message(ticks)
    }

    pub fn get_command_message(&self, command: &Command) -> String {
        self.default_locale().get_command_message(command)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_non_existent_message() {
        let result = std::panic::catch_unwind(|| {
            Config::instance().get_message("non-existent")
        });
        assert!(
            result.is_err(),
            "getting 'non-existent' message should panic"
        );
    }

    #[test]
    fn test_get_game_title() {
        assert_eq!(Config::instance().get_message("game-title"), "Chibi Shiro");
    }

    #[test]
    fn test_get_menu_title() {
        assert_eq!(Config::instance().get_message("menu-title"), "Menu");
    }

    // This is why we don't want to query it directly
    #[test]
    fn test_get_message_without_args() {
        assert_eq!(Config::instance().get_message("fps"), "FPS: {$fps}");
    }

    #[test]
    fn test_get_fps_message() {
        assert_eq!(Config::instance().get_fps_message(32), "FPS: 32");
    }

    #[test]
    fn test_get_clock_message() {
        assert_eq!(Config::instance().get_clock_message(42), "Clock: 42");
    }
}
