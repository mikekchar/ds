//! Code around symbols to be plotted on the map.
//! A symbol is composed of a string indicating what to draw,
//! a weight (either bold or regular), a colour, and a background
//! colour.
use ggez::graphics::{Align, PxScale, Text, TextFragment};
use rand::Rng as RngTrait;
use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
};

use crate::game::{
    client::{
        color::Color,
        font::Font,
        point::{self, Point},
    },
    coord::LossyFrom,
    location,
    location::Direction,
    rng::Rng,
};

mod attribute;
mod char;
mod description;
mod glyph;

pub use self::char::{Char, Weight};
pub use attribute::{
    Attribute,
    MergeableAttribute,
    Priority,
    StackableAttribute,
};
pub use description::Description;
pub use glyph::Glyph;

use super::dwarf::{self, Condition};

pub trait Merge {
    type Output;

    fn merge(self, other: Self) -> Self;
}

/// Represents something to be plotted on the map.
/// The `Symbol` is composed of a string that says
/// what should be drawn, the colour to draw it,
/// the font weight to draw it with and the background
/// colour to draw it agains.  The string and background
/// are optional.  If they are `None` then nothing will
/// be drawn when the symbol is rendered.
#[derive(PartialEq, Default, Debug, Clone)]
pub struct Symbol<'a> {
    pub string: MergeableAttribute<Char<'a>>,
    pub background: MergeableAttribute<Color>,
    pub glyph_index: StackableAttribute<Glyph>,
    pub status: MergeableAttribute<Color>,
    pub water: MergeableAttribute<Color>,
}

impl<'a> Merge for Symbol<'a> {
    type Output = Symbol<'a>;

    fn merge(self, other: Symbol<'a>) -> Self::Output {
        Symbol {
            string: self.string.merge(other.string),
            background: self.background.merge(other.background),
            glyph_index: self.glyph_index.merge(other.glyph_index),
            status: self.status.merge(other.status),
            water: self.water.merge(other.water),
        }
    }
}

impl<'a> Merge for Option<Symbol<'a>> {
    type Output = Option<Symbol<'a>>;

    fn merge(self, other: Option<Symbol<'a>>) -> Self::Output {
        if let Some(x) = self {
            if let Some(y) = other {
                Some(x.merge(y))
            } else {
                Some(x)
            }
        } else {
            other
        }
    }
}

impl<'a> Symbol<'a> {
    /// Returns a `Text` rendering the string in the correct font and
    /// colour. Returns `None` if string is `None`.
    pub fn text(&self, font: Font, bounds: point::Coord) -> Option<Text> {
        self.string.map(|string| {
            let used_font = match string.value.weight {
                Weight::Regular => font.regular,
                Weight::Bold => font.bold,
            };

            let mut text = Text::new(TextFragment {
                text: string.value.string.to_string(),
                color: Some(string.value.color.into()),
                font: used_font,
                scale: Some(PxScale::from(f32::lossy_from(font.size))),
            });
            text.set_bounds(Point::new(bounds, bounds), Align::Center);
            text
        })
    }

    /// Returns a tag to use when caching the foreground of the
    /// `Symbol`.
    pub fn fg_hash(&self) -> Option<u64> {
        let mut hasher = DefaultHasher::new();
        self.string.map(|attr| attr.value).hash(&mut hasher);
        Some(hasher.finish())
    }

    /// Returns a tag to use when caching the background of the
    /// `Symbol`.
    pub fn bg_hash(&self) -> Option<u64> {
        let mut hasher = DefaultHasher::new();
        self.string.map(|attr| attr.value).hash(&mut hasher);
        Some(hasher.finish())
    }

    pub fn error() -> Self {
        Symbol {
            string: Attribute::mergeable(
                Char::regular("?", 1.0, 0.3, 0.3, 1.0),
                255,
            ),
            glyph_index: Attribute::stackable(&[Glyph::opaque(14)], 255),
            ..Default::default()
        }
    }

    pub fn off_map() -> Self {
        Symbol {
            string: None,
            background: Attribute::mergeable(Color::opaque(1.0, 0.9, 0.7), 0),
            ..Default::default()
        }
    }

    pub fn cursor(following: bool) -> Self {
        if following {
            Symbol {
                string: Attribute::mergeable(
                    Char::bold("+", 1.0, 1.0, 0.0, 1.0),
                    254,
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(31)], 254),
                ..Default::default()
            }
        } else {
            Symbol {
                string: Attribute::mergeable(
                    Char::bold("X", 1.0, 1.0, 0.0, 1.0),
                    254,
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(15)], 254),
                ..Default::default()
            }
        }
    }

    fn generate_terrain(
        terrains: &[&'a str],
        terrain_index: location::coord::Internal,
        r: f32,
        g: f32,
        b: f32,
        a: f32,
    ) -> Self {
        if terrain_index > 0 && terrain_index < terrains.len() + 1 {
            let string = terrains[terrain_index - 1];

            Symbol {
                string: Attribute::mergeable(
                    Char::regular(string, r, g, b, a),
                    1,
                ),
                glyph_index: Attribute::stackable(
                    &[Glyph::opaque(31 + terrain_index)],
                    1,
                ),
                ..Default::default()
            }
        } else {
            Symbol {
                glyph_index: Attribute::stackable(&[Glyph::opaque(30)], 1),
                ..Default::default()
            }
        }
    }

    fn generate_floor(
        terrains: &[&'a str],
        terrain_index: location::coord::Internal,
        r: f32,
        g: f32,
        b: f32,
        a: f32,
    ) -> Self {
        if terrain_index > 0 && terrain_index < terrains.len() + 1 {
            let string = terrains[terrain_index - 1];

            Symbol {
                string: Attribute::mergeable(
                    Char::regular(string, r, g, b, a),
                    2,
                ),
                glyph_index: Attribute::stackable(
                    &[Glyph::opaque(terrain_index + 3)],
                    2,
                ),
                ..Default::default()
            }
        } else {
            Symbol {
                string: Attribute::mergeable(
                    Char::regular(terrains[0], r, g, b, a),
                    2,
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(0)], 2),
                ..Default::default()
            }
        }
    }

    // Only 5% of the terrain is "intersting" (uses a non-default symbol)
    fn terrain_type(
        terrains: &[&str],
        mut rng: Rng,
    ) -> location::coord::Internal {
        if rng.gen_range(1..=20) == 1 {
            rng.gen_range(1..=terrains.len())
        } else {
            0
        }
    }

    pub fn undiscovered_terrain(rng: Rng) -> Self {
        const TERRAINS: [&str; 5] = ["'", ",", ".", "`", "%"];
        let terrain_index = Symbol::terrain_type(&TERRAINS, rng);
        Symbol::generate_terrain(&TERRAINS, terrain_index, 0.5, 0.5, 0.5, 1.0)
    }

    // Only 25% of the terrain is "interesting" (uses a non-default
    // symbol)
    fn floor_type(
        terrains: &[&str],
        mut rng: Rng,
    ) -> location::coord::Internal {
        if rng.gen_range(1..=4) == 1 {
            rng.gen_range(1..=terrains.len())
        } else {
            0
        }
    }

    pub fn floor(rng: Rng) -> Self {
        const TERRAINS: [&str; 7] = [".", ",", "'", "`", "\"", ":", ";"];
        let terrain_index = Symbol::floor_type(&TERRAINS, rng);
        Symbol::generate_floor(&TERRAINS, terrain_index, 0.8, 0.8, 0.2, 1.0)
    }

    pub fn water(level: usize) -> Self {
        Symbol {
            water: Attribute::mergeable(
                Color::transparent(0.0, 0.3, 0.7, level as f32 / 256.0),
                2,
            ),
            ..Default::default()
        }
    }

    pub fn wall(wall_index: location::coord::Internal) -> Self {
        Symbol {
            string: Attribute::mergeable(
                Char::bold("#", 1.0, 1.0, 1.0, 1.0),
                3,
            ),
            background: Attribute::mergeable(Color::opaque(0.7, 0.7, 0.7), 3),
            glyph_index: Attribute::stackable(
                &[Glyph::opaque(wall_index + 48)],
                3,
            ),
            ..Default::default()
        }
    }

    pub fn pool(pool_index: location::coord::Internal) -> Self {
        Symbol {
            string: Attribute::mergeable(
                Char::bold("~", 1.0, 1.0, 1.0, 1.0),
                3,
            ),
            background: Attribute::mergeable(Color::opaque(0.2, 0.3, 0.7), 3),
            glyph_index: Attribute::stackable(
                &[Glyph::opaque(pool_index + 96)],
                3,
            ),
            ..Default::default()
        }
    }
    pub fn dwarf(facing: Direction) -> Self {
        let index = match facing {
            Direction::Down => 0,
            Direction::Left => 1,
            Direction::Right => 2,
            Direction::Up => 3,
            _ => 0,
        };
        Symbol {
            string: Attribute::mergeable(
                Char::regular("☺", 1.0, 0.0, 1.0, 1.0),
                5,
            ),
            glyph_index: Attribute::stackable(&[Glyph::opaque(16 + index)], 5),
            ..Default::default()
        }
    }

    pub fn corpse() -> Self {
        Symbol {
            string: Attribute::mergeable(
                Char::regular("☺", 0.2, 0.9, 0.4, 1.0),
                3,
            ),
            background: Attribute::mergeable(Color::opaque(0.8, 0.0, 0.1), 3),
            glyph_index: Attribute::stackable(&[Glyph::opaque(20)], 3),
            ..Default::default()
        }
    }

    pub fn skeleton() -> Self {
        Symbol {
            string: Attribute::mergeable(
                Char::regular("☺", 1.0, 1.0, 1.0, 1.0),
                3,
            ),
            background: Attribute::mergeable(Color::opaque(0.5, 0.3, 0.1), 3),
            glyph_index: Attribute::stackable(&[Glyph::opaque(21)], 3),
            ..Default::default()
        }
    }

    pub fn ghost(facing: Direction) -> Self {
        let index = match facing {
            Direction::Down => 0,
            Direction::Left => 1,
            Direction::Right => 2,
            Direction::Up => 3,
            _ => 0,
        };
        Symbol {
            string: Attribute::mergeable(
                Char::regular("Ñ", 1.0, 1.0, 1.0, 1.0),
                4,
            ),
            glyph_index: Attribute::stackable(
                &[Glyph::transparent(22 + index, 0.5)],
                4,
            ),
            ..Default::default()
        }
    }

    pub fn thirsty(amount: dwarf::Hydration) -> Self {
        Symbol {
            status: Attribute::mergeable(
                Color::transparent(1.0, 0.0, 0.0, (amount as f32) / 255.0),
                2,
            ),
            ..Default::default()
        }
    }

    pub fn target(condition: Condition) -> Self {
        let color = match condition {
            Condition::Target => Color::transparent(0.5, 0.3, 0.1, 0.4),
            Condition::RememberedTarget => {
                Color::transparent(0.1, 0.3, 0.4, 0.4)
            }
        };
        Symbol {
            status: Attribute::mergeable(color, 1),
            ..Default::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::{assert_eq, assert_ne};
    use std::cmp::Ordering;

    #[test]
    fn attribute_eq() {
        // eq() *must* compare all of the fields in Attribute or else all of
        // our subsequent tests will potentially produce incorrect results
        assert_eq!(Attribute::new(10, 1), Attribute::new(10, 1));
        assert_ne!(Attribute::new(10, 1), Attribute::new(10, 2));
        assert_ne!(Attribute::new(10, 1), Attribute::new(9, 1));
    }

    #[test]
    fn attribute_cmp() {
        assert!(Attribute::new(10, 1) == Attribute::new(10, 1));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(10, 1)),
            Ordering::Equal
        );
        assert!(Attribute::new(10, 1) < Attribute::new(10, 2));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(10, 2)),
            Ordering::Less
        );
        assert!(Attribute::new(10, 1) > Attribute::new(10, 0));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(10, 0)),
            Ordering::Greater
        );

        assert!(Attribute::new(10, 1) < Attribute::new(11, 1));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(11, 1)),
            Ordering::Less
        );

        assert!(Attribute::new(10, 1) > Attribute::new(9, 1));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(9, 1)),
            Ordering::Greater
        );

        assert!(Attribute::new(10, 1) < Attribute::new(9, 2));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(9, 2)),
            Ordering::Less
        );
        assert!(Attribute::new(10, 1) > Attribute::new(11, 0));
        assert_eq!(
            Attribute::new(10, 1).cmp(&Attribute::new(11, 0)),
            Ordering::Greater
        );
    }

    #[test]
    fn mergeable_attribute_merge() {
        let ma1 = Some(Attribute::new(22, 1));
        let ma2 = Some(Attribute::new(12, 2));
        assert_eq!(ma1.merge(ma2), ma2);
    }

    #[test]
    fn test_generate_terrain() {
        let terrains = vec!["'", ",", ".", "`", "%"];

        // Zero means that you want default rock
        assert_eq!(
            Symbol::generate_terrain(&terrains, 0, 0.5, 0.5, 0.5, 1.0),
            Symbol {
                glyph_index: Attribute::stackable(&[Glyph::opaque(30)], 1),
                ..Default::default()
            }
        );

        // Anything more than zero is interesting rock
        terrains.iter().enumerate().for_each(|(i, terrain)| {
            assert_eq!(
                Symbol::generate_terrain(&terrains, i + 1, 0.5, 0.5, 0.5, 1.0),
                Symbol {
                    string: Attribute::mergeable(
                        Char::regular(terrain, 0.5, 0.5, 0.5, 1.0),
                        1
                    ),
                    glyph_index: Attribute::stackable(
                        &[Glyph::opaque(31 + i + 1)],
                        1
                    ),
                    ..Default::default()
                }
            )
        });

        // Anything outside the range will generate an default tile
        assert_eq!(
            Symbol::generate_terrain(&terrains, 100, 0.5, 0.5, 0.5, 1.0),
            Symbol {
                glyph_index: Attribute::stackable(&[Glyph::opaque(30)], 1),
                ..Default::default()
            }
        )
    }

    #[test]
    fn test_undiscovered_terrain() {
        let seed = "test_seed";
        let rng = Rng::new(seed);
        assert_eq!(
            Symbol::undiscovered_terrain(rng),
            Symbol {
                glyph_index: Attribute::stackable(&[Glyph::opaque(30)], 1),
                ..Default::default()
            }
        );
    }

    #[test]
    fn test_floor() {
        let seed = "test_seed";
        let rng = Rng::new(seed);
        assert_eq!(
            Symbol::floor(rng),
            Symbol {
                string: Attribute::mergeable(
                    Char::regular(".", 0.8, 0.8, 0.2, 1.0),
                    2
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(0)], 2),
                ..Default::default()
            }
        );
    }

    #[test]
    fn concat() {
        assert_eq!(
            Symbol::default().merge(Symbol::dwarf(Direction::Down)),
            Symbol::dwarf(Direction::Down)
        );
        assert_eq!(
            Symbol::dwarf(Direction::Down).merge(Symbol::default()),
            Symbol::dwarf(Direction::Down)
        );
        assert_eq!(
            Symbol::wall(0).merge(Symbol::dwarf(Direction::Down)),
            Symbol {
                string: Attribute::mergeable(
                    Char::regular("☺", 1.0, 0.0, 1.0, 1.0),
                    5
                ),
                background: Attribute::mergeable(
                    Color::transparent(0.7, 0.7, 0.7, 1.0),
                    3
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(48)], 3)
                    .merge(Attribute::stackable(&[Glyph::opaque(16)], 5)),
                ..Default::default()
            }
        );
        assert_eq!(
            Symbol::dwarf(Direction::Down).merge(Symbol::wall(0)),
            Symbol {
                string: Attribute::mergeable(
                    Char::regular("☺", 1.0, 0.0, 1.0, 1.0),
                    5
                ),
                background: Attribute::mergeable(
                    Color::transparent(0.7, 0.7, 0.7, 1.0),
                    3
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(48)], 3)
                    .merge(Attribute::stackable(&[Glyph::opaque(16)], 5)),
                ..Default::default()
            }
        );
        assert_eq!(
            Symbol::skeleton().merge(Symbol::ghost(Direction::Down)),
            Symbol {
                string: Attribute::mergeable(
                    Char::regular("Ñ", 1.0, 1.0, 1.0, 1.0),
                    4
                ),
                background: Attribute::mergeable(
                    Color::transparent(0.5, 0.3, 0.1, 1.0),
                    3
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(21)], 3)
                    .merge(Attribute::stackable(&[Glyph::opaque(22)], 4)),
                ..Default::default()
            }
        );
        assert_eq!(
            Symbol::ghost(Direction::Down).merge(Symbol::skeleton()),
            Symbol {
                string: Attribute::mergeable(
                    Char::regular("Ñ", 1.0, 1.0, 1.0, 1.0),
                    4
                ),
                background: Attribute::mergeable(
                    Color::transparent(0.5, 0.3, 0.1, 1.0),
                    3
                ),
                glyph_index: Attribute::stackable(&[Glyph::opaque(21)], 3)
                    .merge(Attribute::stackable(&[Glyph::opaque(22)], 4)),
                ..Default::default()
            }
        );
    }
}
