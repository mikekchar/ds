//! Code for encapsulating results of UI actions.

use crate::game::dwarf;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Command {
    OfferContract(dwarf::Id),
}

impl Command {
    pub fn locale_key(&self) -> &str {
        match self {
            Command::OfferContract(_id) => "offer-contract-command",
        }
    }
}
