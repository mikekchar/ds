//! A chunk of map
use super::location;

#[derive(Debug)]
pub struct Chunk {
    pub size: usize,
    pub x: location::Coord,
    pub y: location::Coord,
}

impl Default for Chunk {
    fn default() -> Self {
        Self::new(10, 0, 0)
    }
}

impl Chunk {
    pub fn new(
        size: usize,
        x: impl Into<location::Coord>,
        y: impl Into<location::Coord>,
    ) -> Self {
        let x = x.into();
        let y = y.into();
        Self { size, x, y }
    }
}
