use super::resource::Resource;
use crate::game::{command::Command, player};
use fluent::{fluent_args, FluentArgs, FluentBundle, FluentResource};
use unic_langid::LanguageIdentifier;

pub struct Locale<'a> {
    pub bundle: FluentBundle<&'a FluentResource>,
}

impl<'a> Locale<'a> {
    // FIXME: Building the bundle can panic
    pub fn new(langid: &str, resource: &'a Resource) -> Self {
        let langid_en: LanguageIdentifier =
            langid.parse().expect("Parsing failed");
        let mut bundle = FluentBundle::new(vec![langid_en]);

        bundle
            .add_resource(resource.into())
            .expect("Failed to add FTL resources to the bundle.");

        bundle.set_use_isolating(false);
        Self { bundle }
    }

    // FIXME: get_message can panic
    pub fn get_message(&self, key: &str, args: Option<&FluentArgs>) -> String {
        // unwrap_or_else so that we don't have to format the error message
        // each time
        let msg = self
            .bundle
            .get_message(key)
            .unwrap_or_else(|| panic!("Message {} doesn't exist.", key));
        let pattern = msg
            .value()
            .unwrap_or_else(|| panic!("Message {} has no value.", key));
        let mut errors = vec![];
        self.bundle
            .format_pattern(pattern, args, &mut errors)
            .into_owned()
    }

    pub fn get_fps_message(&self, fps: u32) -> String {
        self.get_message("fps", Some(&fluent_args!["fps" => fps]))
    }

    pub fn get_power_message(&self, power: player::Power) -> String {
        self.get_message("power", Some(&fluent_args!["power" => power]))
    }

    pub fn get_clock_message(&self, ticks: usize) -> String {
        self.get_message("clock", Some(&fluent_args!["ticks" => ticks]))
    }

    pub fn get_command_message(&self, command: &Command) -> String {
        self.get_message(command.locale_key(), None)
    }
}
