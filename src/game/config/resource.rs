use fluent::FluentResource;

pub struct Resource(FluentResource);

impl<'a> From<&'a Resource> for &'a FluentResource {
    fn from(r: &'a Resource) -> Self {
        &r.0
    }
}

impl Default for Resource {
    fn default() -> Self {
        let ftl_string = "game-title = Chibi Shiro\n\
             fps = FPS: {$fps}\n\
             power = POWER: {$power}\n\
             clock = Clock: {$ticks}\n\
             menu-title = Menu\n\
             manifest-dir-error = Could not open manifest directory.\n\
             offer-contract-command = Offer contract"
            .to_owned();
        Self::compile(ftl_string)
    }
}

impl Resource {
    pub fn compile(ftl_string: String) -> Self {
        Self(
            FluentResource::try_new(ftl_string)
                .expect("Failed to parse an FTL string."),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_resource_compile_error() {
        let result = std::panic::catch_unwind(|| {
            Resource::compile("does not compile".to_owned())
        });
        assert!(result.is_err(), "resource compile error should panic");
    }
}
