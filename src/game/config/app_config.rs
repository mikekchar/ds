use ggez::graphics::Color;

use super::{locale::Locale, resource::Resource};
use crate::game::{
    client::{event::key_binding::KeyBinding, point, tile::ElementType},
    location,
};
use std::time::Duration;

pub struct AppConfig {
    pub window_title: String,
    pub window_dimensions: (point::Coord, point::Coord),
    pub manifest_dir_error: String,
    pub game_id: String,
    pub desired_fps: u32,
    pub regular_font_name: String,
    pub bold_font_name: String,
    pub font_size: point::Coord,
    pub tile_sheet_name: String,
    pub tile_sheet_rows: location::coord::Internal,
    pub tile_sheet_cols: location::coord::Internal,
    pub glyph_size: point::Coord,
    pub tile_padding: f32,
    pub chunk_size: usize,
    pub chunks_per_side: usize,
    pub rng_seed: String,
    pub bg_color: Color,
    pub grid_mesh_color: Color,
    pub frame_color: Color,
    pub fps_text_color: Color,
    pub power_text_color: Color,
    pub title_text_color: Color,
    pub minimum_font_size: point::Coord,
    pub maximum_font_size: point::Coord,
    pub zoom_increment: f32,
    pub key_binding: KeyBinding,
    pub grid_width_proporation: f32,
    pub grid_height_proporation: f32,
    pub dwarf_speed: f32,
    pub ticks_per_game_second: f64,
    pub ui_elements: Vec<ElementType>,
    pub long_click_duration: Duration,
}

impl Default for AppConfig {
    fn default() -> Self {
        let resource = Resource::default();
        let locale = Locale::new("en-US", &resource);
        AppConfig {
            window_title: locale.get_message("game-title", None),
            window_dimensions: (800.into(), 600.into()),
            manifest_dir_error: locale.get_message("manifest-dir-error", None),
            game_id: "chibi-shiro".to_owned(),
            desired_fps: 60,
            regular_font_name: "/FiraCode-Regular.ttf".to_owned(),
            bold_font_name: "/FiraCode-Bold.ttf".to_owned(),
            font_size: 26.into(),
            tile_padding: 1.2,
            chunk_size: 10,
            chunks_per_side: 5,
            rng_seed: "seed".to_owned(),
            bg_color: Color::new(0.0, 0.0, 0.0, 1.0),
            grid_mesh_color: Color::new(0.0, 0.4, 0.6, 1.0),
            frame_color: Color::new(0.1, 0.4, 0.3, 1.0),
            fps_text_color: Color::new(1.0, 1.0, 1.0, 1.0),
            power_text_color: Color::new(1.0, 1.0, 1.0, 1.0),
            title_text_color: Color::new(1.0, 1.0, 1.0, 1.0),
            minimum_font_size: 5.into(),
            maximum_font_size: 15_000.into(),
            zoom_increment: 0.2,
            key_binding: KeyBinding::default(),
            grid_width_proporation: 1.0 / 1.5,
            grid_height_proporation: 1.0 / 1.25,
            dwarf_speed: 0.1,
            ticks_per_game_second: 10.0,
            tile_sheet_name: "/StoneFloor.png".to_owned(),
            tile_sheet_rows: 16,
            tile_sheet_cols: 16,
            glyph_size: 32.into(),
            ui_elements: vec![
                ElementType::Backgrounds,
                ElementType::Glyphs,
                ElementType::Status,
            ],
            long_click_duration: Duration::new(1, 0),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::game::client::event::Event;
    use ggez::{event::KeyCode, graphics::Color};

    #[test]
    fn window_title() {
        assert_eq!(AppConfig::default().window_title, "Chibi Shiro");
    }

    #[test]
    fn window_dimensions() {
        assert_eq!(
            AppConfig::default().window_dimensions,
            (800.into(), 600.into())
        );
    }

    #[test]
    fn manifest_dir_error() {
        assert_eq!(
            AppConfig::default().manifest_dir_error,
            "Could not open manifest directory."
        );
    }

    #[test]
    fn manifest_game_id() {
        assert_eq!(AppConfig::default().game_id, "chibi-shiro");
    }

    #[test]
    fn desired_fps() {
        assert_eq!(AppConfig::default().desired_fps, 60);
    }

    #[test]
    fn regular_font_name() {
        assert_eq!(
            AppConfig::default().regular_font_name,
            "/FiraCode-Regular.ttf"
        );
    }

    #[test]
    fn bold_font_name() {
        assert_eq!(AppConfig::default().bold_font_name, "/FiraCode-Bold.ttf");
    }

    #[test]
    fn font_size() {
        assert_eq!(AppConfig::default().font_size, 26.into());
    }

    #[test]
    fn tile_sheet_name() {
        assert_eq!(AppConfig::default().tile_sheet_name, "/StoneFloor.png");
    }

    #[test]
    fn tile_padding() {
        assert_eq!(AppConfig::default().tile_padding, 1.2);
    }

    #[test]
    fn chunk_size() {
        assert_eq!(AppConfig::default().chunk_size, 10);
    }

    #[test]
    fn chunks_per_side() {
        assert_eq!(AppConfig::default().chunks_per_side, 5);
    }

    #[test]
    fn rng_seed() {
        assert_eq!(AppConfig::default().rng_seed, "seed");
    }

    #[test]
    fn grid_mesh_color() {
        assert_eq!(
            AppConfig::default().grid_mesh_color,
            Color::new(0.0, 0.4, 0.6, 1.0)
        );
    }

    #[test]
    fn frame_color() {
        assert_eq!(
            AppConfig::default().frame_color,
            Color::new(0.1, 0.4, 0.3, 1.0)
        );
    }

    #[test]
    fn bg_color() {
        assert_eq!(
            AppConfig::default().bg_color,
            Color::new(0.0, 0.0, 0.0, 1.0)
        );
    }

    #[test]
    fn fps_text_color() {
        assert_eq!(
            AppConfig::default().fps_text_color,
            Color::new(1.0, 1.0, 1.0, 1.0)
        );
    }

    #[test]
    fn title_text_color() {
        assert_eq!(
            AppConfig::default().title_text_color,
            Color::new(1.0, 1.0, 1.0, 1.0)
        );
    }

    #[test]
    fn minimum_font_size() {
        assert_eq!(AppConfig::default().minimum_font_size, 5.into());
    }

    #[test]
    fn maximum_font_size() {
        assert_eq!(AppConfig::default().maximum_font_size, 15_000.into());
    }

    #[test]
    fn maximum_zoom_increment() {
        assert_eq!(AppConfig::default().zoom_increment, 0.2);
    }

    #[test]
    fn key_binding() {
        let event = AppConfig::default().key_binding.get(KeyCode::Q).unwrap();
        match event {
            Event::Quit => {}
            _ => panic!("Received the wrong event for key binding"),
        }
    }

    #[test]
    fn width_proportion() {
        assert_eq!(AppConfig::default().grid_width_proporation, 1.0 / 1.5);
    }

    #[test]
    fn height_proportion() {
        assert_eq!(AppConfig::default().grid_height_proporation, 1.0 / 1.25);
    }

    #[test]
    fn dwarf_speed() {
        assert_eq!(AppConfig::default().dwarf_speed, 0.1);
    }

    #[test]
    fn ticks_per_second() {
        assert_eq!(AppConfig::default().ticks_per_game_second, 10.0);
    }
}
