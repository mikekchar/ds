use super::{
    client::{
        font::Font,
        menu::Menu,
        point,
        point::Point,
        rect::Rect,
        tile::Sheet,
        window::Window,
    },
    config::Config,
    coord::LossyFrom,
    dwarf::{Dwarf, Population},
    grid::Grid,
    layer::{self, Layer, Location},
    location,
    map::MapTile,
};

pub const SEED: &str = "test_seed";

pub struct Setup {
    pub layer: Layer,
    pub font_size: point::Coord,
    pub drawable_rect: Rect,
    pub border_width: point::Coord,
    pub border_height: point::Coord,
    pub window_x: point::Coord,
    pub window_y: point::Coord,
    pub population: Population,
    pub grid: Grid,
}

impl Default for Setup {
    fn default() -> Self {
        let seed = &Config::instance().app_config.rng_seed;
        let chunk_size = Config::instance().app_config.chunk_size;
        let chunks_per_side = Config::instance().app_config.chunks_per_side;
        let width: location::Coord = (chunk_size * chunks_per_side).into();
        let height: location::Coord = (chunk_size * chunks_per_side).into();
        let padding: f32 = Config::instance().app_config.tile_padding;
        let font_size: point::Coord = 10.into();

        let layer = Layer::new(chunk_size, chunks_per_side, seed);
        let tile_width = width.num_points(font_size.scale_by(padding));
        let tile_height = height.num_points(font_size.scale_by(padding));
        let rect = Rect::new(0, 0, tile_width, tile_height);
        let font = Font::new(None, None, font_size);

        Self {
            layer,
            font_size,
            drawable_rect: rect,
            border_width: point::Coord::lossy_from(width) * font_size,
            border_height: point::Coord::lossy_from(height) * font_size,
            window_x: 0.into(),
            window_y: 0.into(),
            population: Population::default(),
            grid: Grid::zero(rect, font, padding),
        }
    }
}

impl Setup {
    pub fn new(
        font_size: impl Into<point::Coord>,
        chunk_size: usize,
        chunks_per_side: usize,
    ) -> Self {
        let seed = &Config::instance().app_config.rng_seed;
        let width: location::Coord = (chunk_size * chunks_per_side).into();
        let height: location::Coord = (chunk_size * chunks_per_side).into();

        let layer = Layer::new(chunk_size, chunks_per_side, seed);
        let font_size = font_size.into();
        let padding: f32 = Config::instance().app_config.tile_padding;
        let tile_width = width.num_points(font_size.scale_by(padding));
        let tile_height = height.num_points(font_size.scale_by(padding));
        let rect = Rect::new(0, 0, tile_width, tile_height);
        let font = Font::new(None, None, font_size);

        Self {
            layer,
            font_size,
            drawable_rect: rect,
            border_width: point::Coord::lossy_from(width) * font_size,
            border_height: point::Coord::lossy_from(height) * font_size,
            window_x: 0.into(),
            window_y: 0.into(),
            population: Default::default(),
            grid: Grid::zero(rect, font, padding),
        }
    }

    pub fn with_room(
        &mut self,
        location: (location::coord::Internal, location::coord::Internal),
        size: location::coord::Internal,
    ) {
        self.layer.map.add_room(
            Location::new(location.0, location.1),
            size,
            size,
        );
    }

    pub fn with_pool(
        &mut self,
        location: (location::coord::Internal, location::coord::Internal),
        size: location::coord::Internal,
    ) {
        self.layer.map.add_space(
            Location::new(location.0, location.1),
            size,
            size,
            MapTile::Pool,
        );
    }

    pub fn with_dwarf(
        &mut self,
        location: (location::coord::Internal, location::coord::Internal),
    ) {
        let mut dwarf =
            Dwarf::new(layer::Location::new(location.0, location.1));
        dwarf.speed = 1.0;
        self.population.push(dwarf);
    }

    pub fn menu(&self) -> Menu {
        let font = Font::new(None, None, self.font_size);
        Menu::new(self.drawable_rect, font)
    }

    pub fn window(&self) -> Window {
        let font = Font::new(None, None, self.font_size);
        let glyphs = Sheet::default();
        let destination = Point::new(self.window_x, self.window_y);
        let mut window = Window::new(
            font,
            glyphs,
            destination,
            self.border_width,
            self.border_height,
        );
        window.move_grid_to(Some(layer::Location::new(0, 0)));
        window
    }
}
