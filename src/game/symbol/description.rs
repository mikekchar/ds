use super::Merge;
use crate::game::{
    client::{font::Font, point},
    coord::LossyFrom,
};
use ggez::graphics::{self, Color, PxScale, Text};

#[derive(Clone, PartialEq, Debug, Default)]
pub struct Description(String);

impl Merge for Description {
    type Output = Description;

    fn merge(self, other: Description) -> Self::Output {
        if self.0.is_empty() {
            other
        } else if other.0.is_empty() {
            self
        } else {
            Description(self.0 + ", " + &other.0)
        }
    }
}

impl Merge for Option<Description> {
    type Output = Option<Description>;

    fn merge(self, other: Option<Description>) -> Self::Output {
        if let Some(x) = self {
            if let Some(y) = other {
                Some(x.merge(y))
            } else {
                Some(x)
            }
        } else {
            other
        }
    }
}

impl Description {
    pub fn new<S: Into<String>>(string: S) -> Self {
        Self(string.into())
    }

    pub fn into_text(
        self,
        color: Option<Color>,
        font: Font,
        size: point::Coord,
    ) -> Text {
        Text::new(graphics::TextFragment {
            text: self.0,
            color,
            font: font.regular,
            scale: Some(PxScale::from(f32::lossy_from(size))),
        })
    }
}
