use crate::game::client::{color::Color, tile};

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
pub struct Glyph {
    pub index: tile::sheet::Index,
    pub color: Color,
}

impl Default for Glyph {
    fn default() -> Self {
        Glyph::opaque(16)
    }
}

impl Glyph {
    pub fn opaque(index: tile::sheet::Index) -> Self {
        Glyph {
            index,
            color: Color::opaque(1.0, 1.0, 1.0),
        }
    }

    pub fn transparent(index: tile::sheet::Index, alpha: f32) -> Self {
        Glyph {
            index,
            color: Color::transparent(1.0, 1.0, 1.0, alpha),
        }
    }
}
