use super::Merge;
use std::cmp::{max, Ord, Ordering};

pub type Priority = u8;

#[derive(Default, Debug, Clone, Copy)]
pub struct Attribute<T> {
    pub value: T,
    pub priority: Priority,
}

pub type MergeableAttribute<T> = Option<Attribute<T>>;
pub type StackableAttribute<T> = Vec<Attribute<T>>;

impl<T: Copy> Attribute<T> {
    pub fn new(value: T, priority: Priority) -> Self {
        Attribute { value, priority }
    }

    pub fn mergeable(value: T, priority: Priority) -> MergeableAttribute<T> {
        Some(Self::new(value, priority))
    }

    pub fn stackable(
        values: &[T],
        priority: Priority,
    ) -> StackableAttribute<T> {
        values
            .iter()
            .copied()
            .map(|value| Self::new(value, priority))
            .collect()
    }
}

impl<T: Ord> Ord for Attribute<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        let pri_cmp = self.priority.cmp(&other.priority);
        if pri_cmp != Ordering::Equal {
            pri_cmp
        } else {
            self.value.cmp(&other.value)
        }
    }
}

impl<T: Ord> PartialOrd for Attribute<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let pri_cmp = self.priority.cmp(&other.priority);
        if pri_cmp != Ordering::Equal {
            Some(pri_cmp)
        } else {
            Some(self.value.cmp(&other.value))
        }
    }
}

impl<T: Eq> PartialEq for Attribute<T> {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value && self.priority == other.priority
    }
}

impl<T: Eq> Eq for Attribute<T> {}

impl<T: Ord> Merge for MergeableAttribute<T> {
    type Output = Self;

    fn merge(self, other: Self) -> Self {
        if let Some(x) = self {
            if let Some(y) = other {
                Some(max(x, y))
            } else {
                Some(x)
            }
        } else {
            other
        }
    }
}

impl<T: Ord> Merge for StackableAttribute<T> {
    type Output = Self;

    fn merge(mut self, mut other: Self) -> Self {
        self.append(&mut other);
        self.sort_unstable();
        self
    }
}

