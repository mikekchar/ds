use crate::game::client::color::Color;

/// The font weight to use when drawing the `Symbol` string.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub enum Weight {
    Regular,
    Bold,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub struct Char<'a> {
    pub string: &'a str,
    pub weight: Weight,
    pub color: Color,
}

impl<'a> Default for Char<'a> {
    /// Default when drawing nothing.
    fn default() -> Self {
        Self {
            string: "",
            color: Color::opaque(1.0, 1.0, 1.0),
            weight: Weight::Regular,
        }
    }
}

impl<'a> Char<'a> {
    pub fn regular(string: &'a str, r: f32, g: f32, b: f32, a: f32) -> Self {
        Self {
            string,
            color: Color::transparent(r, g, b, a),
            weight: Weight::Regular,
        }
    }

    pub fn bold(string: &'a str, r: f32, g: f32, b: f32, a: f32) -> Self {
        Self {
            string,
            color: Color::transparent(r, g, b, a),
            weight: Weight::Bold,
        }
    }
}

