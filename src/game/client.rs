//! # Chibi Shiro Client
//!
//! This module contains the code for
//! doing the client rendering of the game.

pub mod color;
pub mod context;
pub mod event;
pub mod font;
pub mod image;
pub mod menu;
pub mod point;
pub mod rect;
pub mod text_cache;
pub mod tile;
pub mod time;
pub mod window;
