use crate::game::layer;

use super::Population;
use std::collections::HashMap;
/// Respresents the condition of the things in a tile
#[derive(Debug, Copy, Clone)]
pub enum Condition {
    Target,
    RememberedTarget,
}

/// Contains a collection of conditions created by dwarfs
#[derive(Debug, Default)]
pub struct Conditions(HashMap<layer::Location, Vec<Condition>>);

impl Conditions {
    pub fn new(population: &Population) -> Self {
        let mut hash_map = HashMap::default();
        for dwarf in population.iter() {
            dwarf
                .destination()
                .filter(|_| dwarf.is_alive())
                .map(|dest| {
                    let target = if !dwarf.is_wandering() {
                        Condition::RememberedTarget
                    } else {
                        Condition::Target
                    };

                    hash_map.insert(dest, vec![target]).as_mut().map(
                        |existing| {
                            existing.push(target);
                            hash_map.insert(dest, existing.clone());
                        },
                    )
                });
        }
        Conditions(hash_map)
    }

    pub fn get(&self, location: layer::Location) -> Option<Condition> {
        self.0
            .get(&location)
            .and_then(|dwarfs| dwarfs.first().copied())
    }
}
