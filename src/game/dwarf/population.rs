//! Code for representing a population of dwarfs

use super::{Dwarf, Id, Locations, State};
use crate::game::{
    command::Command,
    event::Event,
    layer::{self, Layer},
    location,
};

/// Contains a collection of dwarfs
#[derive(Debug, Default)]
pub struct Population {
    dwarfs: Vec<Dwarf>,
    max_id: Id,
    pub next_spawn_location: Option<layer::Location>,
}

impl Population {
    pub fn queue_spawn_location(&mut self, layer: &mut Layer) {
        if self.next_spawn_location.is_none() {
            self.next_spawn_location = layer.random_pathable_location()
        }
    }

    /// Add a dwarf do the population.
    /// You probably don't want to call this.  Use `add_dwarf` to
    /// create new dwarfs.  Only pushes when the dwarf is not already
    /// part of a population (i.e. their id is set).
    pub fn push(&mut self, mut dwarf: Dwarf) -> bool {
        if dwarf.id.is_none() {
            // TODO Check to ensure that it doesn't overflow
            self.max_id += 1;
            dwarf.id = Some(self.max_id);
            self.dwarfs.push(dwarf);
            true
        } else {
            false
        }
    }

    /// Mutable iterator for the population.  Iterates over `&mut
    /// Dwarf`.
    pub fn iter_mut(&mut self) -> std::slice::IterMut<'_, Dwarf> {
        self.into_iter()
    }

    /// Iterator for the population.  Iterates over `& Dwarf`.
    pub fn iter(&self) -> std::slice::Iter<'_, Dwarf> {
        self.into_iter()
    }

    /// Returns a reference to the dwarf at an index into the vector
    /// if it exists.
    pub fn get(&mut self, index: location::coord::Internal) -> Option<&Dwarf> {
        self.dwarfs.get(index)
    }

    /// Returns a mutable reference to the dwarf at an index if it
    /// exists.
    pub fn get_mut(
        &mut self,
        index: location::coord::Internal,
    ) -> Option<&mut Dwarf> {
        self.dwarfs.get_mut(index)
    }

    /// Creates a new dwarf. Returns true if the dwarf was created, or
    /// false if a dwarf can not be created at that location.
    pub fn add_dwarf(
        &mut self,
        location: layer::Location,
        layer: &mut Layer,
    ) -> bool {
        let index = location.index(layer.width);

        if index >= location::coord::Internal::from(layer.width * layer.height)
            || !layer.pathable(location)
        {
            false
        } else {
            let dwarf = Dwarf::new(location);
            self.push(dwarf)
        }
    }

    /// Randomly spawn a dwarf at a random pathable location.
    /// Returns true if the dwarf was spawned (3% chance).  False
    /// otherwise.
    fn spawn_dwarf(&mut self, layer: &mut Layer) -> bool {
        if let Some(location) = self.next_spawn_location {
            self.add_dwarf(location, layer);
            self.next_spawn_location = None;
            true
        } else {
            self.next_spawn_location = layer.dwarf_spawn_location(3);
            false
        }
    }

    /// Returns true if the population is empty or composed only of
    /// ghosts
    fn no_visible_dwarfs(&self) -> bool {
        self.dwarfs
            .iter()
            .all(|dwarf| dwarf.status.state == State::Ghost)
    }

    /// Indicate that a game tick has passed. Update the population
    /// state. Returns true if the state has changed, otherwise
    /// false.
    pub fn tick(
        &mut self,
        events: &mut Vec<Event>,
        commands: &mut Vec<Command>,
        layer: &mut Layer,
    ) -> bool {
        if self.no_visible_dwarfs() {
            self.spawn_dwarf(layer)
        } else {
            self.iter_mut().fold(false, |d, dwarf| {
                // FIXME: Return updated from these functions
                dwarf.apply_command(commands);
                dwarf.apply_events(events);
                dwarf.tick(events, layer) || d
            })
        }
    }

    pub fn locations(&self) -> Locations {
        Locations::new(self)
    }

    pub fn active_dwarf_location(&self) -> Option<layer::Location> {
        self.dwarfs
            .iter()
            .find(|dwarf| dwarf.is_alive())
            .map(|dwarf| dwarf.location())
    }
}

impl<'a> IntoIterator for &'a Population {
    type Item = &'a Dwarf;
    type IntoIter = std::slice::Iter<'a, Dwarf>;

    fn into_iter(self) -> Self::IntoIter {
        self.dwarfs.iter()
    }
}

impl<'a> IntoIterator for &'a mut Population {
    type Item = &'a mut Dwarf;
    type IntoIter = std::slice::IterMut<'a, Dwarf>;

    fn into_iter(self) -> Self::IntoIter {
        self.dwarfs.iter_mut()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::{dwarf::Plan, test_setup::Setup, trail::Trail};

    #[test]
    fn test_population_iter() {
        let mut setup = Setup::default();
        setup.with_dwarf((0, 0));
        setup.with_dwarf((1, 1));

        let locations: Vec<layer::Location> =
            setup.population.into_iter().map(|d| d.location()).collect();
        assert_eq!(locations.get(0).copied(), Some(layer::Location::new(0, 0)));
        assert_eq!(locations.get(1).copied(), Some(layer::Location::new(1, 1)));
    }

    #[test]
    fn test_add_dwarf() {
        let mut setup = Setup::default();
        setup.with_room((5, 5), 1);

        assert_eq!(
            setup
                .population
                .add_dwarf(layer::Location::new(100, 1), &mut setup.layer),
            false
        );
        assert_eq!(
            setup
                .population
                .add_dwarf(layer::Location::new(1, 100), &mut setup.layer),
            false
        );

        // There is a room at 5, 5
        assert_eq!(
            setup
                .population
                .add_dwarf(layer::Location::new(5, 5), &mut setup.layer),
            true
        );

        // But there is no room at 4, 4
        assert_eq!(
            setup
                .population
                .add_dwarf(layer::Location::new(4, 4), &mut setup.layer),
            false
        );
    }

    #[test]
    fn test_tick() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_dwarf((2, 2));
        let dwarf = setup.population.get_mut(0).unwrap();
        dwarf.plan = None;
        let layer = &mut setup.layer;
        let commands = &mut vec![];

        setup.population.get_mut(0).unwrap().speed = 1.0;

        // Stepping with no (or empty) trail creates a new one which
        // must be recorded as an update
        let mut events: Vec<Event> = Default::default();
        setup.population.get_mut(0).unwrap().plan = None;
        assert_eq!(setup.population.tick(&mut events, commands, layer), true);
        assert_eq!(
            setup.population.get_mut(0).unwrap().trail().is_some(),
            true
        );
        assert_eq!(events.len(), 0);

        let mut events: Vec<Event> = Default::default();
        setup.population.get_mut(0).unwrap().plan =
            Some(Plan::wander(Some(Trail::new())));
        assert_eq!(setup.population.tick(&mut events, commands, layer), true);
        assert_eq!(
            setup
                .population
                .get_mut(0)
                .unwrap()
                .trail()
                .unwrap()
                .is_empty(),
            false
        );
        assert_eq!(events.len(), 0);

        // Stepping with a non-empty trail should be dirty
        let mut events: Vec<Event> = Default::default();
        setup.population.get_mut(0).unwrap().plan =
            Some(Plan::wander(Some(Trail::new())));
        setup
            .population
            .get_mut(0)
            .unwrap()
            .trail_mut()
            .unwrap()
            .push_back(layer::Location::new(2, 3));
        assert_eq!(setup.population.tick(&mut events, commands, layer), true);
        assert_eq!(events.len(), 0);

        // Step eats a step, so put another location in the trail
        setup
            .population
            .get_mut(0)
            .unwrap()
            .trail_mut()
            .unwrap()
            .push_back(layer::Location::new(3, 3));
        // Add another dwarf without a trail
        assert_eq!(
            setup
                .population
                .add_dwarf(layer::Location::new(2, 2), layer),
            true
        );
        assert_eq!(events.len(), 0);

        // Because one dwarf moves, step should return dirty
        let mut events: Vec<Event> = Default::default();
        assert_eq!(setup.population.tick(&mut events, commands, layer), true);
        assert_eq!(events.len(), 0);
    }
}
