//! Code for representing the dwarfs on a map

use super::{Dwarf, Population};
use crate::game::layer;
use std::collections::HashMap;

/// Contains a collection of dwarfs
#[derive(Debug, Default)]
pub struct Locations<'a>(HashMap<layer::Location, Vec<&'a Dwarf>>);

impl<'a> Locations<'a> {
    pub fn new(population: &'a Population) -> Self {
        let mut hash_map = HashMap::default();
        for dwarf in population.iter() {
            if let Some(existing) =
                hash_map.insert(dwarf.location(), vec![dwarf]).as_mut()
            {
                existing.push(dwarf);
                hash_map.insert(dwarf.location(), existing.clone());
            };
        }
        Locations(hash_map)
    }

    pub fn get(&self, location: layer::Location) -> Option<&[&'a Dwarf]> {
        self.0.get(&location).map(|dwarfs| &dwarfs[..])
    }
}
