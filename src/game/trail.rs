//! Code for representing a path a dwarf can follow on a layer
use crate::game::{
    a_star,
    dwarf::Status,
    layer::{self, Layer},
    location,
    location::Direction,
};
use std::collections::{linked_list::Iter, HashMap, LinkedList};

#[derive(Debug, Default)]
pub struct Trail(LinkedList<layer::Location>);

impl Trail {
    pub fn new() -> Self {
        Trail(LinkedList::new())
    }

    pub fn build(
        goal: layer::Location,
        came_from: &HashMap<layer::Location, layer::Location>,
    ) -> Self {
        let mut trail = Self::new();
        trail.push_front(goal);

        let mut current = goal;
        while let Some(previous) = came_from.get(&current).cloned() {
            if current == previous {
                break;
            }
            trail.push_front(previous);
            current = previous;
        }

        // Trail build includes the start node.  We need to remove it
        // because we don't path to the start node.
        trail.pop_front();

        trail
    }

    /// Return a trail from a location to a random pathable location
    /// Returns None if there are no pathable locations or if it
    /// randomly selected `from` as the goal.
    /// layer needs to be `mut` because we are using its RNG to
    /// determine the random location.
    pub fn wander(from: layer::Location, layer: &mut Layer) -> Option<Self> {
        layer
            .random_pathable_location()
            .map(|goal| a_star::create_trail(from, goal, layer))
            .filter(|trail| !trail.is_empty())
    }

    pub fn path_to(
        from: layer::Location,
        goal: layer::Location,
        layer: &Layer,
    ) -> Option<Self> {
        Some(a_star::create_trail(from, goal, layer))
            .filter(|trail| !trail.is_empty())
    }

    pub fn push_front(&mut self, location: layer::Location) {
        self.0.push_front(location);
    }

    pub fn push_back(&mut self, location: layer::Location) {
        self.0.push_back(location);
    }

    pub fn pop_front(&mut self) -> Option<layer::Location> {
        self.0.pop_front()
    }

    pub fn front(&self) -> Option<&layer::Location> {
        self.0.front()
    }

    pub fn back(&self) -> Option<&layer::Location> {
        self.0.back()
    }

    pub fn len(&self) -> location::coord::Internal {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.len() == 0
    }

    pub fn iter(&self) -> Iter<layer::Location> {
        self.0.iter()
    }

    // Returns the location of the next step in the trail if pathable
    // Pops the location off the trail.
    pub fn step(&mut self, layer: &Layer) -> Option<layer::Location> {
        self.pop_front()
            .filter(|location| layer.pathable(*location))
    }

    pub fn facing(&self, location: layer::Location) -> Direction {
        self.front()
            .map(|dest| location.direction_to(dest))
            .unwrap_or(Direction::Down)
    }

    // Returns the location of the nth step in the trail if pathable
    // Pops the locations off the trail
    pub fn walk(
        &mut self,
        start: Status,
        speed: f32,
        layer: &Layer,
    ) -> Option<Status> {
        let (steps, progress) = start.steps(speed);
        let end_location = if steps > 0 {
            std::iter::repeat(())
                .take(steps)
                .flat_map(|_| self.step(layer))
                .last()
        } else {
            Some(start.location)
        };
        end_location.map(|location| Status {
            location,
            progress,
            facing: self.facing(location),
            ..start
        })
    }

    pub fn destination(&self) -> Option<layer::Location> {
        self.back().copied()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::dwarf::{Observation, State};

    struct Setup {
        pub layer: Layer,
        pub trail: Trail,
    }

    impl Default for Setup {
        fn default() -> Self {
            let seed = "test_seed";
            let height = 10;
            let width = 10;

            let layer = Layer::new(width, height, seed);
            let trail = Trail::new();

            Self { layer, trail }
        }
    }

    impl Setup {
        fn with_room(
            &mut self,
            upper_left: (location::coord::Internal, location::coord::Internal),
            size: location::coord::Internal,
        ) {
            self.layer.map.add_room(
                layer::Location::new(upper_left.0, upper_left.1),
                size,
                size,
            );
        }
    }

    #[test]
    fn test_delegation() {
        let setup = Setup::default();
        let mut trail = setup.trail;

        assert_eq!(trail.len(), 0);

        trail.push_back(layer::Location::new(0, 0));
        trail.push_back(layer::Location::new(0, 1));
        trail.push_back(layer::Location::new(1, 1));
        assert_eq!(trail.len(), 3);

        let expected = vec![
            layer::Location::new(0, 0),
            layer::Location::new(0, 1),
            layer::Location::new(1, 1),
        ];
        assert_eq!(trail.iter().copied().collect::<Vec<_>>(), expected);
    }

    #[test]
    fn test_build() {
        let mut came_from: HashMap<layer::Location, layer::Location> =
            HashMap::new();

        // When came_from is empty
        let mut trail = Trail::build(layer::Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<layer::Location>>(),
            vec![]
        );

        // With a trail of 1
        came_from
            .insert(layer::Location::new(10, 10), layer::Location::new(9, 9));
        trail = Trail::build(layer::Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<layer::Location>>(),
            vec![layer::Location::new(10, 10)]
        );

        // With a trail of 2
        came_from
            .insert(layer::Location::new(9, 9), layer::Location::new(9, 8));
        trail = Trail::build(layer::Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<layer::Location>>(),
            vec![layer::Location::new(9, 9), layer::Location::new(10, 10)]
        );

        // With a disconnected node
        came_from
            .insert(layer::Location::new(2, 2), layer::Location::new(1, 1));
        trail = Trail::build(layer::Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<layer::Location>>(),
            vec![layer::Location::new(9, 9), layer::Location::new(10, 10)]
        );
    }

    #[test]
    fn step() {
        let mut setup = Setup::default();
        setup.with_room((5, 5), 3);
        let mut trail = setup.trail;
        let layer = setup.layer;

        // With an empty trail
        assert_eq!(trail.step(&layer), None);
        assert_eq!(trail.is_empty(), true);

        trail.push_back(layer::Location::new(5, 5));
        trail.push_back(layer::Location::new(5, 6));
        trail.push_back(layer::Location::new(5, 7));

        assert_eq!(trail.step(&layer), Some(layer::Location::new(5, 5)));
        assert_eq!(trail.is_empty(), false);
        assert_eq!(trail.step(&layer), Some(layer::Location::new(5, 6)));
        assert_eq!(trail.step(&layer), Some(layer::Location::new(5, 7)));
        assert_eq!(trail.is_empty(), true);
        assert_eq!(trail.step(&layer), None);
        assert_eq!(trail.is_empty(), true);

        trail.push_back(layer::Location::new(5, 5));
        trail.push_back(layer::Location::new(5, 6));
        trail.push_back(layer::Location::new(5, 7));
        trail.push_back(layer::Location::new(5, 8));

        assert_eq!(trail.step(&layer), Some(layer::Location::new(5, 5)));
        assert_eq!(trail.step(&layer), Some(layer::Location::new(5, 6)));
        assert_eq!(trail.step(&layer), Some(layer::Location::new(5, 7)));
        // 5, 8 is not passable (it's in a wall)
        assert_eq!(trail.step(&layer), None);
    }

    #[test]
    fn walk() {
        let mut setup = Setup::default();
        setup.with_room((5, 4), 3);
        let mut trail = setup.trail;
        let layer = setup.layer;
        let start = Status {
            location: layer::Location::new(5, 4),
            progress: 0.0,
            facing: Direction::Down,
            state: State::Living,
            observation: Observation::Nothing,
        };

        // With an empty trail
        assert_eq!(trail.walk(start, 1.0, &layer), None);
        assert_eq!(trail.is_empty(), true);

        trail.push_back(layer::Location::new(5, 5));
        trail.push_back(layer::Location::new(5, 6));

        let end1 = Status {
            location: layer::Location::new(5, 6),
            progress: 0.0,
            facing: Direction::Down,
            state: State::Living,
            observation: Observation::Nothing,
        };
        assert_eq!(trail.walk(start, 2.0, &layer), Some(end1));
        assert_eq!(trail.is_empty(), true);

        trail.push_back(layer::Location::new(5, 5));
        trail.push_back(layer::Location::new(5, 6));
        trail.push_back(layer::Location::new(5, 7));

        let end2 = Status {
            location: layer::Location::new(5, 5),
            progress: 0.5,
            facing: Direction::Down,
            state: State::Living,
            observation: Observation::Nothing,
        };
        assert_eq!(trail.walk(start, 1.5, &layer), Some(end2));
        assert_eq!(trail.is_empty(), false);

        let end3 = Status {
            location: layer::Location::new(5, 6),
            progress: 0.0,
            facing: Direction::Down,
            state: State::Living,
            observation: Observation::Nothing,
        };
        assert_eq!(trail.walk(end2, 0.5, &layer), Some(end3));
        assert_eq!(trail.is_empty(), false);

        assert_eq!(trail.walk(end3, 1.0, &layer), None);
        assert_eq!(trail.is_empty(), true);
    }

    #[test]
    fn facing() {
        let setup = Setup::default();
        let mut trail = setup.trail;
        let location = layer::Location::new(1, 1);

        // With an empty trail, face down
        assert_eq!(trail.facing(location), Direction::Down);

        // Otherwise it faces in the direction of the first location
        // on the trail
        trail.push_back(layer::Location::new(1, 0));
        assert_eq!(trail.facing(location), Direction::Up);
    }
}
