use super::{layer, location};

use std::collections::HashMap;

#[derive(Default)]
pub struct DistanceMap(HashMap<layer::Location, location::Coord>);

impl DistanceMap {
    pub fn insert(
        &mut self,
        location: layer::Location,
        distance: location::Coord,
    ) {
        self.0.insert(location, distance);
    }

    pub fn get(&self, location: layer::Location) -> location::Coord {
        self.0
            .get(&location)
            .copied()
            .unwrap_or_else(location::Coord::max_value)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert() {
        let mut map = DistanceMap::default();

        // If the location doesn't exist we should get max_value
        assert_eq!(
            map.get(layer::Location::new(1, 1)),
            location::Coord::max_value()
        );

        map.insert(layer::Location::new(1, 1), 42.into());
        assert_eq!(
            map.get(layer::Location::new(1, 1)),
            location::Coord::from(42)
        );
    }
}
