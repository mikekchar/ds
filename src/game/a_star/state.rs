use std::collections::{HashMap, HashSet};

use super::{
    candidate_queue::CandidateQueue,
    distance_map::DistanceMap,
    layer::{self, Layer},
    location,
    trail::Trail,
};

pub struct State<'layer> {
    goal: layer::Location,
    open: CandidateQueue,
    start_distances: DistanceMap,
    closed: HashSet<layer::Location>,
    came_from: HashMap<layer::Location, layer::Location>,
    layer: &'layer Layer,
}

impl<'layer> Iterator for State<'layer> {
    type Item = layer::Location;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(current) = self.next_candidate() {
            if current == self.goal {
                return None;
            }

            self.checked(current);

            if let Some(location) = self.layer.contained_location(current) {
                for neighbour in location.into_surrounding_iter(self.layer) {
                    if !self.is_checked(neighbour) {
                        if let Some(distance) =
                            self.closer_distance(current, neighbour, self.layer)
                        {
                            // FIXME: Remove the node from the open set if it
                            // exists
                            self.link(current, neighbour, distance);
                        }
                    }
                }
                Some(location)
            } else {
                None
            }
        } else {
            None
        }
    }
}

impl<'layer> State<'layer> {
    pub fn new(goal: layer::Location, layer: &'layer Layer) -> Self {
        State {
            goal,
            open: CandidateQueue::default(),
            start_distances: DistanceMap::default(),
            closed: HashSet::new(),
            came_from: HashMap::new(),
            layer,
        }
    }

    fn add_candidate(
        &mut self,
        location: layer::Location,
        distance_to_start: location::Coord,
    ) {
        self.open.push(
            location,
            distance_to_start,
            location.distance_to(&self.goal),
        );
        self.start_distances.insert(location, distance_to_start);
    }

    pub fn start(&mut self, location: layer::Location) {
        self.add_candidate(location, 0.into());
    }

    pub fn link(
        &mut self,
        from: layer::Location,
        to: layer::Location,
        distance: location::Coord,
    ) {
        self.add_candidate(to, distance);
        // came_from is linked from destination to source
        self.came_from.insert(to, from);
    }

    pub fn next_candidate(&mut self) -> Option<layer::Location> {
        self.open.pop()
    }

    // If location hasn't been stored, return max_value
    pub fn stored_distance(
        &self,
        location: layer::Location,
    ) -> location::Coord {
        self.start_distances.get(location)
    }

    // We have finished checking this location for a viable path
    pub fn checked(&mut self, location: layer::Location) {
        self.closed.insert(location);
    }

    pub fn is_checked(&self, location: layer::Location) -> bool {
        self.closed.contains(&location)
    }

    // Trail will be empty unless we reach the goal
    pub fn build_trail(&self) -> Trail {
        Trail::build(self.goal, &self.came_from)
    }

    pub fn closer_distance(
        &self,
        current: layer::Location,
        neighbour: layer::Location,
        layer: &Layer,
    ) -> Option<location::Coord> {
        // If current hasn't been previously stored, new_distance will be
        // max_value
        let new_distance =
            self.stored_distance(current) + layer.path_cost(current, neighbour);

        if new_distance < self.stored_distance(neighbour) {
            Some(new_distance)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::test_setup::Setup;

    #[test]
    fn add_candidate_adds_location_to_open_queue() {
        let layer = &Setup::new(10, 10, 1).layer;
        let goal = layer::Location::new(10, 10);
        let start = layer::Location::new(0, 0);
        let mut state = State::new(goal, layer);

        state.add_candidate(layer::Location::new(0, 0), 0.into());
        assert_eq!(state.next_candidate(), Some(start));
        assert_eq!(state.next_candidate(), None);
    }

    #[test]
    fn add_candidate_stores_distances() {
        let layer = &Setup::new(10, 10, 1).layer;
        let goal = layer::Location::new(10, 10);
        let start = layer::Location::new(0, 0);
        let mut state = State::new(goal, layer);

        // If no distance is stored, it returns max value
        assert_eq!(state.stored_distance(start), location::Coord::max_value());

        // Adding the candidate stores the given distance
        state.add_candidate(start, 0.into());
        assert_eq!(state.stored_distance(start), location::Coord::from(0));

        // Removing the candidate leaves the distance in the hash
        assert_eq!(state.next_candidate(), Some(start));
        assert_eq!(state.next_candidate(), None);
        assert_eq!(state.stored_distance(start), location::Coord::from(0));
    }

    #[test]
    fn checked() {
        let layer = &Setup::new(10, 10, 1).layer;
        let goal = layer::Location::new(10, 10);
        let location = layer::Location::new(0, 0);
        let mut state = State::new(goal, layer);

        assert_eq!(state.is_checked(location), false);
        state.checked(location);
        assert_eq!(state.is_checked(location), true);
    }

    #[test]
    fn link() {
        let layer = &Setup::new(10, 10, 1).layer;
        let goal = layer::Location::new(1, 1);
        let start = layer::Location::new(0, 0);
        let location1 = layer::Location::new(1, 0);
        let mut state = State::new(goal, layer);

        assert_eq!(state.build_trail().is_empty(), true);

        // build_trail only builds a trail if we manage to reach the goal
        state.start(start);
        assert_eq!(state.build_trail().is_empty(), true);
        state.link(start, location1, 1.into());
        assert_eq!(state.build_trail().is_empty(), true);
        state.link(location1, goal, 1.into());

        // The trail doesn't include start
        assert_eq!(
            state
                .build_trail()
                .iter()
                .copied()
                .collect::<Vec<layer::Location>>(),
            vec![location1, goal]
        );
    }
}
