use std::cmp::Ordering;

use super::{layer, location};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Node {
    pub location: layer::Location,
    pub score: location::Coord,
}

impl Node {
    pub fn new(
        location: layer::Location,
        score: impl Into<location::Coord>,
    ) -> Self {
        Node {
            location,
            score: score.into(),
        }
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other
            .score
            .cmp(&self.score)
            .then_with(|| other.location.cmp(&self.location))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cmp() {
        let node = Node::new(layer::Location::new(1, 1), 42);
        let lesser = Node::new(layer::Location::new(0, 0), 12);
        let greater = Node::new(layer::Location::new(0, 0), 52);
        let equal_score_less = Node::new(layer::Location::new(0, 0), 42);
        let equal_score_greater = Node::new(layer::Location::new(2, 2), 42);

        assert_eq!(node.cmp(&lesser), Ordering::Less);
        assert_eq!(node.cmp(&greater), Ordering::Greater);
        assert_eq!(node.cmp(&equal_score_less), Ordering::Less);
        assert_eq!(node.cmp(&equal_score_greater), Ordering::Greater);
        assert_eq!(node.cmp(&node), Ordering::Equal);
    }
}
