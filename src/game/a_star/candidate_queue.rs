use super::{layer, location, node::Node};
use std::collections::BinaryHeap;

#[derive(Default)]
pub struct CandidateQueue(BinaryHeap<Node>);

impl CandidateQueue {
    pub fn score(
        distance_to_start: location::Coord,
        distance_to_goal: location::Coord,
    ) -> location::Coord {
        distance_to_start + distance_to_goal
    }

    pub fn push(
        &mut self,
        location: layer::Location,
        distance_to_start: location::Coord,
        distance_to_goal: location::Coord,
    ) {
        self.0.push(Node::new(
            location,
            Self::score(distance_to_start, distance_to_goal),
        ));
    }

    pub fn pop(&mut self) -> Option<layer::Location> {
        self.0.pop().map(|node| node.location)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_candidate_queue_score() {
        let location = layer::Location::new(0, 0);
        let goal = layer::Location::new(0, 0);
        assert_eq!(
            CandidateQueue::score(0.into(), location.distance_to(&goal)),
            0.into()
        );

        let location = layer::Location::new(5, 0);
        let goal = layer::Location::new(10, 0);
        assert_eq!(
            CandidateQueue::score(5.into(), location.distance_to(&goal)),
            10.into()
        );

        let location = layer::Location::new(0, 0);
        let goal = layer::Location::new(location::Coord::max_value(), 0);

        assert_eq!(
            CandidateQueue::score(5.into(), location.distance_to(&goal)),
            location::Coord::max_value()
        );
    }

    #[test]
    fn push_and_pop() {
        let location1 = layer::Location::new(2, 4);
        let location2 = layer::Location::new(3, 5);
        let mut queue = CandidateQueue::default();

        queue.push(location1, 5.into(), 10.into());
        queue.push(location2, 7.into(), 8.into());
        assert_eq!(queue.pop(), Some(location1));
        assert_eq!(queue.pop(), Some(location2));
        // Should be empty now
        assert_eq!(queue.pop(), None);
    }
}
