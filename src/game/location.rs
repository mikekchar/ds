//! Code for dealing with locations on a grid,
//! whether it be the `Grid` display or the map `Layer`

pub mod coord;
pub mod direction;
mod offset;

use crate::game::coord::{CheckedOps, LossyFrom};
pub use coord::Coord;
pub use direction::Direction;
pub use offset::Offset;
use std::{
    cmp::{max, min},
    hash::Hash,
};

/// Location on the `Grid` display or map `Layer`
#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash, PartialOrd, Ord)]
pub struct Location {
    pub x: Coord,
    pub y: Coord,
}

fn difference(a: Coord, b: Coord) -> Coord {
    if a > b {
        a - b
    } else {
        b - a
    }
}

impl std::fmt::Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

// You can only add Offsets to Locations
// If you want to add or subtract Locations, use
// Offset::lossy_from(my_location)
impl CheckedOps<Offset> for Location {
    /// Try to add an offset to a location.
    /// Returns `None` if the result would overflow.
    fn checked_add(self, other: Offset) -> Option<Self> {
        Offset::lossy_from(self)
            .checked_add(other)
            .and_then(Location::from_offset)
    }

    /// Try to subtract an offset from a location.
    /// Returns `None` if the result would be less than zero.
    fn checked_sub(self, other: Offset) -> Option<Self> {
        Offset::lossy_from(self)
            .checked_sub(other)
            .and_then(Location::from_offset)
    }
}

impl Location {
    /// Create a `Location` for the `Grid` display or map `Layer`
    pub fn new(x: impl Into<Coord>, y: impl Into<Coord>) -> Self {
        Location {
            x: x.into(),
            y: y.into(),
        }
    }

    /// Converts an Offset into a location if it is positive
    pub fn from_offset(offset: Offset) -> Option<Self> {
        if offset.x >= 0 && offset.y >= 0 {
            Some(Self::new(
                offset.x as coord::Internal,
                offset.y as coord::Internal,
            ))
        } else {
            None
        }
    }

    /// Calculate the offset from another location
    pub fn offset_from(&self, other: &Location) -> Option<Offset> {
        Offset::lossy_from(*self).checked_sub(Offset::lossy_from(*other))
    }

    /// Returns the index into a grid for a breadth-first traversal
    /// width is the width of the grid
    pub fn index(&self, width: impl Into<Coord>) -> coord::Internal {
        coord::Internal::from(self.y * width.into() + self.x)
    }

    pub fn distance_to(&self, goal: &Location) -> Coord {
        let x = f64::lossy_from(difference(self.x, goal.x));
        let y = f64::lossy_from(difference(self.y, goal.y));

        let result = x.mul_add(x, y * y).sqrt().round();
        if result >= f64::lossy_from(Coord::max_value()) {
            Coord::max_value()
        } else {
            (result as coord::Internal).into()
        }
    }

    pub fn direction_to(&self, dest: &Location) -> Direction {
        let horiz_dist = max(self.x, dest.x) - min(self.x, dest.x);
        let vert_dist = max(self.y, dest.y) - min(self.y, dest.y);
        if horiz_dist > vert_dist {
            if self.x <= dest.x {
                Direction::Right
            } else {
                Direction::Left
            }
        } else if self.y <= dest.y {
            Direction::Down
        } else {
            Direction::Up
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cmp::Ordering;

    #[test]
    fn test_index() {
        assert_eq!(Location::new(0, 0).index(10), 0);
        assert_eq!(Location::new(1, 0).index(10), 1);
        assert_eq!(Location::new(2, 0).index(10), 2);
        assert_eq!(Location::new(9, 0).index(10), 9);
        assert_eq!(Location::new(0, 1).index(10), 10);
        assert_eq!(Location::new(1, 1).index(10), 11);
    }

    #[test]
    fn index_middle() {
        let location = Location::new(0, 3);
        assert_eq!(location.index(10), 30);
    }

    #[test]
    fn test_distance_to() {
        assert_eq!(
            Location::new(1, 1).distance_to(&Location::new(1, 1)),
            0.into()
        );
        assert_eq!(
            Location::new(1, 1).distance_to(&Location::new(1, 2)),
            1.into()
        );
        assert_eq!(
            Location::new(1, 1).distance_to(&Location::new(2, 2)),
            Coord::from(2.0_f64.sqrt().round() as coord::Internal)
        );
        assert_eq!(
            Location::new(1, 1).distance_to(&Location::new(3, 4)),
            Coord::from(13.0_f64.sqrt().round() as coord::Internal)
        );

        assert_eq!(
            Location::new(0, 0)
                .distance_to(&Location::new(0, Coord::max_value())),
            Coord::max_value()
        );
    }

    #[test]
    fn test_offset_neg() {
        let offset = Offset { x: -5, y: 10 };
        assert_eq!(--offset, offset);

        assert_eq!(-Offset { x: -3, y: -2 }, Offset { x: 3, y: 2 });
        assert_eq!(-Offset { x: 3, y: 2 }, Offset { x: -3, y: -2 });
    }

    #[test]
    fn test_ord() {
        assert_eq!(
            Location::new(1, 1).cmp(&Location::new(2, 2)),
            Ordering::Less
        );
        assert_eq!(
            Location::new(2, 1).cmp(&Location::new(2, 2)),
            Ordering::Less
        );
        assert_eq!(
            Location::new(1, 2).cmp(&Location::new(2, 2)),
            Ordering::Less
        );
        assert_eq!(
            Location::new(2, 2).cmp(&Location::new(1, 1)),
            Ordering::Greater
        );
        assert_eq!(
            Location::new(2, 2).cmp(&Location::new(2, 1)),
            Ordering::Greater
        );
        assert_eq!(
            Location::new(2, 2).cmp(&Location::new(1, 2)),
            Ordering::Greater
        );
        assert_eq!(
            Location::new(1, 1).cmp(&Location::new(1, 1)),
            Ordering::Equal
        );
    }

    #[test]
    fn test_checked_sub() {
        // Can't have negative locations
        assert_eq!(Location::new(0, 0).checked_sub(Offset::new(0, 1)), None);
        assert_eq!(Location::new(0, 0).checked_sub(Offset::new(1, 0)), None);
        assert_eq!(
            Location::new(1, 1).checked_sub(Offset::new(0, 1)),
            Some(Location::new(1, 0))
        );
        assert_eq!(
            Location::new(1, 1).checked_sub(Offset::new(1, 0)),
            Some(Location::new(0, 1))
        );
    }

    #[test]
    fn test_checked_add() {
        assert_eq!(
            Location::new(0, 0).checked_add(Offset::new(0, 0)),
            Some(Location::new(0, 0))
        );
        assert_eq!(
            Location::new(0, 0).checked_add(Offset::new(1, 0)),
            Some(Location::new(1, 0))
        );
    }

    #[test]
    fn direction_to() {
        let a = Location::new(1, 1);

        let b = Location::new(1, 0);
        assert_eq!(a.direction_to(&b), Direction::Up);

        let b = Location::new(2, 1);
        assert_eq!(a.direction_to(&b), Direction::Right);

        let b = Location::new(1, 2);
        assert_eq!(a.direction_to(&b), Direction::Down);

        let b = Location::new(0, 1);
        assert_eq!(a.direction_to(&b), Direction::Left);

        let b = Location::new(1, 1);
        assert_eq!(a.direction_to(&b), Direction::Down);

        let b = Location::new(0, 0);
        assert_eq!(a.direction_to(&b), Direction::Up);

        let b = Location::new(2, 0);
        assert_eq!(a.direction_to(&b), Direction::Up);

        let b = Location::new(2, 2);
        assert_eq!(a.direction_to(&b), Direction::Down);

        let b = Location::new(0, 2);
        assert_eq!(a.direction_to(&b), Direction::Down);

        let a = Location::new(5, 5);

        // More left than up
        let b = Location::new(0, 4);
        assert_eq!(a.direction_to(&b), Direction::Left);

        // More right than up
        let b = Location::new(10, 4);
        assert_eq!(a.direction_to(&b), Direction::Right);

        // More up than left
        let b = Location::new(4, 0);
        assert_eq!(a.direction_to(&b), Direction::Up);

        // More down than left
        let b = Location::new(4, 10);
        assert_eq!(a.direction_to(&b), Direction::Down);
    }
}
