//! Code for representing the player

use crate::game::{
    command::Command,
    dwarf::{self, Dwarf},
    event::Event,
    layer::{self, Layer},
    location::Direction,
};
use dwarf::Locations;

pub type Power = usize;

#[derive(Debug, Default)]
pub struct Player {
    pub location: Option<layer::Location>,
    pub contracts: Vec<dwarf::Id>,
    pub power: Power,
    pub following: bool,
}

impl Player {
    pub fn new(location: Option<layer::Location>) -> Self {
        Self {
            location,
            contracts: Default::default(),
            power: Default::default(),
            following: false,
        }
    }

    pub fn walk(&mut self, layer: &Layer, dir: Direction) -> bool {
        let result = self
            .location
            .and_then(|location| layer.try_walk(&location, dir));
        if result.is_some() {
            self.following = false;
            self.location = result
        }
        result.is_some()
    }

    pub fn dwarfs_in<'pop>(
        &self,
        locations: &'pop Locations<'pop>,
    ) -> Option<&'pop [&'pop Dwarf]> {
        self.location.and_then(|location| locations.get(location))
    }

    pub fn focussed_dwarf<'pop>(
        &self,
        locations: &'pop Locations<'pop>,
    ) -> Option<&'pop Dwarf> {
        self.dwarfs_in(locations).and_then(|dwarfs| {
            dwarfs.iter().copied().find(|&dwarf| dwarf.is_alive())
        })
    }

    pub fn mut_focussed_dwarf<'pop>(
        &self,
        locations: &'pop Locations<'pop>,
    ) -> Option<&'pop Dwarf> {
        self.dwarfs_in(locations).and_then(|dwarfs| {
            dwarfs.iter().copied().find(|&dwarf| dwarf.is_alive())
        })
    }

    fn add_contract(&mut self, id: dwarf::Id) {
        self.contracts.push(id);
    }

    fn remove_contract(&mut self, id: dwarf::Id) {
        self.contracts
            .iter()
            .position(|&contract_id| id == contract_id)
            .map(|index| self.contracts.remove(index));
    }

    /// Apply a command resulting from a player (usually UI) action
    /// Commands are things that are generated from real player
    /// interaction with the game. They are usually generated from
    /// the client
    fn apply_command(&mut self, commands: &mut Vec<Command>) -> bool {
        let mut updated = false;

        commands.retain(|command| {
            match command {
                Command::OfferContract(id) => {
                    self.add_contract(*id);
                    updated |= true;
                    true
                } // _ => true,
            }
        });

        updated
    }

    /// Apply the results of game actions to the player
    /// Events are things that happen as a result of the game updating
    /// its state They are part of the game mechanics
    fn apply_event(&mut self, events: &mut Vec<Event>) -> bool {
        let mut updated = false;

        events.retain(|event| {
            match event {
                Event::Death(id) => {
                    self.remove_contract(*id);
                    updated |= true;
                    // Returning false means to remove event
                    // as we have now handled it.
                    false
                }
                Event::DistributePower(_power) => true, // _ => true,
            }
        });

        updated
    }

    pub fn collect_power(&mut self) -> bool {
        self.power += self.contracts.len() * 10;
        false
    }

    pub fn distribute_power(&mut self, events: &mut Vec<Event>) -> bool {
        let num_contracts = self.contracts.len();
        if num_contracts > 0 {
            let distributed = self.power / (10 * num_contracts);
            self.power -= distributed;
            if distributed > 0 {
                events.push(Event::DistributePower(distributed));
            }
        }
        false
    }

    /// Indicate that a game tick has passed. Update the play
    /// state. Returns true if the state has changed, otherwise
    /// false.
    pub fn tick(
        &mut self,
        events: &mut Vec<Event>,
        commands: &mut Vec<Command>,
    ) -> bool {
        let mut updated = false;

        updated |= self.apply_event(events);
        updated |= self.apply_command(commands);
        updated |= self.collect_power();
        updated |= self.distribute_power(events);
        updated
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::location;

    struct Setup {
        pub player: Player,
        pub layer: Layer,
        pub population: dwarf::Population,
    }

    impl Default for Setup {
        fn default() -> Self {
            Self {
                player: Default::default(),
                layer: Layer::new(10, 1, "test seed"),
                population: dwarf::Population::default(),
            }
        }
    }

    impl Setup {
        fn with_room(
            mut self,
            location: (impl Into<location::Coord>, impl Into<location::Coord>),
            size: location::coord::Internal,
        ) -> Self {
            self.layer.map.add_room(
                layer::Location::new(location.0, location.1),
                size,
                size,
            );
            self
        }

        fn with_dwarf(
            mut self,
            location: (impl Into<location::Coord>, impl Into<location::Coord>),
        ) -> Self {
            self.population.add_dwarf(
                layer::Location::new(location.0, location.1),
                &mut self.layer,
            );
            self
        }
    }
    #[test]
    fn walk() {
        let setup = Setup::default().with_room((3, 3), 3).with_dwarf((3, 3));
        let mut player = setup.player;
        player.location = Some(layer::Location::new(0, 0));
        let layer = setup.layer;

        assert_eq!(player.location, Some(layer::Location::new(0, 0)));
        assert_eq!(player.walk(&layer, Direction::Left), false);
        assert_eq!(player.location, Some(layer::Location::new(0, 0)));
        assert_eq!(player.walk(&layer, Direction::Right), true);
        assert_eq!(player.location, Some(layer::Location::new(1, 0)));
    }

    #[test]
    fn dwarfs_in() {
        let setup = Setup::default().with_room((3, 3), 3).with_dwarf((4, 4));
        let mut player = setup.player;

        // When the player is not on the layer
        player.location = None;
        assert!(player.dwarfs_in(&setup.population.locations()).is_none());

        // When the player is on the layer but not on a dwarf
        player.location = Some(layer::Location::new(5, 5));
        assert!(player.dwarfs_in(&setup.population.locations()).is_none());

        // When the player is on the layer and on a dwarf
        player.location = Some(layer::Location::new(4, 4));
        assert!(player.dwarfs_in(&setup.population.locations()).is_some());
    }

    #[test]
    fn add_contract() {
        let setup = Setup::default();
        let mut player = setup.player;
        let id = 1;

        player.add_contract(id);
        assert_eq!(player.contracts.len(), 1);
        assert_eq!(player.contracts.get(0), Some(&id));
    }

    #[test]
    fn remove_contract() {
        let setup = Setup::default();
        let mut player = setup.player;
        let id = 1;

        // When the contract list is empty
        player.remove_contract(id);
        assert_eq!(player.contracts.len(), 0);

        // When the contract list is has an entry
        player.add_contract(id);
        player.remove_contract(id);
        assert_eq!(player.contracts.len(), 0);
    }

    #[test]
    fn apply_command() {
        let setup = Setup::default();
        let mut player = setup.player;
        let id = 1;
        let commands = &mut Default::default();

        // With no commands
        assert_eq!(player.apply_command(commands), false);

        commands.push(Command::OfferContract(id));
    }
}
