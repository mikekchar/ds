use ggez::{graphics::MeshBuilder, GameResult};

use crate::game::{
    client::{
        point::{self, Point},
        rect::Rect,
    },
    config::Config,
    coord::LossyFrom,
};

pub struct Divider {
    pub start: Point,
    pub end: Point,
    pub width: point::Coord,
}

impl Divider {
    pub fn vertical(pane_rect: Rect, width: point::Coord) -> Self {
        let x = pane_rect.right() - width.scale_by(0.5);
        Divider {
            start: Point::new(x, pane_rect.top()),
            end: Point::new(x, pane_rect.bottom()),
            width,
        }
    }

    pub fn horizontal(pane_rect: Rect, width: point::Coord) -> Self {
        let y = pane_rect.bottom() - width.scale_by(0.5);
        Divider {
            start: Point::new(pane_rect.left(), y),
            end: Point::new(pane_rect.right(), y),
            width,
        }
    }

    pub fn rect(&self) -> Rect {
        let half_width = self.width.scale_by(0.5);
        Rect::new(
            self.start.x() - half_width,
            self.start.y(),
            self.width,
            self.end.y() - self.start.y(),
        )
    }

    pub fn line(&self) -> [Point; 2] {
        [self.start, self.end]
    }

    pub fn build(&self, mb: &mut MeshBuilder) -> GameResult<()> {
        mb.line(
            &self.line(),
            f32::lossy_from(self.width),
            Config::instance().app_config.frame_color,
        )?;
        Ok(())
    }
}
