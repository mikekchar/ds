//! A window pane for displaying the main grid map for the game.
use ggez::{
    graphics::{self, DrawParam, Mesh, MeshBuilder},
    Context,
    GameResult,
};
use std::default::Default;

use crate::game::{
    client::{
        font::Font,
        point::{self, Point},
        rect::Rect,
        text_cache::TextCache,
        tile::{element, Batch, ElementType, Sheet, Tile},
    },
    config::Config,
    dwarf::{Conditions, Locations},
    grid::Grid,
    layer::{self, Layer},
    symbol::Symbol,
};

use super::{divider::Divider, Pane};
use crate::game::{player::Player, symbol::Merge};

/// Contains items that are cached on the pane.
/// Generally this contains anything that requires a context to create
/// and must persist over several function calls.
pub struct Cache {
    pub pane_bg_batch: Batch,
    pub background_batch: Batch,
    pub text_cache: TextCache,
    pub fg_glyph_batch: Batch,
    pub status_batch: Batch,
    pub water_batch: Batch,
    pub grid_mesh: Mesh,
    pub border_mesh: Mesh,
}

impl Cache {
    /// Creates a cache with all the default settings.
    fn new(ctx: &mut Context, parent_pane: &GridPane) -> GameResult<Cache> {
        let grid_mesh = parent_pane
            .grid
            .mesh(ctx, Config::instance().app_config.grid_mesh_color)?;
        let white_sheet = Sheet::white_sheet(ctx)?;

        let mut pane_bg_batch = white_sheet.new_batch();
        pane_bg_batch.add(
            pane_bg_batch
                .draw_param(
                    0,
                    parent_pane.drawable_rect.width(),
                    parent_pane.drawable_rect.height(),
                )
                .dest(parent_pane.drawable_rect.destination())
                .color(Config::instance().app_config.bg_color),
        );

        let border_mesh = parent_pane.build_border(ctx)?;

        let cache = Cache {
            text_cache: TextCache::new(
                parent_pane.font,
                parent_pane.grid.tile_size(),
            ),
            grid_mesh,
            background_batch: white_sheet.new_batch(),
            fg_glyph_batch: parent_pane.glyphs.new_batch(),
            status_batch: white_sheet.new_batch(),
            water_batch: white_sheet.new_batch(),
            pane_bg_batch,
            border_mesh,
        };
        Ok(cache)
    }

    pub fn clear(&mut self) {
        self.background_batch.clear();
        self.fg_glyph_batch.clear();
        self.status_batch.clear();
        self.water_batch.clear();
    }
}

// A window pane for displaying the main grid/map for the game.
pub struct GridPane {
    pub rect: Rect,
    pub font: Font,
    pub glyphs: Sheet,
    pub drawable_rect: Rect,
    pub rhs_divider: Divider,
    pub bottom_divider: Divider,
    pub elements: Vec<ElementType>,
    pub grid: Grid,
    cache: Option<Cache>,
}

/// The rectangle (in point coordinates) for the pane.
/// Includes all of the borders and windows.
fn rect(parent_rect: Rect) -> Rect {
    Rect::new(
        parent_rect.left(),
        parent_rect.top(),
        parent_rect
            .width()
            .scale_by(Config::instance().app_config.grid_width_proporation),
        parent_rect
            .height()
            .scale_by(Config::instance().app_config.grid_height_proporation),
    )
}

/// The rectangle (in point coordinates) for drawable portion of the
/// pane.
fn drawable_rect(parent_rect: Rect, border_width: point::Coord) -> Rect {
    rect(parent_rect).shrink_by(border_width.scale_by(0.5))
}

/// A vertical divider on the right hand side of the pane.
fn rhs_divider(pane_rect: Rect, border_width: point::Coord) -> Divider {
    Divider::vertical(pane_rect, border_width.scale_by(0.5))
}

/// A horizontal divider on the bootom of the pane
fn bottom_divider(pane_rect: Rect, border_width: point::Coord) -> Divider {
    Divider::horizontal(pane_rect, border_width.scale_by(0.5))
}

fn create_symbol<'a>(
    layer: &'a Layer,
    layer_location: Option<layer::Location>,
    locations: &'a Locations,
    conditions: &Conditions,
) -> Symbol<'a> {
    let dwarf = layer_location
        .and_then(|location| locations.get(location))
        .map(|dwarfs| {
            dwarfs.iter().fold(Symbol::default(), |symbol, dwarf| {
                if let Some(ds) = dwarf.symbol() {
                    symbol.merge(ds)
                } else {
                    symbol
                }
            })
        });
    let condition = layer_location
        .and_then(|location| conditions.get(location))
        .map(Symbol::target);
    let terrain = match layer_location {
        None => Some(Symbol::off_map()),
        Some(location) => layer.terrain_symbol(location),
    };
    let water = match layer_location {
        None => Some(Symbol::off_map()),
        Some(location) => layer.water_symbol(location),
    };

    terrain
        .merge(condition)
        .merge(dwarf)
        .merge(water)
        .unwrap_or_else(Symbol::error)
}

impl Pane for GridPane {
    /// Queues up everything in the drawable portion of the pane to be
    /// drawn. The `location` is the location on the layer that
    /// will be drawn in the upper left hand corner of the pane.
    /// Note: if the cache is not set up yet, this function will
    /// create a new cache.
    fn queue(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()> {
        let cache = self.cache.get_or_insert(Cache::new(ctx, self)?);
        let dest = self.drawable_rect.destination();

        cache.clear();

        self.grid.cursor_to(player.location);
        for i in &self.grid {
            let layer_location =
                layer::Location::from_grid_location(i, &self.grid, layer);

            let mut symbol =
                create_symbol(layer, layer_location, locations, conditions);
            if Some(i) == self.grid.cursor_location {
                symbol = symbol.merge(Symbol::cursor(player.following));
            }
            let tile = Tile::new(
                symbol,
                self.grid.font.size,
                self.grid.padding,
                self.grid.point(i) + dest,
            );
            element::queue_tile(ctx, &tile, cache, &self.elements);
        }
        Ok(())
    }

    /// Draw everything the in drawable portion of the pane.
    /// The `location` is the location on the layer that will be drawn
    /// in the upper left hand corner of the pane.
    /// Note: if the cache is not set up yet, this function will
    /// create a new cache.
    fn draw(
        &mut self,
        ctx: &mut Context,
        _layer: &Layer,
        _player: &Player,
        _locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        let dest = self.drawable_rect.destination();
        let cache = self.cache.get_or_insert(Cache::new(ctx, self)?);

        cache.pane_bg_batch.draw(ctx, DrawParam::default())?;
        element::draw_tile(ctx, cache, &self.elements)?;
        element::draw_overlays(ctx, cache, &self.elements, dest)?;
        graphics::draw(ctx, &cache.border_mesh, DrawParam::default())?;
        Ok(())
    }
}

impl GridPane {
    /// Creates a new grid pane.
    /// It will be drawn inside the `parent_rect` and use the give
    /// font and glyph sheet.  The cache is not intialised.  This
    /// is so that we don't need a ggez context for this function.
    /// The cache will be initialised the first time something is
    /// queued or drawn.
    pub fn new(parent_rect: Rect, font: Font, glyphs: Sheet) -> Self {
        let padding = Config::instance().app_config.tile_padding;
        let glyph_width = font.size.scale_by(padding);

        let rect = rect(parent_rect);
        let drawable_rect = drawable_rect(parent_rect, glyph_width);

        GridPane {
            rect,
            font,
            glyphs,
            drawable_rect,
            rhs_divider: rhs_divider(rect, glyph_width),
            bottom_divider: bottom_divider(rect, glyph_width),
            elements: Config::instance().app_config.ui_elements.clone(),
            grid: Grid::new(rect, font, padding),
            cache: None,
        }
    }

    /// Chance the font size.  Resets the cache.
    pub fn change_font(&mut self, size: point::Coord) {
        let padding = Config::instance().app_config.tile_padding;
        self.font.size = size;
        let mut grid = Grid::new(self.rect, self.font, padding);
        grid.move_to(self.grid.layer_location);
        grid.cursor_to(self.grid.layer_location);
        self.grid = grid;
        self.cache = None;
    }

    /// Set the cursor location on the grid
    pub fn layer_location_from_point(
        &mut self,
        point: Point,
        layer: &Layer,
    ) -> Option<layer::Location> {
        let ul = self.grid.rect.destination();
        self.grid
            .location_from_point(point - ul)
            .and_then(|grid_location| {
                layer::Location::from_grid_location(
                    grid_location,
                    &self.grid,
                    layer,
                )
            })
    }

    /// Builds the mesh for the dividers in the pane.
    fn build_border(&self, ctx: &mut Context) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();
        self.rhs_divider.build(&mut mb)?;
        self.bottom_divider.build(&mut mb)?;
        mb.build(ctx)
    }

    pub fn move_to(&mut self, location: Option<layer::Location>) {
        self.grid.move_to(location);
    }

    pub fn cursor_to(&mut self, location: Option<layer::Location>) {
        self.grid.cursor_to(location);
    }

    /// Toggle the drawing of the given `ElementType`.
    /// Can turn off and on drawing of the grid mesh, for instance.
    pub fn toggle_ui(&mut self, element: ElementType) {
        if let Some(pos) = self.elements.iter().position(|x| *x == element) {
            self.elements.remove(pos);
        } else {
            self.elements.push(element);
        }
    }
}
