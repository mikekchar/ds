use ggez::{
    graphics::{self, DrawParam, Mesh, MeshBuilder, Text},
    Context,
    GameResult,
};

use super::{divider::Divider, Pane, Player};

use crate::game::{
    client::{
        font::Font,
        point,
        rect::Rect,
        tile::{Batch, Sheet},
    },
    config::Config,
    dwarf::{Conditions, Locations},
    layer::{self, Layer},
    symbol::{Description, Merge},
};

struct Cache {
    pane_bg_batch: Batch,
    border_mesh: Mesh,
}

impl Cache {
    fn new(ctx: &mut Context, parent_pane: &StatusPane) -> GameResult<Cache> {
        let white_sheet = Sheet::white_sheet(ctx)?;

        let mut pane_bg_batch = white_sheet.new_batch();
        pane_bg_batch.add(
            pane_bg_batch
                .draw_param(
                    0,
                    parent_pane.drawable_rect.width(),
                    parent_pane.drawable_rect.height(),
                )
                .dest(parent_pane.drawable_rect.destination())
                .color(Config::instance().app_config.bg_color),
        );
        let border_mesh = parent_pane.build_border(ctx)?;
        Ok(Cache {
            pane_bg_batch,
            border_mesh,
        })
    }
}

pub struct StatusPane {
    pub rect: Rect,
    pub font: Font,
    pub drawable_rect: Rect,
    pub rhs_divider: Divider,
    cache: Option<Cache>,
}

fn rect(parent_rect: Rect, grid_pane_rect: Rect) -> Rect {
    Rect::new(
        grid_pane_rect.left(),
        grid_pane_rect.bottom(),
        grid_pane_rect.width(),
        parent_rect.height() - grid_pane_rect.height(),
    )
}

fn drawable_rect(pane_rect: Rect, border_width: point::Coord) -> Rect {
    pane_rect.narrow_by(border_width.scale_by(0.5))
}

fn rhs_divider(pane_rect: Rect, border_width: point::Coord) -> Divider {
    Divider::vertical(pane_rect, border_width.scale_by(0.5))
}

impl Pane for StatusPane {
    fn queue(
        &mut self,
        _ctx: &mut Context,
        _layer: &Layer,
        _player: &Player,
        _locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        Ok(())
    }

    fn draw(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        let cache = self.cache.get_or_insert(Cache::new(ctx, self)?);
        cache.pane_bg_batch.draw(ctx, DrawParam::default())?;

        graphics::draw(ctx, &cache.border_mesh, DrawParam::default())?;

        let params =
            DrawParam::default().dest(self.drawable_rect.destination());
        if let Some(location) = player.location {
            match self.location_text(layer, location, locations).as_ref() {
                Some(text) => graphics::draw(ctx, text, params),
                None => Ok(()),
            }
        } else {
            Ok(())
        }
    }
}

impl StatusPane {
    pub fn new(parent_rect: Rect, grid_pane_rect: Rect, font: Font) -> Self {
        let border_width = font
            .size
            .scale_by(Config::instance().app_config.tile_padding);
        let rect = rect(parent_rect, grid_pane_rect);
        let drawable_rect = drawable_rect(rect, border_width);
        Self {
            rect,
            font,
            drawable_rect,
            rhs_divider: rhs_divider(rect, border_width),
            cache: None,
        }
    }

    fn build_border(&self, ctx: &mut Context) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();
        self.rhs_divider.build(&mut mb)?;
        mb.build(ctx)
    }

    // Location is the layer location of the center of the grid on the
    // layer
    pub fn location_text(
        &self,
        layer: &Layer,
        location: layer::Location,
        locations: &Locations,
    ) -> Option<Text> {
        let terrain_description = layer.terrain_description(location);

        let dwarf_description = locations.get(location).map(|dwarfs| {
            dwarfs
                .iter()
                .fold(Description::default(), |description, dwarf| {
                    if let Some(dd) = dwarf.description() {
                        description.merge(dd)
                    } else {
                        description
                    }
                })
        });
        let description = terrain_description.merge(dwarf_description);
        description.map(|desc| {
            desc.into_text(
                Some(Config::instance().app_config.fps_text_color),
                self.font,
                self.font.size,
            )
        })
    }
}
