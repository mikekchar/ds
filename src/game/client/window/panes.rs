use super::{ContextMenuPane, GridPane, MenuPane, Pane, StatusPane};
use crate::game::{
    client::{font::Font, rect::Rect, tile::Sheet},
    dwarf::{Conditions, Locations},
    layer::Layer,
    player::Player,
};
use ggez::{Context, GameResult};

pub struct Panes {
    pub grid_pane: Option<GridPane>,
    pub menu_pane: Option<MenuPane>,
    pub status_pane: Option<StatusPane>,
    pub context_menu_pane: Option<ContextMenuPane>,
}

impl Panes {
    pub fn new(rect: Rect, font: Font, glyphs: Sheet) -> Self {
        let grid_pane = GridPane::new(rect, font, glyphs);
        let grid_pane_rect = grid_pane.rect;
        Self {
            grid_pane: Some(grid_pane),
            menu_pane: Some(MenuPane::new(rect, grid_pane_rect, font)),
            status_pane: Some(StatusPane::new(rect, grid_pane_rect, font)),
            context_menu_pane: None,
        }
    }

    fn iter(&mut self) -> impl Iterator<Item = &'_ mut dyn Pane> + '_ {
        let mut panes: Vec<&mut dyn Pane> = vec![];
        panes.extend(self.grid_pane.as_mut().map(|p| p as &mut dyn Pane));
        panes.extend(self.menu_pane.as_mut().map(|p| p as &mut dyn Pane));
        panes.extend(self.status_pane.as_mut().map(|p| p as &mut dyn Pane));
        panes.extend(
            self.context_menu_pane.as_mut().map(|p| p as &mut dyn Pane),
        );
        panes.into_iter()
    }

    /// Queues contents to be drawn
    pub fn queue(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()> {
        for pane in self.iter() {
            pane.queue(ctx, layer, player, locations, conditions)?
        }
        Ok(())
    }

    /// Draws the contents of the pane
    pub fn draw(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()> {
        for pane in self.iter() {
            pane.draw(ctx, layer, player, locations, conditions)?
        }
        Ok(())
    }
}
