use ggez::{
    graphics::{self, DrawParam, Mesh, MeshBuilder, PxScale, Text},
    Context,
    GameResult,
};

use super::{Pane, Player};

use crate::game::{
    client::{
        font::Font,
        point::{self, Point},
        rect::Rect,
        tile::{Batch, Sheet},
    },
    command::Command,
    config::Config,
    coord::LossyFrom,
    dwarf::{Conditions, Locations},
    layer::Layer,
};

struct Cache {
    pane_bg_batch: Batch,
}

impl Cache {
    fn new(
        ctx: &mut Context,
        parent_pane: &ContextMenuPane,
    ) -> GameResult<Cache> {
        let white_sheet = Sheet::white_sheet(ctx)?;

        let mut pane_bg_batch = white_sheet.new_batch();
        pane_bg_batch.add(
            pane_bg_batch
                .draw_param(
                    0,
                    parent_pane.drawable_rect.width(),
                    parent_pane.drawable_rect.height(),
                )
                .dest(parent_pane.drawable_rect.destination())
                .color(ggez::graphics::Color::WHITE),
        );
        Ok(Cache { pane_bg_batch })
    }
}

pub struct ContextMenuPane {
    pub rect: Rect,
    pub mouse_position: Point,
    pub font: Font,
    pub drawable_rect: Rect,
    cache: Option<Cache>,
}

impl Pane for ContextMenuPane {
    fn queue(
        &mut self,
        _ctx: &mut Context,
        _layer: &Layer,
        _player: &Player,
        _locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        Ok(())
    }

    fn draw(
        &mut self,
        ctx: &mut Context,
        _layer: &Layer,
        player: &Player,
        locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        let cache = self.cache.get_or_insert(Cache::new(ctx, self)?);
        let mut commands: Vec<Command> = vec![];

        if let Some(d) = player.focussed_dwarf(locations) {
            d.commands(&mut commands)
        }
        if !commands.is_empty() {
            cache.pane_bg_batch.draw(ctx, DrawParam::default())?;
            let dest =
                DrawParam::default().dest(self.drawable_rect.destination());
            if self.is_highlighted() {
                let mesh = &self.background_highlight(ctx)?;
                graphics::draw(ctx, mesh, dest)?;
            }
            graphics::draw(ctx, &self.text(&commands), dest)
        } else {
            Ok(())
        }
    }
}

impl ContextMenuPane {
    pub fn new(point: Point, mouse_position: Point, font: Font) -> Self {
        let width = font.size * 15;
        let height = font.size * 3;
        let rect = Rect::new(point.x(), point.y(), width, height);
        Self {
            rect,
            mouse_position,
            font,
            drawable_rect: rect,
            cache: None,
        }
    }

    pub fn width(&self) -> point::Coord {
        self.font.size * 15
    }

    pub fn line_height(&self) -> point::Coord {
        self.font.size
    }

    pub fn is_highlighted(&self) -> bool {
        let x = self.mouse_position.x();
        let y = self.mouse_position.y();
        x >= self.drawable_rect.destination().x()
            && x <= self.drawable_rect.destination().x() + self.width()
            && y >= self.drawable_rect.destination().y()
            && y <= self.drawable_rect.destination().y() + self.line_height()
    }

    pub fn background_highlight(&self, ctx: &mut Context) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();
        let y = self.font.size / 2;
        mb.line(
            &[Point::new(0, y), Point::new(self.width(), y)],
            f32::lossy_from(self.font.size),
            ggez::graphics::Color::new(0.0, 0.2, 0.4, 1.0),
        )?;
        mb.build(ctx)
    }

    pub fn text(&self, commands: &[Command]) -> Text {
        let color = if self.is_highlighted() {
            ggez::graphics::Color::new(8.0, 8.0, 0.0, 1.0)
        } else {
            ggez::graphics::Color::new(0.0, 0.2, 0.4, 1.0)
        };

        Text::new(graphics::TextFragment {
            text: commands
                .first()
                .map(|c| Config::instance().get_command_message(c))
                .unwrap_or_else(|| "".to_owned()),
            color: Some(color),
            font: self.font.regular,
            scale: Some(PxScale::from(f32::lossy_from(self.font.size))),
        })
    }

    pub fn selected_item(
        &self,
        player: &Player,
        locations: &Locations,
    ) -> Option<Command> {
        let dwarf = player.focussed_dwarf(locations)?;
        let mut commands: Vec<Command> = vec![];
        dwarf.commands(&mut commands);

        commands.first().copied().filter(|_| self.is_highlighted())
    }
}
