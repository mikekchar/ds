use super::Pane;
use crate::game::{
    client::{
        font::Font,
        menu::Menu,
        rect::Rect,
        tile::{Batch, Sheet},
    },
    config::Config,
    dwarf::{Conditions, Locations},
    layer::Layer,
    player::Player,
};
use ggez::{graphics::DrawParam, Context, GameResult};

struct Cache {
    menu: Menu,
    pane_bg_batch: Batch,
}

impl Cache {
    fn new(
        ctx: &mut Context,
        parent_pane: &MenuPane,
        menu: Menu,
    ) -> GameResult<Cache> {
        let white_sheet = Sheet::white_sheet(ctx)?;

        let mut pane_bg_batch = white_sheet.new_batch();
        pane_bg_batch.add(
            pane_bg_batch
                .draw_param(
                    0,
                    parent_pane.drawable_rect.width(),
                    parent_pane.drawable_rect.height(),
                )
                .dest(parent_pane.drawable_rect.destination())
                .color(Config::instance().app_config.bg_color),
        );
        Ok(Cache {
            menu,
            pane_bg_batch,
        })
    }
}

pub struct MenuPane {
    pub rect: Rect,
    pub font: Font,
    pub drawable_rect: Rect,
    cache: Option<Cache>,
}

fn rect(parent_rect: Rect, grid_pane_rect: Rect) -> Rect {
    Rect::new(
        grid_pane_rect.right(),
        parent_rect.top(),
        parent_rect.right() - grid_pane_rect.right(),
        parent_rect.height(),
    )
}

impl Pane for MenuPane {
    fn queue(
        &mut self,
        _ctx: &mut Context,
        _layer: &Layer,
        _player: &Player,
        _locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        Ok(())
    }

    fn draw(
        &mut self,
        ctx: &mut Context,
        _layer: &Layer,
        _player: &Player,
        _locations: &Locations,
        _conditions: &Conditions,
    ) -> GameResult<()> {
        let cache = self.cache.get_or_insert(Cache::new(
            ctx,
            self,
            Menu::new(self.drawable_rect, self.font),
        )?);
        cache.pane_bg_batch.draw(ctx, DrawParam::default())?;
        cache.menu.draw(ctx).unwrap();
        Ok(())
    }
}

impl MenuPane {
    pub fn new(parent_rect: Rect, grid_pane_rect: Rect, font: Font) -> Self {
        let rect = rect(parent_rect, grid_pane_rect);
        Self {
            rect,
            drawable_rect: rect,
            font,
            cache: None,
        }
    }
}
