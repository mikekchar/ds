//! The portion of the screen that contains all of the graphical parts
//! of a tile. The nomenclature is a bit strange:
//!
//! Tile: The container for all the graphical bits that can be drawn.
//! Sheet: The image that contains the graphical glyphs that might
//!       be drawn in the forground of a tile.
//! Symbol: The logical struct that contains the information which
//!       determines what will be drawn in a tile.
mod batch;
pub mod element;
pub mod sheet;

use ggez::{
    graphics::{self, DrawParam, FilterMode},
    Context, GameResult,
};

use crate::game::{
    client::point::{self, Point},
    symbol::Symbol,
};

pub use batch::Batch;
pub use element::ElementType;
pub use sheet::Sheet;

/// Represents the `Text` and background color for a symbol to be
/// rendered on the screen at a given position (with offset).
pub struct Tile<'a> {
    symbol: Symbol<'a>,
    destination: Point,
    offset: Point,
    size: point::Coord,
}

impl<'a> Tile<'a> {
    pub fn new(
        symbol: Symbol<'a>,
        em: point::Coord,
        padding: f32,
        destination: Point,
    ) -> Tile<'a> {
        let offset = Point::new(0, em.scale_by((padding - 1.0) / 2.0));
        let size = em.scale_by(padding);

        Tile {
            symbol,
            destination,
            offset,
            size,
        }
    }

    pub fn draw_batch(
        ctx: &mut Context,
        batch: &Batch,
        params: DrawParam,
    ) -> GameResult<()> {
        batch.draw(ctx, params)
    }

    pub fn draw_text(ctx: &mut Context) -> GameResult<()> {
        graphics::draw_queued_text(
            ctx,
            DrawParam::default(),
            None,
            FilterMode::Linear,
        )
    }
}
