//! Holds the image for a sheet of tile glyphs
use ggez::GameResult;

use super::batch::Batch;
use crate::game::{
    client::{context, image::Image, point},
    config::app_config::AppConfig,
    coord::LossyFrom,
    location,
};

pub type Index = location::coord::Internal;

#[derive(Clone, Debug)]
pub struct Sheet {
    pub image: Image,
    pub num_rows: Index,
    pub num_cols: Index,
    pub glyph_width: point::Coord,
}

impl Default for Sheet {
    fn default() -> Self {
        // Don't use 0 so that we avoid divide by zero errors in tests, etc.
        Self {
            image: Image::default(),
            num_rows: 1,
            num_cols: 1,
            glyph_width: 1.into(),
        }
    }
}

/// Holds the image containing the glyphs for tiles.
impl Sheet {
    pub fn new(
        image: Image,
        num_rows: Index,
        num_cols: Index,
        glyph_width: impl Into<point::Coord>,
    ) -> Self {
        Sheet {
            image,
            num_rows,
            num_cols,
            glyph_width: glyph_width.into(),
        }
    }

    /// Returns a tile::Sheet composed of a single tile filled with
    /// opaque white.  This can be used to draw a box of any size
    /// with a solid color depending on the DrawParam used when
    /// queuing
    pub fn white_sheet(image: &mut impl context::Image) -> GameResult<Self> {
        Ok(Self::new(image.opaque(255)?, 1, 1, 1))
    }

    pub fn from_config(
        ctx: &mut impl context::Image,
        app_config: &AppConfig,
    ) -> GameResult<Self> {
        let filename = &app_config.tile_sheet_name;
        let image = ctx.from_file(filename)?;
        let num_rows = app_config.tile_sheet_rows;
        let num_cols = app_config.tile_sheet_cols;
        let glyph_width = image.width().unwrap_or_else(|| 1.into())
            / point::Coord::lossy_from(num_cols);

        Ok(Sheet::new(image, num_rows, num_cols, glyph_width))
    }

    pub fn new_batch(&self) -> Batch {
        Batch::new(self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ggez::graphics;

    #[test]
    fn tile_rect() {
        let sheet = Sheet::new(Image::default(), 16, 16, 32);
        let batch = Batch::new(&sheet);
        assert_eq!(
            batch.glyph_rect(11),
            graphics::Rect::new(0.6875, 0.0, 0.0625, 0.0625)
        );
    }
}
