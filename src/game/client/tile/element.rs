//! Represents a graphic element to queue or draw.  For example
//! a glyph, ascii character, background, etc.

use ggez::{graphics, Context, GameResult};
use strum::IntoEnumIterator;

use crate::game::{
    client::{point::Point, text_cache::TextCache, window::grid_pane},
    symbol::Glyph,
};

use super::{Batch, Tile};
use graphics::DrawParam;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, EnumIter)]
pub enum ElementType {
    Backgrounds,
    Glyphs,
    Status,
    Ascii,
    Grid,
    Water,
}

pub fn queue_background(tile: &Tile, background_batch: &mut Batch) {
    if let Some(background) = tile.symbol.background {
        background_batch.add(
            background_batch
                .draw_param(0, tile.size, tile.size)
                .color(background.value.into())
                .dest(tile.destination),
        );
    }
}

pub fn queue_glyph(tile: &Tile, glyph: &Glyph, glyph_batch: &mut Batch) {
    glyph_batch.add(
        glyph_batch
            .draw_param(glyph.index, tile.size, tile.size)
            .dest(tile.destination)
            .color(glyph.color.into()),
    );
}

pub fn queue_fg_glyph(tile: &Tile, glyph_batch: &mut Batch) {
    for index in &tile.symbol.glyph_index {
        queue_glyph(tile, &index.value, glyph_batch);
    }
}

pub fn queue_text(tile: &Tile, ctx: &mut Context, text_cache: &mut TextCache) {
    if let Some(ref text) = text_cache.get(&tile.symbol) {
        graphics::queue_text(ctx, text, tile.destination + tile.offset, None);
    }
}

pub fn queue_status(tile: &Tile, status_batch: &mut Batch) {
    if let Some(status) = tile.symbol.status {
        status_batch.add(
            status_batch
                .draw_param(0, tile.size, tile.size)
                .color(status.value.into())
                .dest(tile.destination),
        );
    }
}

pub fn queue_water(tile: &Tile, status_batch: &mut Batch) {
    if let Some(water) = tile.symbol.water {
        status_batch.add(
            status_batch
                .draw_param(0, tile.size, tile.size)
                .color(water.value.into())
                .dest(tile.destination),
        );
    }
}
pub fn queue_tile(
    ctx: &mut Context,
    tile: &Tile,
    cache: &mut grid_pane::Cache,
    enabled: &[ElementType],
) {
    for element in ElementType::iter() {
        if enabled.contains(&element) {
            match element {
                ElementType::Backgrounds => {
                    queue_background(tile, &mut cache.background_batch)
                }
                ElementType::Glyphs => {
                    queue_fg_glyph(tile, &mut cache.fg_glyph_batch)
                }
                ElementType::Ascii => {
                    queue_text(tile, ctx, &mut cache.text_cache)
                }
                ElementType::Status => {
                    queue_status(tile, &mut cache.status_batch)
                }
                ElementType::Water => {
                    queue_water(tile, &mut cache.water_batch)
                }
                ElementType::Grid => (),
            }
        }
    }
}

pub fn draw_tile(
    ctx: &mut Context,
    cache: &mut grid_pane::Cache,
    enabled: &[ElementType],
) -> GameResult<()> {
    for element in ElementType::iter() {
        if enabled.contains(&element) {
            match element {
                ElementType::Backgrounds => Tile::draw_batch(
                    ctx,
                    &cache.background_batch,
                    DrawParam::default(),
                )?,
                ElementType::Glyphs => Tile::draw_batch(
                    ctx,
                    &cache.fg_glyph_batch,
                    DrawParam::default(),
                )?,
                ElementType::Ascii => Tile::draw_text(ctx)?,
                ElementType::Status => Tile::draw_batch(
                    ctx,
                    &cache.status_batch,
                    DrawParam::default(),
                )?,
                ElementType::Water => Tile::draw_batch(
                    ctx,
                    &cache.water_batch,
                    DrawParam::default(),
                )?,
                ElementType::Grid => (),
            }
        }
    }
    Ok(())
}

pub fn draw_overlays(
    ctx: &mut Context,
    cache: &mut grid_pane::Cache,
    enabled: &[ElementType],
    grid_dest: Point,
) -> GameResult<()> {
    let grid_mesh = &cache.grid_mesh;
    if enabled.contains(&ElementType::Grid) {
        graphics::draw(ctx, grid_mesh, DrawParam::default().dest(grid_dest))?;
    }
    Ok(())
}
