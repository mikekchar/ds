use ggez::{
    graphics::{
        self,
        spritebatch::{SpriteBatch, SpriteIdx},
        DrawParam,
    },
    Context, GameError, GameResult,
};

use super::{sheet::Index, Sheet};
use crate::game::{client::point, coord::LossyFrom, location};

/// Represents a SpriteBatch for the tile glyphs
pub struct Batch {
    batch: Option<SpriteBatch>,
    sheet_rows: f32,
    sheet_cols: f32,
    glyph_size: f32,
}

impl Batch {
    pub fn new(sheet: &Sheet) -> Self {
        let image = sheet.image.clone();
        Batch {
            batch: image.spritebatch(),
            sheet_rows: sheet.num_rows as f32,
            sheet_cols: sheet.num_cols as f32,
            glyph_size: f32::lossy_from(sheet.glyph_width),
        }
    }

    pub fn glyph_rect(
        &self,
        index: location::coord::Internal,
    ) -> graphics::Rect {
        let index = index as f32;
        let x_pos = (index % self.sheet_cols).floor();
        let y_pos = (index / self.sheet_rows).floor();
        let x = x_pos / self.sheet_cols;
        let y = y_pos / self.sheet_rows;
        let w = 1.0 / self.sheet_cols;
        let h = 1.0 / self.sheet_rows;

        graphics::Rect::new(x, y, w, h)
    }

    pub fn glyph_size(&self) -> f32 {
        self.glyph_size
    }

    pub fn draw_param(
        &self,
        index: Index,
        x_size: point::Coord,
        y_size: point::Coord,
    ) -> DrawParam {
        let rect = self.glyph_rect(index);
        let scale_x = f32::lossy_from(x_size) / self.glyph_size();
        let scale_y = f32::lossy_from(y_size) / self.glyph_size();
        DrawParam::default().src(rect).scale(ggez::mint::Vector2 {
            x: scale_x,
            y: scale_y,
        })
    }

    pub fn add(
        &mut self,
        draw_param: graphics::DrawParam,
    ) -> Option<SpriteIdx> {
        self.batch.as_mut().map(|batch| batch.add(draw_param))
    }

    pub fn draw(
        &self,
        ctx: &mut Context,
        draw_param: graphics::DrawParam,
    ) -> GameResult<()> {
        match self.batch.as_ref() {
            Some(batch) => graphics::draw(ctx, batch, draw_param),
            None => Err(GameError::RenderError(
                "Tile in tileset was not found".to_owned(),
            )),
        }
    }

    pub fn clear(&mut self) {
        if let Some(batch) = self.batch.as_mut() {
            batch.clear()
        }
    }
}
