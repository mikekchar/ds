//! Code for dealing with rectangles on the screen
use super::point::{self, Point};
use crate::game::coord::LossyFrom;
use ggez::graphics;
use std::convert::From;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Rect(graphics::Rect);

impl From<graphics::Rect> for Rect {
    fn from(r: graphics::Rect) -> Self {
        Self(r)
    }
}

impl From<Rect> for graphics::Rect {
    fn from(r: Rect) -> Self {
        r.0
    }
}

impl Rect {
    pub fn new(
        x: impl Into<point::Coord>,
        y: impl Into<point::Coord>,
        w: impl Into<point::Coord>,
        h: impl Into<point::Coord>,
    ) -> Self {
        Self(graphics::Rect {
            x: f32::lossy_from(x.into()),
            y: f32::lossy_from(y.into()),
            h: f32::lossy_from(h.into()),
            w: f32::lossy_from(w.into()),
        })
    }

    pub fn destination(&self) -> Point {
        Point::new(self.left(), self.top())
    }

    pub fn width(&self) -> point::Coord {
        point::Coord::lossy_from(self.0.w)
    }

    pub fn height(&self) -> point::Coord {
        point::Coord::lossy_from(self.0.h)
    }

    pub fn left(&self) -> point::Coord {
        point::Coord::lossy_from(self.0.left())
    }

    pub fn right(&self) -> point::Coord {
        point::Coord::lossy_from(self.0.right())
    }

    pub fn top(&self) -> point::Coord {
        point::Coord::lossy_from(self.0.top())
    }

    pub fn bottom(&self) -> point::Coord {
        point::Coord::lossy_from(self.0.bottom())
    }

    pub fn shrink_by(self, amount: point::Coord) -> Self {
        Self::new(
            self.left(),
            self.top(),
            (self.width() - amount).max(1.into()),
            (self.height() - amount).max(1.into()),
        )
    }

    pub fn narrow_by(self, amount: point::Coord) -> Self {
        Self::new(
            self.left(),
            self.top(),
            (self.width() - amount).max(1.into()),
            self.height(),
        )
    }

    pub fn shorten_by(self, amount: point::Coord) -> Self {
        Self::new(
            self.left(),
            self.top(),
            self.width(),
            (self.height() - amount).max(1.into()),
        )
    }

    pub fn translate(self, amount: Point) -> Self {
        Self::new(
            self.left() + amount.x(),
            self.top() + amount.y(),
            self.width(),
            self.height(),
        )
    }
}
