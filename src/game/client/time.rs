//! Handles the update to to time including FPS, etc

use crate::game::{client::context::Timer, config::app_config::AppConfig};
use std::time::Duration;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum PauseState {
    Normal,
    ResumeStarting,
    ResumePaused,
}

/// Encodes the ratio of game time that passes per real time.
/// `Paused` means that no game time is passing.
/// `Starting` means that the game is transitioning between Paused and
/// Running states. `Running` is the ratio of game time per real time.
#[derive(Debug, PartialEq)]
pub enum GameSpeed {
    Paused(PauseState),
    Starting,
    Running(f64),
}

pub struct Time {
    pub desired_fps: u32,
    pub frames: u32,
    pub speed: GameSpeed,
    pub ticks_per_second: f64,
}

/// Returns the number of seconds that have passed for a number of
/// frames at a given fps.  Always returns 0.0 at 0 FPS.
pub fn seconds(frames: u32, fps: u32) -> f64 {
    if fps == 0 {
        0.0
    } else {
        f64::from(frames) / f64::from(fps)
    }
}

pub fn now(timer: &impl Timer) -> Duration {
    timer.time_since_start()
}

impl Default for Time {
    fn default() -> Self {
        Self {
            desired_fps: 60,
            frames: 0,
            speed: GameSpeed::Paused(PauseState::Normal),
            ticks_per_second: 10.0,
        }
    }
}

impl Time {
    pub fn from_config(app_config: &AppConfig) -> Self {
        Self {
            desired_fps: app_config.desired_fps,
            frames: 0,
            speed: GameSpeed::Paused(PauseState::Normal),
            ticks_per_second: app_config.ticks_per_game_second,
        }
    }

    pub fn pause(&mut self, state: PauseState) {
        self.speed = GameSpeed::Paused(state);
    }

    pub fn resume(&mut self) {
        self.speed = GameSpeed::Starting;
    }

    /// Toggle the pause state.  If it is paused, then restart, if
    /// not, the pause.
    pub fn toggle_pause(&mut self) {
        match self.speed {
            GameSpeed::Paused(PauseState::Normal) => self.resume(),
            GameSpeed::Paused(PauseState::ResumePaused) => (),
            GameSpeed::Paused(PauseState::ResumeStarting) => (),
            _ => self.pause(PauseState::Normal),
        }
    }

    pub fn temporary_pause(&mut self) {
        match self.speed {
            GameSpeed::Paused(PauseState::Normal) => {
                self.pause(PauseState::ResumePaused)
            }
            GameSpeed::Paused(PauseState::ResumePaused) => (),
            GameSpeed::Paused(PauseState::ResumeStarting) => (),
            _ => self.pause(PauseState::ResumeStarting),
        }
    }

    pub fn temporary_resume(&mut self) {
        match self.speed {
            GameSpeed::Paused(PauseState::Normal) => (),
            GameSpeed::Paused(PauseState::ResumePaused) => {
                self.pause(PauseState::Normal)
            }
            GameSpeed::Paused(PauseState::ResumeStarting) => self.resume(),
            _ => (),
        }
    }

    /// Returns the number of ticks that have elapsed for the given
    /// seconds at the current game speed.
    pub fn seconds_to_ticks(&self, seconds: f64) -> usize {
        match self.speed {
            GameSpeed::Paused(PauseState::Normal) => 0,
            GameSpeed::Paused(PauseState::ResumePaused) => 0,
            GameSpeed::Paused(PauseState::ResumeStarting) => 0,
            GameSpeed::Starting => 0,
            GameSpeed::Running(speed) => {
                (speed * seconds * self.ticks_per_second) as usize
            }
        }
    }

    /// Properly move between Paused, Starting, and Running states
    pub fn settle_pause(&mut self) {
        if self.speed == GameSpeed::Starting {
            self.speed = GameSpeed::Running(1.0);
            self.frames = 0;
        }
    }

    pub fn increment_frames(&mut self, timer: &mut impl Timer) {
        let mut frames = 0;
        while timer.check_update_time(self.desired_fps) {
            frames += 1;
        }
        self.frames += frames;
    }

    /// Ensure that the display is updated every 3 seconds when
    /// the game is paused.
    pub fn health_check(&mut self) -> bool {
        match self.speed {
            GameSpeed::Running(_) => false,
            _ => {
                if seconds(self.frames, self.desired_fps) > 3.0 {
                    self.frames = 0;
                    true
                } else {
                    false
                }
            }
        }
    }

    pub fn ticks(&self) -> usize {
        let elapsed = seconds(self.frames, self.desired_fps);
        self.seconds_to_ticks(elapsed)
    }

    pub fn process_ticks<F>(&mut self, mut f: F) -> bool
    where
        F: FnMut(bool) -> bool,
    {
        let mut updated = false;

        let ticks = self.ticks();
        if ticks > 0 {
            for _ in 0..ticks {
                updated |= f(updated)
            }
            self.frames = 0;
        }
        updated
    }
}
