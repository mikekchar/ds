/// A newtype to hold coordinate types for locations on a grid or map
pub use crate::game::{
    coord::{
        self, CheckedOps, CoordInner, LossyFrom, LossyInto, MaxValue,
        SaturatingOps,
    },
    location,
};

pub type Internal = i64;

impl SaturatingOps for Internal {
    fn saturating_add(self, x: Self) -> Self {
        self.saturating_add(x)
    }

    fn saturating_sub(self, x: Self) -> Self {
        self.saturating_sub(x)
    }

    fn saturating_mul(self, x: Self) -> Self {
        self.saturating_mul(x)
    }
}

impl CheckedOps<Internal> for Internal {
    fn checked_add(self, x: Internal) -> Option<Self> {
        self.checked_add(x)
    }

    fn checked_sub(self, x: Internal) -> Option<Self> {
        self.checked_sub(x)
    }
}

impl MaxValue for Internal {
    fn max_value() -> Internal {
        Internal::max_value()
    }
}

impl LossyFrom<f32> for Internal {
    fn lossy_from(x: f32) -> Internal {
        x as Internal
    }
}

impl LossyInto<f32> for Internal {
    fn lossy_into(self) -> f32 {
        self as f32
    }
}

impl CoordInner<Internal> for Internal {}

pub type Coord = coord::Coord<Internal>;

impl From<Internal> for Coord {
    fn from(x: Internal) -> Coord {
        Coord::new(x)
    }
}

impl From<Coord> for Internal {
    fn from(x: Coord) -> Internal {
        x.value()
    }
}

impl From<u16> for Coord {
    fn from(x: u16) -> Coord {
        Coord::new(x as Internal)
    }
}

impl From<i32> for Coord {
    fn from(x: i32) -> Coord {
        Coord::new(x as Internal)
    }
}

impl LossyFrom<Coord> for f32 {
    fn lossy_from(x: Coord) -> f32 {
        x.value() as f32
    }
}

impl LossyFrom<f32> for Coord {
    fn lossy_from(x: f32) -> Coord {
        Coord::new(x.round() as Internal)
    }
}

impl LossyFrom<location::coord::Internal> for Coord {
    fn lossy_from(x: location::coord::Internal) -> Coord {
        Coord::new(x as Internal)
    }
}

impl LossyFrom<Coord> for u16 {
    fn lossy_from(x: Coord) -> u16 {
        x.value() as u16
    }
}

impl LossyFrom<Coord> for u64 {
    fn lossy_from(x: Coord) -> u64 {
        x.value() as u64
    }
}

impl LossyFrom<Coord> for location::coord::Internal {
    fn lossy_from(x: Coord) -> location::coord::Internal {
        x.value() as location::coord::Internal
    }
}

impl Coord {
    pub fn scale_by(self, x: f32) -> Self {
        Self::lossy_from((f32::lossy_from(self) * x).round())
    }
}
