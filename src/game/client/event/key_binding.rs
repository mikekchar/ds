use ggez::event::KeyCode;
use std::collections::HashMap;

use super::Event;
use crate::game::{client::tile::ElementType, location::Direction};

pub struct KeyBinding(HashMap<KeyCode, Event>, HashMap<Event, KeyCode>);

impl Default for KeyBinding {
    fn default() -> Self {
        let mut keybinding = Self::new();

        keybinding.insert(KeyCode::Q, Event::Quit);
        keybinding.insert(KeyCode::J, Event::Walk(Direction::Down));
        keybinding.insert(KeyCode::B, Event::Walk(Direction::DownLeft));
        keybinding.insert(KeyCode::N, Event::Walk(Direction::DownRight));
        keybinding.insert(KeyCode::K, Event::Walk(Direction::Up));
        keybinding.insert(KeyCode::Y, Event::Walk(Direction::UpLeft));
        keybinding.insert(KeyCode::U, Event::Walk(Direction::UpRight));
        keybinding.insert(KeyCode::H, Event::Walk(Direction::Left));
        keybinding.insert(KeyCode::L, Event::Walk(Direction::Right));
        keybinding.insert(KeyCode::Space, Event::TogglePause);
        keybinding.insert(KeyCode::G, Event::ToggleUI(ElementType::Grid));
        keybinding.insert(KeyCode::A, Event::ToggleUI(ElementType::Ascii));
        keybinding.insert(KeyCode::D, Event::ToggleUI(ElementType::Glyphs));
        keybinding.insert(KeyCode::W, Event::ToggleUI(ElementType::Water));
        keybinding.insert(KeyCode::Z, Event::ToggleAscii);
        keybinding
            .insert(KeyCode::C, Event::ToggleUI(ElementType::Backgrounds));
        keybinding.insert(KeyCode::V, Event::ZoomToDwarf);
        keybinding.insert(KeyCode::F, Event::FollowDwarf);

        keybinding
    }
}

impl KeyBinding {
    fn new() -> Self {
        Self(HashMap::new(), HashMap::new())
    }

    pub fn insert(&mut self, key_code: KeyCode, event: Event) {
        self.0.insert(key_code, event);
        self.1.insert(event, key_code);
    }

    pub fn get(&self, key_code: KeyCode) -> Option<Event> {
        self.0.get(&key_code).copied()
    }

    pub fn get_key_code(&self, event: Event) -> Option<KeyCode> {
        self.1.get(&event).copied()
    }

    pub fn walk_key(&self, direction: Direction) -> (Direction, String) {
        let event = Event::Walk(direction);
        if let Some(keycode) = self.get_key_code(event) {
            (direction, format!("{:?}", keycode))
        } else {
            (direction, "NONE".to_owned())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use strum::IntoEnumIterator;

    #[test]
    fn insert() {
        let mut keybinding = KeyBinding::new();
        keybinding.insert(KeyCode::Q, Event::Quit);

        assert_eq!(keybinding.get(KeyCode::Q), Some(Event::Quit));
    }

    #[test]
    fn get() {
        let keybinding = KeyBinding::new();

        assert_eq!(keybinding.get(KeyCode::Q), None);
    }

    #[test]
    fn get_key_code() {
        let mut keybinding = KeyBinding::new();

        assert_eq!(keybinding.get_key_code(Event::Quit), None);

        keybinding.insert(KeyCode::Q, Event::Quit);
        assert_eq!(keybinding.get_key_code(Event::Quit), Some(KeyCode::Q));
    }

    #[test]
    fn walk_key() {
        let mut keybinding = KeyBinding::new();

        assert_eq!(
            keybinding.walk_key(Direction::Down),
            (Direction::Down, "NONE".to_owned())
        );

        keybinding.insert(KeyCode::J, Event::Walk(Direction::Down));
        assert_eq!(
            keybinding.walk_key(Direction::Down),
            (Direction::Down, "J".to_owned())
        );
    }

    #[test]
    fn default() {
        let keybinding = KeyBinding::default();

        assert_ne!(keybinding.get_key_code(Event::Quit), None);

        for dir in Direction::iter() {
            assert_ne!(keybinding.get_key_code(Event::Walk(dir)), None);
        }
    }
}
