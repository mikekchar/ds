//! Code for dealing with image pixmaps
use ggez::graphics;

use crate::game::client::point;

#[derive(Clone, Debug, Default)]
pub struct Image(Option<graphics::Image>);

impl Image {
    pub fn new(image: Option<graphics::Image>) -> Self {
        Self(image)
    }

    pub fn spritebatch(self) -> Option<graphics::spritebatch::SpriteBatch> {
        self.0.map(graphics::spritebatch::SpriteBatch::new)
    }

    pub fn width(&self) -> Option<point::Coord> {
        self.0.as_ref().map(|image| image.width().into())
    }
}
