use ggez::graphics::Text;

use crate::game::{client::font::Font, client::point, symbol::Symbol};

use std::collections::HashMap;

pub struct TextCache {
    hash: HashMap<Option<u64>, Option<Text>>,
    font: Font,
    bounds: point::Coord,
}

impl TextCache {}

impl TextCache {
    pub fn new(font: Font, bounds: point::Coord) -> Self {
        Self {
            hash: HashMap::default(),
            font,
            bounds,
        }
    }

    pub fn get(&mut self, symbol: &Symbol) -> &Option<Text> {
        let text = symbol.text(self.font, self.bounds);
        self.hash.entry(symbol.fg_hash()).or_insert_with(|| text)
    }
}
