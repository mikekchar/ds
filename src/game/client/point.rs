//! Code for dealing with points of the graphics screen.
pub mod coord;

pub use coord::Coord;
use coord::LossyFrom;
use ggez::mint::Point2;
use std::{
    convert::From,
    ops::{Add, Sub},
};

/// A location on the graphics screen in pixels.
/// Basically a wrapper around the points that ggez uses.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Point(Point2<Coord>);

impl From<(f32, f32)> for Point {
    fn from(p: (f32, f32)) -> Self {
        Self(Point2 {
            x: Coord::lossy_from(p.0),
            y: Coord::lossy_from(p.1),
        })
    }
}

impl From<Point2<f32>> for Point {
    /// Convert from an nalgebra `Point2` (which ggez uses) to our
    /// `Point` wrapper.
    fn from(p: Point2<f32>) -> Self {
        let x: Coord = Coord::lossy_from(p.x);
        let y: Coord = Coord::lossy_from(p.y);

        Self(Point2 { x, y })
    }
}

impl From<Point> for Point2<f32> {
    /// Convert from our `Point` wrapper to the points that ggez uses.
    fn from(p: Point) -> Self {
        let x: f32 = f32::lossy_from(p.x());
        let y: f32 = f32::lossy_from(p.y());

        Point2 { x, y }
    }
}

impl Point {
    /// Create a new `Point` describing a location on the screen in
    /// pixels.
    pub fn new(x: impl Into<Coord>, y: impl Into<Coord>) -> Self {
        Self(Point2 {
            x: x.into(),
            y: y.into(),
        })
    }

    pub fn x(self) -> Coord {
        self.0.x
    }

    pub fn y(self) -> Coord {
        self.0.y
    }

    pub fn is_positive(&self) -> bool {
        self.0.x.value() >= 0 && self.0.y.value() >= 0
    }
}

impl Add for Point {
    type Output = Point;

    /// Add 2 points together.
    fn add(self, other: Point) -> Point {
        Point::new(self.0.x + other.0.x, self.0.y + other.0.y)
    }
}

impl Sub for Point {
    type Output = Point;

    /// subtract 2 points from each other.
    fn sub(self, other: Point) -> Point {
        Point::new(self.0.x - other.0.x, self.0.y - other.0.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_points() {
        let a = Point::new(1, 2);
        let b = Point::new(4, 6);

        assert_eq!(a.add(b), Point::new(5, 8));
    }

    #[test]
    fn plus_points() {
        let a = Point::new(1, 2);
        let b = Point::new(4, 6);

        assert_eq!(a + b, Point::new(5, 8));
    }

    #[test]
    fn sub_points() {
        let a = Point::new(1, 2);
        let b = Point::new(4, 6);

        assert_eq!(a.sub(b), Point::new(-3, -4));
    }

    #[test]
    fn minus_points() {
        let a = Point::new(1, 2);
        let b = Point::new(4, 6);

        assert_eq!(a - b, Point::new(-3, -4));
    }
}
