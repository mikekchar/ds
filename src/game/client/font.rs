//! Wrapper for fonts.
use ggez::{graphics, GameResult};

use crate::game::{
    client::{context, point},
    config::app_config::AppConfig,
};

#[derive(Clone, Copy, Debug)]
pub struct Font {
    pub regular: Option<graphics::Font>,
    pub bold: Option<graphics::Font>,
    pub size: point::Coord,
}

impl Default for Font {
    fn default() -> Self {
        Font::new(None, None, 12)
    }
}

/// Holds the font for rendering text.
impl Font {
    pub fn new(
        regular: Option<graphics::Font>,
        bold: Option<graphics::Font>,
        size: impl Into<point::Coord>,
    ) -> Self {
        Font {
            regular,
            bold,
            size: size.into(),
        }
    }

    pub fn from_config(
        font: &mut impl context::Font,
        app_config: &AppConfig,
    ) -> GameResult<Self> {
        font.create(
            &app_config.regular_font_name,
            &app_config.bold_font_name,
            app_config.font_size,
        )
    }

    pub fn with_size(self, size: impl Into<point::Coord>) -> Self {
        Font {
            regular: self.regular,
            bold: self.bold,
            size: size.into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn with_size() {
        let font = Font::default().with_size(42);
        assert_eq!(font.size, 42.into());
    }
}
