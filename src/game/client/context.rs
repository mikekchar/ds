use crate::game::{
    client::{
        font, image,
        point::{self, Point},
        rect::Rect,
    },
    coord::LossyFrom,
};
use ggez::{graphics, input::mouse, timer, Context, GameResult};
use std::time::Duration;

pub trait Timer {
    fn time_since_start(&self) -> Duration;
    fn check_update_time(&mut self, desired_fps: u32) -> bool;
    fn sleep(&self, duration: Duration);
    fn fps(&self) -> f64;
}

pub trait Mouse {
    fn position(&self) -> Point;
}

pub trait Screen {
    fn set_coordinates(&mut self, rect: Rect) -> GameResult<()>;
    fn coordinates(&self) -> Option<Rect>;
}

pub trait Font {
    fn create(
        &mut self,
        regular_name: &str,
        bold_name: &str,
        size: impl Into<point::Coord>,
    ) -> GameResult<font::Font>;
}

pub trait Image {
    fn from_file(&mut self, filename: &str) -> GameResult<image::Image>;
    fn from_rgba8(
        &mut self,
        width: point::Coord,
        height: point::Coord,
        rgba: &[u8],
    ) -> GameResult<image::Image>;
    fn opaque(&mut self, color: u8) -> GameResult<image::Image>;
}

impl Timer for Context {
    fn time_since_start(&self) -> Duration {
        timer::time_since_start(self)
    }

    fn check_update_time(&mut self, desired_fps: u32) -> bool {
        ggez::timer::check_update_time(self, desired_fps)
    }

    fn sleep(&self, duration: Duration) {
        ggez::timer::sleep(duration);
    }

    fn fps(&self) -> f64 {
        ggez::timer::fps(self)
    }
}

impl Mouse for Context {
    fn position(&self) -> Point {
        mouse::position(self).into()
    }
}

impl Screen for Context {
    fn set_coordinates(&mut self, rect: Rect) -> GameResult<()> {
        graphics::set_screen_coordinates(self, rect.into())?;
        // You have to present after setting the screen coordinates
        // otherwise the mesh will draw improperly the first time.
        graphics::present(self)
    }

    fn coordinates(&self) -> Option<Rect> {
        Some(graphics::screen_coordinates(self).into())
    }
}

impl Font for Context {
    fn create(
        &mut self,
        regular_name: &str,
        bold_name: &str,
        size: impl Into<point::Coord>,
    ) -> GameResult<font::Font> {
        let regular = Some(graphics::Font::new(self, regular_name)?);
        let bold = Some(graphics::Font::new(self, bold_name)?);
        Ok(font::Font::new(regular, bold, size))
    }
}

impl Image for Context {
    fn from_file(&mut self, filename: &str) -> GameResult<image::Image> {
        let data = graphics::Image::new(self, filename)?;
        Ok(image::Image::new(Some(data)))
    }

    fn from_rgba8(
        &mut self,
        width: point::Coord,
        height: point::Coord,
        rgba: &[u8],
    ) -> GameResult<image::Image> {
        let width = u16::lossy_from(width);
        let height = u16::lossy_from(height);
        let data = graphics::Image::from_rgba8(self, width, height, rgba)?;
        Ok(image::Image::new(Some(data)))
    }

    fn opaque(&mut self, color: u8) -> GameResult<image::Image> {
        Self::from_rgba8(self, 1.into(), 1.into(), &[color; 4])
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::game::client::rect::Rect;

    pub struct FakeScreen {
        pub rect: Option<Rect>,
    }

    impl Default for FakeScreen {
        fn default() -> Self {
            FakeScreen { rect: None }
        }
    }

    impl Screen for FakeScreen {
        fn set_coordinates(&mut self, rect: Rect) -> GameResult<()> {
            self.rect = Some(rect);
            Ok(())
        }

        fn coordinates(&self) -> Option<Rect> {
            self.rect
        }
    }

    impl FakeScreen {
        pub fn assert_rect_is(&self, rect: &Rect) {
            assert_eq!(self.rect.as_ref(), Some(rect))
        }
    }

    pub struct FakeTimer {
        elapsed_frames: usize,
    }

    impl Default for FakeTimer {
        fn default() -> Self {
            FakeTimer { elapsed_frames: 1 }
        }
    }

    impl Timer for FakeTimer {
        fn time_since_start(&self) -> Duration {
            Duration::new(1, 0)
        }

        fn check_update_time(&mut self, _desired_fps: u32) -> bool {
            if self.elapsed_frames > 0 {
                self.elapsed_frames -= 1;
                true
            } else {
                false
            }
        }

        fn sleep(&self, _duration: Duration) {}

        fn fps(&self) -> f64 {
            60.0
        }
    }

    struct FakeFont {}

    impl Font for FakeFont {
        fn create(
            &mut self,
            _regular_name: &str,
            _bold_name: &str,
            size: impl Into<point::Coord>,
        ) -> GameResult<font::Font> {
            Ok(font::Font::new(None, None, size))
        }
    }
}
