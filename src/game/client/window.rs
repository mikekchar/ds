//! Eventually will hold code for dealing with the UI Frame.
//! At the moment it is a bit bare.
use ggez::{
    graphics::{
        self,
        Align,
        DrawParam,
        MeshBuilder,
        PxScale,
        Text,
        TextFragment,
    },
    Context,
    GameResult,
};

pub mod context_menu_pane;
pub mod divider;
pub mod grid_pane;
pub mod menu_pane;
mod panes;
pub mod status_pane;

use crate::game::{
    client::{
        context,
        font::Font,
        point::{self, Point},
        rect::Rect,
        tile::{ElementType, Sheet},
    },
    command::Command,
    config::{app_config::AppConfig, Config},
    coord::{CheckedOps, LossyFrom},
    dwarf::{Conditions, Locations},
    layer::{self, Layer},
    location::{Direction, Offset},
    player::{Player, Power},
};
use context_menu_pane::ContextMenuPane;
use graphics::{DrawMode, Mesh};
use grid_pane::GridPane;
use menu_pane::MenuPane;
use panes::Panes;
use status_pane::StatusPane;

trait Pane {
    /// Queues contents to be drawn
    fn queue(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()>;
    /// Draws the contents of the pane
    fn draw(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()>;
}

pub fn border_width(font: Font) -> point::Coord {
    font.size
        .scale_by(Config::instance().app_config.tile_padding)
}

pub fn drawable_rect(rect: Rect, border_width: point::Coord) -> Rect {
    rect.shrink_by(border_width.scale_by(2.0))
        .translate(Point::new(border_width, border_width))
}

/// Window for the UI.  Contains the FPS display.
pub struct Window {
    pub font: Font,
    pub glyphs: Sheet,
    pub rect: Rect,
    pub drawable_rect: Rect,
    fps: f64,
    clock: usize,
    power: Power,
    panes: Panes,
}

impl Default for Window {
    /// Creates a 1000x1000 pixel window with the default font
    /// and sheet at (0, 0).
    fn default() -> Self {
        Self::new(
            Font::default(),
            Sheet::default(),
            Point::new(0, 0),
            point::Coord::new(1000),
            point::Coord::new(1000),
        )
    }
}

impl Window {
    /// Initialise new FPS text somewhere on the screen.
    pub fn new(
        font: Font,
        glyphs: Sheet,
        destination: Point,
        width: point::Coord,
        height: point::Coord,
    ) -> Self {
        let rect = Rect::new(destination.x(), destination.y(), width, height);
        let drawable_rect = drawable_rect(rect, border_width(font));
        let panes = Panes::new(drawable_rect, font, glyphs.clone());
        Self {
            rect,
            drawable_rect,
            fps: 0.0,
            clock: 0,
            power: Default::default(),
            font,
            glyphs,
            panes,
        }
    }

    pub fn from_config(
        ctx: &mut (impl context::Screen + context::Image + context::Font),
        app_config: &AppConfig,
    ) -> GameResult<Self> {
        let coords = ctx.coordinates().unwrap_or_else(|| Rect::new(0, 0, 1, 1));
        Ok(Window::new(
            Font::from_config(ctx, app_config)?,
            Sheet::from_config(ctx, &Config::instance().app_config)?,
            Point::new(0, 0),
            coords.width(),
            coords.height(),
        ))
    }

    /// Creates a new window the same as before, but with a different
    /// size
    pub fn resize(&self, width: point::Coord, height: point::Coord) -> Self {
        Window::new(
            self.font,
            self.glyphs.clone(),
            self.rect.destination(),
            width,
            height,
        )
    }

    pub fn border_rect(&self) -> Rect {
        self.rect
            .shrink_by(border_width(self.font))
            .translate(Point::new(
                border_width(self.font).scale_by(0.5),
                border_width(self.font).scale_by(0.5),
            ))
    }

    pub fn update_fps(&mut self, fps: f64) {
        self.fps = fps;
    }

    pub fn fps_text(&self) -> Text {
        Text::new(graphics::TextFragment {
            text: Config::instance().get_fps_message(self.fps.round() as u32),
            color: Some(Config::instance().app_config.fps_text_color),
            font: self.font.regular,
            scale: Some(PxScale::from(f32::lossy_from(self.font.size))),
        })
    }

    pub fn title_text(&self) -> Text {
        let mut text = Text::new(TextFragment {
            text: Config::instance().get_message("game-title"),
            color: Some(Config::instance().app_config.title_text_color),
            font: self.font.bold,
            scale: Some(PxScale::from(f32::lossy_from(self.font.size))),
        });
        text.set_bounds(
            Point::new(self.border_rect().right(), border_width(self.font)),
            Align::Center,
        );
        text
    }

    pub fn text_offset(&self) -> point::Coord {
        self.font
            .size
            .scale_by((Config::instance().app_config.tile_padding - 1.0) / 2.0)
    }

    /// Increcement game time by 1 tick
    pub fn tick(&mut self) -> bool {
        self.clock += 1;
        true
    }

    pub fn clock_text(&self) -> Text {
        let mut text = Text::new(graphics::TextFragment {
            text: Config::instance().get_clock_message(self.clock),
            color: Some(Config::instance().app_config.fps_text_color),
            font: self.font.regular,
            scale: Some(PxScale::from(f32::lossy_from(self.font.size))),
        });
        text.set_bounds(
            Point::new(self.border_rect().right(), border_width(self.font)),
            Align::Right,
        );
        text
    }

    pub fn update_power(&mut self, power: Power) -> bool {
        if power != self.power {
            self.power = power;
            true
        } else {
            false
        }
    }

    pub fn power_text(&self) -> Text {
        Text::new(graphics::TextFragment {
            text: Config::instance().get_power_message(self.power),
            color: Some(Config::instance().app_config.power_text_color),
            font: self.font.regular,
            scale: Some(PxScale::from(f32::lossy_from(self.font.size))),
        })
    }

    /// Point where we draw the text in the border in screen
    /// coordinates
    pub fn title_text_destination(&self) -> Point {
        let width = border_width(self.font).scale_by(0.5);
        let offset = self.text_offset();
        self.border_rect().destination() - Point::new(width, width - offset)
    }

    /// Point where we draw the text in the bottom border in screen
    /// coordinates
    pub fn bottom_text_destination(&self) -> Point {
        let width = border_width(self.font).scale_by(0.5);
        let offset = self.text_offset();
        self.border_rect().destination()
            + Point::new(0, self.border_rect().height())
            - Point::new(width, width - offset)
    }

    /// Changes the font size in the grid
    pub fn change_grid_font(&mut self, size: impl Into<point::Coord>) {
        if let Some(pane) = self.panes.grid_pane.as_mut() {
            pane.change_font(size.into())
        }
    }

    /// Returns the font in the grid
    pub fn grid_font(&mut self) -> Font {
        if let Some(pane) = self.panes.grid_pane.as_ref() {
            pane.font
        } else {
            Font::default()
        }
    }

    pub fn layer_location_from_point(
        &mut self,
        point: Point,
        layer: &Layer,
    ) -> Option<layer::Location> {
        self.panes
            .grid_pane
            .as_mut()
            .and_then(|pane| pane.layer_location_from_point(point, layer))
    }

    pub fn move_grid_to(&mut self, location: Option<layer::Location>) {
        if let Some(pane) = self.panes.grid_pane.as_mut() {
            pane.move_to(location);
        }
    }

    pub fn move_cursor_to(&mut self, location: Option<layer::Location>) {
        if let Some(pane) = self.panes.grid_pane.as_mut() {
            pane.cursor_to(location);
        }
    }

    pub fn toggle_ui(&mut self, element: ElementType) {
        if let Some(pane) = self.panes.grid_pane.as_mut() {
            pane.toggle_ui(element);
        }
    }

    /// Add a context menu with the upper left hand corner at the
    /// location to the right of the mouse position.
    pub fn open_context_menu(&mut self, mouse_position: Point) {
        if let Some(grid_pane) = self.panes.grid_pane.as_ref() {
            if let Some(menu) = self.panes.context_menu_pane.as_mut() {
                menu.mouse_position = mouse_position;
            } else {
                let ul = grid_pane.grid.rect.destination();
                let width = grid_pane.rect.width();
                let font = self.font.with_size(width / 60);
                if let Some(point_loc) =
                    grid_pane.grid.location_from_point(mouse_position - ul)
                {
                    if let Some(dest_loc) =
                        point_loc.checked_add(Offset::from(Direction::Right))
                    {
                        self.panes.context_menu_pane =
                            Some(ContextMenuPane::new(
                                grid_pane.grid.point(dest_loc) + ul,
                                mouse_position,
                                font,
                            ));
                    }
                }
            }
        }
    }

    pub fn context_menu_item(
        &self,
        player: &Player,
        locations: &Locations,
    ) -> Option<Command> {
        self.panes
            .context_menu_pane
            .as_ref()
            .and_then(|pane| pane.selected_item(player, locations))
    }

    /// Remove the context menu
    pub fn close_context_menu(&mut self) {
        self.panes.context_menu_pane = None;
    }

    /// The rectangle containing the Window
    pub fn rect(&self) -> Rect {
        self.rect
    }

    /// Build any meshes that are necessary
    pub fn build_border(
        &mut self,
        ctx: &mut Context,
        border_rect: Rect,
        border_width: point::Coord,
    ) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();
        mb.rectangle(
            DrawMode::stroke(f32::lossy_from(border_width)),
            border_rect.into(),
            Config::instance().app_config.frame_color,
        )?;
        mb.build(ctx)
    }

    /// Queues contents to be drawn
    pub fn queue(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()> {
        self.panes.queue(ctx, layer, player, locations, conditions)
    }

    /// Draws the contents of the pane
    pub fn draw(
        &mut self,
        ctx: &mut Context,
        layer: &Layer,
        player: &Player,
        locations: &Locations,
        conditions: &Conditions,
    ) -> GameResult<()> {
        self.panes.draw(ctx, layer, player, locations, conditions)?;

        let border_mesh = self.build_border(
            ctx,
            self.border_rect(),
            border_width(self.font),
        )?;
        graphics::draw(ctx, &border_mesh, DrawParam::default())?;

        let top_params =
            DrawParam::default().dest(self.title_text_destination());
        let bottom_params =
            DrawParam::default().dest(self.bottom_text_destination());
        graphics::draw(ctx, &self.fps_text(), top_params)?;
        graphics::draw(ctx, &self.title_text(), top_params)?;
        graphics::draw(ctx, &self.clock_text(), top_params)?;
        graphics::draw(ctx, &self.power_text(), bottom_params)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::test_setup::Setup;

    #[test]
    fn test_update_fps() {
        let mut window = Setup::default().window();
        // At first default to 0.0 fps
        assert_eq!(window.fps, 0.0);

        window.update_fps(60.9);
        assert_eq!(window.fps, 60.9);
    }

    #[test]
    fn test_fps_text() {
        let mut window = Setup::default().window();
        // At first default to 0.0 fps
        assert_eq!(window.fps_text().contents(), "FPS: 0");

        // It rounding the value
        window.update_fps(60.9);
        assert_eq!(window.fps_text().contents(), "FPS: 61");
    }

    #[test]
    fn test_text_offset() {
        assert_eq!(Setup::default().window().text_offset(), 1.into());
        assert_eq!(
            Setup {
                font_size: 100.into(),
                ..Default::default()
            }
            .window()
            .text_offset(),
            10.into()
        );
        assert_eq!(
            Setup {
                font_size: 4.into(),
                ..Default::default()
            }
            .window()
            .text_offset(),
            0.into()
        );
    }

    #[test]
    fn test_title_text_destination() {
        let window = Setup::default().window();

        assert_eq!(
            window.title_text_destination(),
            Point::new(0, window.text_offset() + 0)
        );

        let window = Setup {
            window_x: 100.into(),
            window_y: 100.into(),
            ..Default::default()
        }
        .window();
        assert_eq!(
            window.title_text_destination(),
            Point::new(100, window.text_offset() + 100)
        );
    }

    #[test]
    fn test_grid_pane_constuction() {
        let setup = Setup::default();
        let window = setup.window();

        assert_eq!(
            window.panes.grid_pane.unwrap().rect,
            Rect::new(12, 12, 317, 381)
        );

        let window = Setup {
            window_x: 100.into(),
            window_y: 100.into(),
            ..Default::default()
        }
        .window();
        assert_eq!(
            window.panes.grid_pane.unwrap().rect,
            Rect::new(112, 112, 317, 381)
        );
    }

    #[test]
    fn test_grid_divider() {
        let window = Setup::default().window();
        assert_eq!(
            window.panes.grid_pane.unwrap().rhs_divider.rect(),
            Rect::new(323, 12, 6, 381)
        );

        let window = Setup {
            window_x: 100.into(),
            window_y: 100.into(),
            ..Default::default()
        }
        .window();
        assert_eq!(
            window.panes.grid_pane.unwrap().rhs_divider.rect(),
            Rect::new(423, 112, 6, 381)
        );
    }

    #[test]
    fn test_menu_size() {
        let setup = Setup::default();
        let window = setup.window();

        assert_eq!(
            window.panes.menu_pane.unwrap().rect,
            Rect::new(329, 12, 159, 476)
        );

        let window = Setup {
            window_x: 100.into(),
            window_y: 100.into(),
            ..Default::default()
        }
        .window();
        assert_eq!(
            window.panes.menu_pane.unwrap().rect,
            Rect::new(429, 112, 159, 476)
        );
    }
}
