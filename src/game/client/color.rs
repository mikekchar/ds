use ggez::graphics;
use std::{
    cmp::{Ord, Ordering},
    hash::{Hash, Hasher},
};

#[derive(Clone, Copy, Debug)]
pub struct Color(graphics::Color);

impl From<graphics::Color> for Color {
    fn from(c: graphics::Color) -> Self {
        Self(c)
    }
}

impl From<Color> for graphics::Color {
    fn from(c: Color) -> Self {
        c.0
    }
}

impl Ord for Color {
    fn cmp(&self, other: &Self) -> Ordering {
        self.to_rgb_u32().cmp(&other.to_rgb_u32())
    }
}

impl PartialOrd for Color {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.to_rgb_u32().cmp(&other.to_rgb_u32()))
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        self.to_rgb_u32() == other.to_rgb_u32()
    }
}

impl Eq for Color {
}

impl Hash for Color {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.to_rgb_u32().hash(state)
    }
}

impl Color {
    pub fn opaque(r: f32, g: f32, b: f32) -> Self {
        Self(graphics::Color::new(r, g, b, 1.0))
    }

    pub fn transparent(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self(graphics::Color::new(r, g, b, a))
    }

    fn to_rgb_u32(self) -> u32 {
        self.0.to_rgb_u32()
    }
}
