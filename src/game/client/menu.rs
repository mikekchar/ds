use ggez::{
    graphics::{self, Align, Color, DrawParam, PxScale, Text, TextFragment},
    Context,
    GameResult,
};

use crate::game::{
    client::{
        font::Font,
        point::{self, Point},
        rect::Rect,
    },
    config::Config,
    coord::LossyFrom,
    location::{self, Direction},
};

pub struct Title {
    pub text: Text,
    pub param: DrawParam,
}

impl Title {
    pub fn new(string: &str, font: Font, drawable_rect: Rect) -> Self {
        let mut text = Text::new(TextFragment {
            text: string.to_string(),
            color: Some(Color::new(1.0, 1.0, 1.0, 1.0)),
            font: font.bold,
            scale: Some(PxScale::from(f32::lossy_from(font.size))),
        });
        text.set_bounds(
            Point::new(drawable_rect.width(), font.size as point::Coord),
            Align::Center,
        );
        let param = DrawParam::default().dest(drawable_rect.destination());
        Self { text, param }
    }

    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        graphics::draw(ctx, &self.text, self.param)?;
        Ok(())
    }
}

pub struct MovementBox {
    pub font: Font,
    pub rect: Rect,
}

impl MovementBox {
    pub fn new(font: Font, drawable_rect: Rect) -> Self {
        Self {
            font,
            rect: drawable_rect,
        }
    }

    pub fn direction_pos(
        index: Direction,
    ) -> (location::Coord, location::Coord) {
        match index {
            Direction::UpLeft => (0.into(), 0.into()),
            Direction::Up => (0.into(), 1.into()),
            Direction::UpRight => (0.into(), 2.into()),
            Direction::Left => (1.into(), 0.into()),
            Direction::Right => (1.into(), 2.into()),
            Direction::DownLeft => (2.into(), 0.into()),
            Direction::Down => (2.into(), 1.into()),
            Direction::DownRight => (2.into(), 2.into()),
        }
    }

    pub fn row(index: Direction) -> location::Coord {
        Self::direction_pos(index).0
    }

    pub fn col(index: Direction) -> location::Coord {
        Self::direction_pos(index).1
    }

    fn key_size(&self) -> (point::Coord, point::Coord) {
        (
            self.rect.width().scale_by(1.0 / 3.0),
            self.rect.height().scale_by(1.0 / 3.0),
        )
    }

    pub fn key_rect(&self, index: Direction) -> Rect {
        let size = self.key_size();
        let x = self.rect.left() + Self::col(index).num_points(size.0);
        let y = self.rect.top() + Self::row(index).num_points(size.1);
        Rect::new(x, y, size.0, size.1)
    }

    pub fn font_size(&self) -> point::Coord {
        self.key_size()
            .1
            .scale_by(1.0 / Config::instance().app_config.tile_padding)
    }

    pub fn key(&self, pair: (Direction, String)) -> Key {
        Key::new(
            &pair.1,
            self.font.with_size(self.font_size()),
            self.key_rect(pair.0),
        )
    }
}

pub struct Key {
    pub text: Text,
    pub param: DrawParam,
}

impl Key {
    pub fn new(key: &str, font: Font, rect: Rect) -> Self {
        let mut text = Text::new(TextFragment {
            text: key.to_string(),
            color: Some(Color::new(1.0, 1.0, 1.0, 1.0)),
            font: font.regular,
            scale: Some(PxScale::from(f32::lossy_from(font.size))),
        });
        text.set_bounds(Point::new(rect.width(), font.size), Align::Center);
        let dest = rect.destination();
        let param = DrawParam::default().dest(Point::new(dest.x(), dest.y()));
        Self { text, param }
    }

    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        graphics::draw(ctx, &self.text, self.param)?;
        Ok(())
    }
}

pub struct Menu {
    pub font: Font,
    pub title_rect: Rect,
    pub movement_box: MovementBox,
}

pub fn title_rect(drawable_rect: Rect, font: Font) -> Rect {
    Rect::new(
        drawable_rect.left(),
        drawable_rect.top(),
        drawable_rect.width(),
        font.size
            .scale_by(Config::instance().app_config.tile_padding),
    )
}

fn movement_box(drawable_rect: Rect, font: Font) -> MovementBox {
    let tr = title_rect(drawable_rect, font);
    let box_size = drawable_rect.width().scale_by(0.4);
    let rect = Rect::new(
        drawable_rect.left(),
        tr.top() + tr.height() + 1,
        box_size,
        box_size,
    );
    MovementBox::new(font, rect)
}

impl Menu {
    pub fn new(parent_rect: Rect, font: Font) -> Self {
        Menu {
            font,
            title_rect: title_rect(parent_rect, font),
            movement_box: movement_box(parent_rect, font),
        }
    }

    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        Title::new(
            &Config::instance().get_message("menu-title"),
            self.font,
            self.title_rect,
        )
        .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::UpLeft),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::Up),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::UpRight),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::Left),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::Right),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::DownLeft),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::Down),
            )
            .draw(ctx)?;
        self.movement_box
            .key(
                Config::instance()
                    .app_config
                    .key_binding
                    .walk_key(Direction::DownRight),
            )
            .draw(ctx)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::game::test_setup::Setup;

    #[test]
    fn test_title_rect() {
        let setup = Setup::default();
        let menu = setup.menu();
        let rect = menu.title_rect;
        assert_eq!(rect.top(), 0.into());
        assert_eq!(rect.left(), 0.into());
        assert_eq!(rect.width(), 600.into());
        assert_eq!(rect.height(), 12.into());
        assert_eq!(rect.bottom(), 12.into());
    }

    #[test]
    fn test_movement_box_rect() {
        let setup = Setup::default();
        let menu = setup.menu();
        let rect = menu.movement_box.rect;
        assert_eq!(rect.top(), 13.into());
        assert_eq!(rect.left(), 0.into());
        assert_eq!(rect.width(), 240.into());
        assert_eq!(rect.height(), 240.into());
        assert_eq!(rect.bottom(), 253.into());
    }
}
