//! UI Events for the game
use std::hash::Hash;

use crate::game::{
    client::{
        point::{self, Point},
        tile::ElementType,
    },
    location::Direction,
};

pub mod key_binding;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Event {
    Continue,
    Quit,
    Redraw,
    Resize(Point),
    Walk(Direction),
    Recenter(),
    Zoom(point::Coord),
    TogglePause,
    Pause,
    Resume,
    ToggleUI(ElementType),
    ToggleAscii,
    FocusCursor(Point),
    CloseContextMenu,
    LongClickStart,
    LongClickStop,
    ZoomToDwarf,
    FollowDwarf,
}
