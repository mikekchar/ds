//! Code for representing a dwarf
use crate::game::{
    command::Command,
    config::Config,
    event::Event,
    layer::{self, Layer},
    location::{self, Direction},
    player::Power,
    symbol::{Description, Merge, Symbol},
    trail::Trail,
};

use std::{
    collections::HashMap,
    fmt::{self, Display},
};

pub mod conditions;
pub mod locations;
pub mod population;

pub use conditions::{Condition, Conditions};
pub use locations::Locations;
pub use population::Population;

use super::map::MapTile;

pub type Id = u32;
pub type Hydration = u8;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Thirst {
    Fine,
    Thirsty,
    Dead,
    Dried,
    Dessicated,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Observation {
    Nothing,
    DiscoverWater,
}

impl Display for Observation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Observation::Nothing => write!(f, "Nothing"),
            Observation::DiscoverWater => write!(f, "Discovered Water"),
        }
    }
}

impl Observation {
    pub fn create(location: layer::Location, layer: &Layer) -> Self {
        let pool_location =
            location.into_surrounding_iter(layer).find(|&location| {
                layer
                    .map
                    .get_tile(location)
                    .filter(|&tile| tile == MapTile::Pool)
                    .is_some()
            });
        if pool_location.is_some() {
            Observation::DiscoverWater
        } else {
            Observation::Nothing
        }
    }
}

#[derive(Debug, Default)]
pub struct ObservationMap(HashMap<layer::Location, Observation>);
impl ObservationMap {
    pub fn insert(
        &mut self,
        location: layer::Location,
        observation: Observation,
    ) {
        self.0.insert(location, observation);
    }

    pub fn get(&self, location: layer::Location) -> Option<Observation> {
        self.0.get(&location).copied()
    }

    pub fn find_closest(
        &self,
        observation: Observation,
        location: layer::Location,
    ) -> Option<layer::Location> {
        let mut min: Option<layer::Location> = None;
        for (&key, &value) in self.0.iter() {
            if value == observation {
                let distance = location.distance_to(&key);
                if let Some(current) = min {
                    if distance < location.distance_to(&current) {
                        min = Some(key);
                    }
                } else {
                    min = Some(key);
                }
            }
        }
        min
    }
}

#[derive(Debug, Display, Clone, Copy, PartialEq, Eq)]
pub enum Goal {
    Drink,
    Wander,
}

#[derive(Debug)]
pub struct Plan {
    goal: Goal,
    trail: Option<Trail>,
}

impl Display for Plan {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} ({})", self.goal, self.destination_string())
    }
}

impl Plan {
    fn wander(trail: Option<Trail>) -> Plan {
        Plan {
            goal: Goal::Wander,
            trail,
        }
    }

    fn drink(trail: Option<Trail>) -> Plan {
        Plan {
            goal: Goal::Drink,
            trail,
        }
    }

    fn is_achieved(&self, dwarf: &Dwarf) -> bool {
        match self.goal {
            Goal::Drink => !dwarf.is_thirsty(),
            Goal::Wander => self.destination().is_none(),
        }
    }

    fn destination(&self) -> Option<layer::Location> {
        self.trail.as_ref().and_then(|trail| trail.destination())
    }

    fn destination_string(&self) -> String {
        match self.destination() {
            None => "None".to_owned(),
            Some(dest) => dest.to_string(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum State {
    Living,
    Dead,
    Skeleton,
    Ghost,
}

#[derive(Debug, Clone, Copy)]
pub struct Status {
    pub location: layer::Location,
    pub progress: f32,
    pub facing: Direction,
    pub state: State,
    pub observation: Observation,
}

impl PartialEq for Status {
    fn eq(&self, other: &Self) -> bool {
        self.location == other.location
            && self.facing == other.facing
            && self.state == other.state
            && (self.progress - other.progress).abs() < 0.01
    }
}

impl Status {
    /// Returns a tuple of the number of steps in the path
    /// as well as any fractional portion of a step the dwarf
    /// should travel in this tick.
    pub fn steps(&self, speed: f32) -> (location::coord::Internal, f32) {
        let steps = self.progress + speed;
        (steps.trunc() as location::coord::Internal, steps.fract())
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum CauseOfDeath {
    Dehydration,
}

impl Display for CauseOfDeath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            CauseOfDeath::Dehydration => write!(f, "Dehydration"),
        }
    }
}

#[derive(Debug)]
pub struct Dwarf {
    pub id: Option<Id>,
    pub status: Status,
    pub plan: Option<Plan>,
    // Percentage of a tile the dwarf can move per tick
    pub speed: f32,
    hydration: Hydration,
    pub has_contract: bool,
    pub power: Power,
    pub memory: ObservationMap,
    pub cause_of_death: Option<CauseOfDeath>,
}

impl Dwarf {
    pub fn new(location: layer::Location) -> Self {
        let status = Status {
            location,
            progress: 0.0,
            facing: Direction::Down,
            state: State::Living,
            observation: Observation::Nothing,
        };
        Dwarf {
            id: None,
            status,
            plan: None,
            speed: Config::instance().app_config.dwarf_speed,
            hydration: 255,
            has_contract: false,
            power: Default::default(),
            memory: Default::default(),
            cause_of_death: Default::default(),
        }
    }

    fn trail(&self) -> Option<&Trail> {
        self.plan.as_ref().and_then(|plan| plan.trail.as_ref())
    }

    fn trail_mut(&mut self) -> Option<&mut Trail> {
        self.plan.as_mut().and_then(|plan| plan.trail.as_mut())
    }

    /// Returns the final destination of the dwarf's trail if any
    pub fn destination(&self) -> Option<layer::Location> {
        self.trail().and_then(|trail| trail.destination())
    }

    /// Returns true if the dwarf is wandering randomly
    /// instead of going actively to a remembered location
    pub fn is_wandering(&self) -> bool {
        self.plan
            .as_ref()
            .filter(|plan| plan.goal == Goal::Wander)
            .is_some()
    }

    /// Returns the location of the dwarf on the layer
    pub fn location(&self) -> layer::Location {
        self.status.location
    }

    /// Walk as many steps in the trail (including fractional steps)
    /// as possible in this tick.  If the trail is blocked
    /// then the dwarf does not move.
    /// Returns true if the dwarf moved.
    pub fn walk(&mut self, layer: &mut Layer) -> bool {
        let start = self.status;
        let speed = self.speed;
        self.trail_mut()
            .and_then(|trail| trail.walk(start, speed, layer))
            .map(|end| self.status = end)
            .is_some()
    }

    fn drink(&mut self) {
        self.hydration = 255;
    }

    pub fn drink_from_pool(&mut self, layer: &mut Layer) -> bool {
        self.is_thirsty()
            && layer
                .nearest_pool(self.location())
                .map(|location| {
                    self.drink();
                    layer.map.set_tile(location, MapTile::Floor);
                })
                .is_some()
    }

    /// Uses power to increase hydration and live a little but longer
    pub fn rehydrate(&mut self) -> bool {
        if !(self.is_thirsty() && self.power > 10) {
            return false;
        }

        let power =
            self.power.min((255 - self.hydration) as Power) as Hydration;
        self.hydration += power;
        self.power = 0;
        true
    }

    pub fn plan_drink(&self, layer: &Layer) -> Option<Plan> {
        if self.is_thirsty() {
            self.memory
                .find_closest(Observation::DiscoverWater, self.status.location)
                .map(|goal| Trail::path_to(self.status.location, goal, layer))
                .map(Plan::drink)
        } else {
            None
        }
    }

    /// Create a new plan
    /// layer is mutable because we use its RNG to determine locations
    /// to path to.
    pub fn add_plan(&mut self, layer: &mut Layer) -> bool {
        self.plan = self.plan_drink(layer).or_else(|| {
            Some(Plan::wander(Trail::wander(self.status.location, layer)))
        });
        true
    }

    fn plan_string(&self) -> String {
        match &self.plan {
            None => "None".to_owned(),
            Some(plan) => plan.to_string(),
        }
    }
    pub fn observe(&mut self, layer: &Layer) -> bool {
        let observation = Observation::create(self.status.location, layer);
        self.status.observation = observation;
        self.memory.insert(self.status.location, observation);
        false
    }

    /// Activities might do while in the middle of a different job
    pub fn on_the_way(&mut self, layer: &mut Layer) -> bool {
        self.is_alive() && (self.drink_from_pool(layer) || self.rehydrate())
    }

    /// Set the dwarf's state to `Dead` and cache it on
    /// the layer.
    pub fn die(
        &mut self,
        cause_of_death: CauseOfDeath,
        events: &mut Vec<Event>,
    ) {
        self.status.state = State::Dead;
        self.cause_of_death = Some(cause_of_death);
        self.has_contract = false;
        self.power = 0;
        if let Some(id) = self.id {
            events.push(Event::Death(id));
        }
    }

    /// Set the dwarf's state to `Skeleton` and cache it on
    /// the layer.
    pub fn decompose(&mut self) {
        self.status.state = State::Skeleton;
    }

    /// makes the dwarf a ghost
    pub fn disintegrate(&mut self) {
        self.status.state = State::Ghost;
    }

    fn can_act(&self) -> bool {
        self.status.state == State::Living || self.status.state == State::Ghost
    }

    /// Decrease the hydration of the dwarf/corpse
    fn dehydrate(&mut self) {
        if self.hydration > 0 {
            self.hydration -= 1;
        }
    }

    /// Returns true if the dwarf is living
    pub fn is_alive(&self) -> bool {
        self.status.state == State::Living
    }

    /// Update state for a tick
    pub fn update_state(&mut self, events: &mut Vec<Event>) {
        self.dehydrate();

        match self.thirst() {
            Thirst::Dessicated => self.disintegrate(),
            Thirst::Dried => self.decompose(),
            Thirst::Dead => self.die(CauseOfDeath::Dehydration, events),
            _ => (),
        }
    }

    /// Create a new plan if the current one has been achieved
    /// Returns true if a new plan has been added.
    /// This is so that we spend the tick planning (Dwarfs need
    /// thinking time)
    pub fn update_plan(&mut self, layer: &mut Layer) -> bool {
        self.plan
            .as_ref()
            .map_or(true, |plan| plan.is_achieved(self))
            && self.add_plan(layer)
    }

    /// Execute a single tick for the dwarf
    pub fn tick(&mut self, events: &mut Vec<Event>, layer: &mut Layer) -> bool {
        self.update_state(events);

        self.can_act()
            && (self.update_plan(layer)
                || self.on_the_way(layer)
                || self.observe(layer)
                || self.walk(layer))
    }

    /// Returns the symbol for the dwarf including status and
    /// condition but without the terrain mixed in.
    pub fn symbol(&self) -> Option<Symbol> {
        let thirst = 255 - self.hydration;
        let facing = self.status.facing;
        match self.status.state {
            State::Living => {
                Some(Symbol::dwarf(facing).merge(Symbol::thirsty(thirst)))
            }
            State::Dead => Some(Symbol::corpse()),
            State::Skeleton => Some(Symbol::skeleton()),
            State::Ghost => Some(Symbol::ghost(facing)),
        }
    }

    pub fn thirst(&self) -> Thirst {
        match self.hydration {
            0 => Thirst::Dessicated,
            1..=70 => Thirst::Dried,
            71..=100 => Thirst::Dead,
            101..=200 => Thirst::Thirsty,
            _ => Thirst::Fine,
        }
    }

    pub fn is_thirsty(&self) -> bool {
        self.is_alive() && self.thirst() == Thirst::Thirsty
    }

    /// Returns the description of the dwarf including status
    /// and condition, but without the terrain mixed in.
    pub fn description(&self) -> Option<Description> {
        let cod = self.cause_of_death;
        match self.status.state {
            State::Living => {
                let mut description = Description::new("Dwarf");
                if self.is_thirsty() {
                    description =
                        description.merge(Description::new("thirsty"));
                }
                if self.has_contract {
                    description = description.merge(Description::new(
                        &format!("Power: {}", self.power),
                    ));
                }
                description = description.merge(Description::new(&format!(
                    "Plan: {}",
                    self.plan_string(),
                )));
                description = description.merge(Description::new(&format!(
                    "Observing: {}",
                    self.status.observation
                )));
                Some(description)
            }
            State::Dead => {
                let mut description = Description::new("Dwarf corpse");
                if let Some(cod) = cod {
                    description = description.merge(Description::new(
                        &format!("Cause of death: {}", cod),
                    ));
                }
                Some(description)
            }
            State::Skeleton => Some(Description::new("Dwarf skeleton")),
            State::Ghost => Some(Description::new("Dwarf ghost")),
        }
    }

    pub fn commands(&self, commands: &mut Vec<Command>) {
        if let Some(id) = self.id {
            commands.clear();
            if !self.has_contract {
                commands.push(Command::OfferContract(id));
            }
        }
    }

    fn apply_command(&mut self, commands: &mut Vec<Command>) {
        commands.retain(|command| match command {
            Command::OfferContract(id) => {
                if self.id == Some(*id) {
                    self.has_contract = true;
                    false
                } else {
                    true
                }
            }
        });
    }

    /// Apply the results of game actions to the player
    /// Events are things that happen as a result of the game updating
    /// its state They are part of the game mechanics
    fn apply_events(&mut self, events: &mut Vec<Event>) {
        events.retain(|event| {
            match event {
                Event::Death(_id) => true,
                Event::DistributePower(power) => {
                    self.power += power;
                    // Leave it in the events so the next dwarf can get power
                    // too
                    true
                }
            }
        });
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::test_setup::Setup;

    #[test]
    fn dehydrate() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        let mut dwarf = Dwarf::new(layer::Location::new(2, 2));

        assert_eq!(dwarf.hydration, 255);
        dwarf.dehydrate();
        assert_eq!(dwarf.hydration, 254);

        dwarf.drink();
        assert_eq!(dwarf.hydration, 255);

        dwarf.hydration = 0;
        assert_eq!(dwarf.thirst(), Thirst::Dessicated);
    }

    #[test]
    fn observe_nothing() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        let dwarf = Dwarf::new(layer::Location::new(2, 2));
        assert_eq!(dwarf.is_thirsty(), false);
        assert_eq!(
            dwarf.description(),
            Some(Description::new("Dwarf, Plan: None, Observing: Nothing"))
        );
    }

    #[test]
    fn observe_water() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_pool((3, 3), 1);
        let mut dwarf = Dwarf::new(layer::Location::new(2, 2));

        // Make it easier to do the tests.  He walks one tile per turn.
        dwarf.speed = 1.0;

        let mut layer = setup.layer;

        assert_eq!(dwarf.is_thirsty(), false);
        let mut events = vec![];

        // The dwarf creates a plan to wander, since they are thinking, they
        // don't observe the water
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Wander ((8, 2)), Observing: Nothing"
            ))
        );
        assert_eq!(dwarf.location(), layer::Location::new(2, 2));

        // The dwarf has a plan now, and can observe the water
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Wander ((8, 2)), Observing: Discovered Water"
            ))
        );
        assert_eq!(dwarf.location(), layer::Location::new(3, 1));
    }

    #[test]
    fn finish_wandering() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_pool((3, 3), 1);
        let mut dwarf = Dwarf::new(layer::Location::new(2, 2));

        // Make it easier to do the tests.  He walks one tile per turn.
        dwarf.speed = 1.0;

        let mut layer = setup.layer;

        assert_eq!(dwarf.is_thirsty(), false);
        let mut events = vec![];

        // The dwarf creates a plan to wander, since they are thinking, they
        // don't observe the water
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Wander ((8, 2)), Observing: Nothing"
            ))
        );
        assert_eq!(dwarf.location(), layer::Location::new(2, 2));

        // Walk to the end of the wandering plan
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);

        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Wander (None), Observing: Nothing"
            ))
        );
        assert_eq!(dwarf.location(), layer::Location::new(8, 2));
    }

    #[test]
    fn plan_drinking() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_pool((3, 3), 1);
        let mut dwarf = Dwarf::new(layer::Location::new(2, 2));

        // Make it easier to do the tests.  He walks one tile per turn.
        dwarf.speed = 1.0;

        let mut layer = setup.layer;

        assert_eq!(dwarf.is_thirsty(), false);
        let mut events = vec![];

        // The dwarf creates a plan to wander, since they are thinking, they
        // don't observe the water
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Wander ((8, 2)), Observing: Nothing"
            ))
        );
        assert_eq!(dwarf.location(), layer::Location::new(2, 2));

        // Walk to the end of the wandering plan
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);

        assert_eq!(dwarf.location(), layer::Location::new(8, 2));
        dwarf.hydration = 119;
        assert_eq!(dwarf.is_thirsty(), true);

        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, thirsty, Plan: Drink ((2, 2)), Observing: Nothing"
            ))
        );
    }

    #[test]
    fn go_to_drink() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_pool((3, 3), 1);
        let mut dwarf = Dwarf::new(layer::Location::new(2, 2));

        // Make it easier to do the tests.  He walks one tile per turn.
        dwarf.speed = 1.0;

        let mut layer = setup.layer;

        assert_eq!(dwarf.is_thirsty(), false);
        let mut events = vec![];

        // The dwarf creates a plan to wander, since they are thinking, they
        // don't observe the water
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(dwarf.location(), layer::Location::new(2, 2));

        // Walk to the end of the wandering plan
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);

        assert_eq!(dwarf.location(), layer::Location::new(8, 2));
        dwarf.hydration = 119;
        assert_eq!(dwarf.is_thirsty(), true);

        // Walk to the end of the wandering plan
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);
        dwarf.tick(&mut events, &mut layer);

        assert_eq!(dwarf.cause_of_death, None);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, thirsty, Plan: Drink (None), Observing: Nothing"
            ))
        );
        assert_eq!(dwarf.location(), layer::Location::new(2, 2));
        assert_eq!(dwarf.is_thirsty(), true);

        // Dwarf drinks
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Drink (None), Observing: Nothing"
            ))
        );
        assert_eq!(dwarf.is_thirsty(), false);

        // Dwarf makes a new plan
        dwarf.tick(&mut events, &mut layer);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Plan: Wander ((7, 4)), Observing: Nothing"
            ))
        );
    }

    #[test]
    fn test_update_progress() {
        let dwarf = Dwarf::new(layer::Location::new(2, 2));

        // Initial speed and progress for a dwarf
        assert_eq!(dwarf.speed, 0.1);
        assert_eq!(dwarf.status.progress, 0.0);

        assert_eq!(dwarf.status.steps(dwarf.speed), (0, 0.1));
    }

    #[test]
    fn test_walk() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);

        let mut dwarf = Dwarf::new(layer::Location::new(2, 2));
        dwarf.speed = 1.0;

        // Without a trail nothing happens
        assert!(dwarf.trail().is_none());
        assert_eq!(dwarf.walk(&mut setup.layer), false);
        assert!(dwarf.trail().is_none());

        // With an empty trail nothing happens
        dwarf.plan = Some(Plan::wander(Some(Trail::new())));
        assert_eq!(dwarf.trail().unwrap().is_empty(), true);
        assert_eq!(dwarf.walk(&mut setup.layer), false);
        assert_eq!(dwarf.trail().unwrap().is_empty(), true);

        // With an full trail
        let mut trail = Trail::new();
        trail.push_back(layer::Location::new(2, 3));
        trail.push_back(layer::Location::new(3, 3));
        dwarf.plan = Some(Plan::wander(Some(trail)));

        assert_eq!(dwarf.walk(&mut setup.layer), true);
        assert_eq!(dwarf.location(), layer::Location::new(2, 3));
        assert_eq!(dwarf.walk(&mut setup.layer), true);
        assert_eq!(dwarf.location(), layer::Location::new(3, 3));

        // Try to walk into a wall
        dwarf.status.location = layer::Location::new(1, 1);
        let mut trail = Trail::new();
        trail.push_back(layer::Location::new(1, 0));
        trail.push_back(layer::Location::new(0, 0));
        dwarf.plan = Some(Plan::wander(Some(trail)));
        assert_eq!(dwarf.walk(&mut setup.layer), false);
        assert_eq!(dwarf.location(), layer::Location::new(1, 1));
    }

    #[test]
    fn living_tick() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_dwarf((1, 1));
        let population = &mut setup.population;
        let dwarf = population.get_mut(0).unwrap();
        let layer = &mut setup.layer;

        let mut events: Vec<Event> = Default::default();
        dwarf.tick(&mut events, layer);
        // Dwarf is alive by default
        assert_eq!(dwarf.status.state, State::Living);
        assert_eq!(events.len(), 0);

        let mut events: Vec<Event> = Default::default();
        dwarf.hydration = 101;
        dwarf.tick(&mut events, layer);
        // Dwarf is dead due to dehydration
        assert_eq!(dwarf.status.state, State::Dead);
        assert_eq!(events.len(), 1);
        assert_eq!(events.get(0), Some(&Event::Death(dwarf.id.unwrap())));

        let mut events: Vec<Event> = Default::default();
        dwarf.hydration = 71;
        dwarf.tick(&mut events, layer);
        // Dwarf is a skeleton due to decomposition
        assert_eq!(dwarf.status.state, State::Skeleton);
        assert_eq!(events.len(), 0);

        let mut events: Vec<Event> = Default::default();
        dwarf.hydration = 1;
        dwarf.tick(&mut events, layer);
        // Dwarf is a ghost now
        assert_eq!(dwarf.status.state, State::Ghost);
        assert_eq!(events.len(), 0);
    }

    #[test]
    fn description() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_dwarf((1, 1));
        let population = &mut setup.population;
        let mut dwarf = population.get_mut(0).unwrap();

        assert_eq!(
            dwarf.description(),
            Some(Description::new("Dwarf, Plan: None, Observing: Nothing"))
        );

        // Add a contract
        dwarf.has_contract = true;
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, Power: 0, Plan: None, Observing: Nothing"
            ))
        );

        // Make him thirsty
        dwarf.hydration = 119;
        assert_eq!(dwarf.thirst(), Thirst::Thirsty);
        assert_eq!(
            dwarf.description(),
            Some(Description::new(
                "Dwarf, thirsty, Power: 0, Plan: None, Observing: Nothing"
            ))
        );
    }

    #[test]
    fn rehydrate() {
        let mut setup = Setup::default();
        setup.with_room((1, 1), 8);
        setup.with_dwarf((1, 1));
        let population = &mut setup.population;
        let dwarf = population.get_mut(0).unwrap();
        let layer = &mut setup.layer;

        // The dwarf dehydrates 1 per tick
        let mut events: Vec<Event> = Default::default();
        dwarf.tick(&mut events, layer);
        assert_eq!(dwarf.hydration, 254);

        // A dwarf uses his power to rehydrate when he is thirsty
        let mut events: Vec<Event> = Default::default();
        dwarf.power = 500;
        dwarf.hydration = 200;
        dwarf.tick(&mut events, layer);
        assert_eq!(dwarf.hydration, 255);
        assert_eq!(dwarf.power, 0);

        // A dwarf that dies of thirst doesn't use his power
        let mut events: Vec<Event> = Default::default();
        dwarf.power = 5000;
        dwarf.hydration = 101;
        dwarf.tick(&mut events, layer);
        assert_eq!(dwarf.hydration, 100);
        assert_eq!(dwarf.is_alive(), false);
        // A dwarf's power is removed when he dies
        assert_eq!(dwarf.power, 0);
    }
}
