use strum::IntoEnumIterator;

use super::{Layer, Location};
use crate::game::location::Direction;

pub struct SurroundingIter<'a> {
    center: Location,
    layer: &'a Layer,
    direction_iter: <Direction as IntoEnumIterator>::Iterator,
}

impl<'a> Iterator for SurroundingIter<'a> {
    type Item = Location;

    /// Returns the next `Grid` `Location` in breadth-wise order.
    /// If there are no more locations on the grid, returns `None`.
    fn next(&mut self) -> Option<Location> {
        let mut direction;
        let mut location;

        loop {
            direction = self.direction_iter.next();
            location = direction
                .and_then(|dir| self.layer.try_walk(&self.center, dir));
            if direction.is_none() || location.is_some() {
                break;
            }
        }
        location
    }
}

impl<'a> SurroundingIter<'a> {
    pub fn new(location: Location, layer: &'a Layer) -> Self {
        SurroundingIter {
            center: location,
            layer,
            direction_iter: Direction::iter(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn at_location_outside_the_layer() {
        let layer = Layer::new(0, 0, "seed");
        assert_eq!(
            layer.contained_location(Location::new(0, 0)).is_none(),
            true
        );

        let layer = Layer::new(10, 1, "seed");
        assert_eq!(
            layer.contained_location(Location::new(11, 11)).is_none(),
            true
        );
    }

    #[test]
    fn surrounding_iter_on_corner_of_layer() {
        let layer = Layer::new(10, 1, "seed");
        let visited: Vec<Location> = layer
            .contained_location(Location::new(0, 0))
            .unwrap()
            .into_surrounding_iter(&layer)
            .collect();
        assert_eq!(
            visited,
            [
                Location::new(1, 0),
                Location::new(1, 1),
                Location::new(0, 1)
            ]
        );
    }

    #[test]
    fn surrounding_iter() {
        let layer = Layer::new(10, 1, "seed");
        let visited: Vec<Location> = layer
            .contained_location(Location::new(5, 5))
            .unwrap()
            .into_surrounding_iter(&layer)
            .collect();
        assert_eq!(
            visited,
            [
                Location::new(4, 4),
                Location::new(5, 4),
                Location::new(6, 4),
                Location::new(6, 5),
                Location::new(6, 6),
                Location::new(5, 6),
                Location::new(4, 6),
                Location::new(4, 5),
            ]
        );
    }
}
