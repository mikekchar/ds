//! Locations on the Layer

use super::{surrounding_iter::SurroundingIter, Layer};
use crate::game::{
    coord::CheckedOps,
    grid::{self, Grid},
    location::{self, Coord, Offset},
};
use std::ops::Deref;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone, Copy)]
pub struct Location(location::Location);

// You can only add Offsets to Locations
// If you want to add or subtract Locations, use
// Offset::lossy_from(my_location)
impl CheckedOps<Offset> for Location {
    /// Try to add an Offset to a Location.
    /// Returns `None` if the result would overflow.
    fn checked_add(self, other: Offset) -> Option<Self> {
        self.0.checked_add(other).map(Self)
    }

    /// Try to subtract an Offset from a Location.
    /// Returns `None` if the result would be less than zero.
    fn checked_sub(self, other: Offset) -> Option<Self> {
        self.0.checked_sub(other).map(Self)
    }
}

impl Deref for Location {
    type Target = location::Location;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Location {
    /// Create a `Location` for the map `Layer`
    pub fn new(x: impl Into<Coord>, y: impl Into<Coord>) -> Self {
        Self(location::Location::new(x, y))
    }

    pub fn from_index(
        index: location::coord::Internal,
        width: impl Into<Coord>,
    ) -> Self {
        let width = location::coord::Internal::from(width.into());
        Self(location::Location::new(index % width, index / width))
    }

    /// Calculates the layer location given a grid location.
    /// If the layer doesn't extend to this part of the grid, returns
    /// None.
    pub fn from_grid_location(
        grid_location: grid::Location,
        grid: &Grid,
        layer: &Layer,
    ) -> Option<Location> {
        grid.layer_location
            .and_then(|layer_location| {
                grid_location
                    .offset_from(&grid.center().into())
                    .and_then(|offset| layer_location.checked_add(offset))
            })
            .filter(|l| layer.contains(l))
    }

    pub fn into_surrounding_iter(self, layer: &Layer) -> SurroundingIter {
        SurroundingIter::new(self, layer)
    }

    pub fn surrounding_locations(self, layer: &Layer) -> Vec<Location> {
        self.into_surrounding_iter(layer).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::test_setup::Setup;

    #[test]
    fn surrounding_locations() {
        let layer = &Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let expected = vec![
            Location::new(1, 0),
            Location::new(1, 1),
            Location::new(0, 1),
        ];
        assert_eq!(location.surrounding_locations(layer), expected);
    }
}
