//! This represents code for 3x3 blocks in a layer.  For doing various
//! calculations

use super::Layer;
use crate::game::{
    client::tile,
    layer,
    location::{
        direction::{CARDINAL_DIRS, INTERCARDINAL_DIRS},
        Direction,
    },
    map::MapTile,
};

const OFFSETS: [usize; 31] = [
    19, 23, 27, 31, 38, 39, 46, 47, 55, 63, 76, 77, 78, 79, 95, 110, 111, 127,
    137, 139, 141, 143, 155, 159, 175, 191, 205, 207, 223, 239, 255,
];

/// Represents a 3x3 block of tiles with a particular location
/// in the center. Used for tiling the tile sets
pub struct Block<'a> {
    pub center: layer::Location,
    pub layer: &'a Layer,
}

impl<'a> Block<'a> {
    pub fn new(layer: &'a Layer, center: layer::Location) -> Self {
        Self { center, layer }
    }

    fn index_increment<F>(
        &self,
        map_tile: MapTile,
        dir: Direction,
        increment: F,
    ) -> usize
    where
        F: Fn() -> usize,
    {
        self.layer
            .try_walk(&self.center, dir)
            .map_or(0, |test_loc| {
                if self.layer.map.get_tile(test_loc) == Some(map_tile) {
                    increment()
                } else {
                    0
                }
            })
    }

    fn cardinal_index(&self, map_tile: MapTile) -> usize {
        let mut card_index = 0;
        for dir in &CARDINAL_DIRS {
            card_index +=
                self.index_increment(map_tile, *dir, || *dir as usize);
        }

        card_index
    }

    fn intercardinal_index(
        &self,
        map_tile: MapTile,
        card_index: usize,
    ) -> usize {
        let mut diag_index = 0;

        for dir in &INTERCARDINAL_DIRS {
            diag_index += self.index_increment(map_tile, *dir, || {
                if self.is_attached(*dir, card_index) {
                    (*dir as usize) >> 4
                } else {
                    0
                }
            });
        }

        diag_index
    }

    pub fn wall_index(&self) -> tile::sheet::Index {
        let card_index = self.cardinal_index(MapTile::Terrain);
        let diag_index = self.intercardinal_index(MapTile::Terrain, card_index);

        if diag_index == 0 {
            card_index
        } else {
            OFFSETS
                .iter()
                .position(|&r| r == card_index + diag_index * 16)
                .unwrap_or(25)
                + 16
        }
    }

    pub fn pool_index(&self) -> tile::sheet::Index {
        let card_index = self.cardinal_index(MapTile::Pool);
        let diag_index = self.intercardinal_index(MapTile::Pool, card_index);

        if diag_index == 0 {
            card_index
        } else {
            OFFSETS
                .iter()
                .position(|&r| r == card_index + diag_index * 16)
                .unwrap_or(25)
                + 16
        }
    }

    /// In a 3x3 block, returns true if the tile in the given
    /// direction is attached to the middle tile
    pub fn is_attached(&self, dir: Direction, card_index: usize) -> bool {
        if (dir as usize) < 16 {
            return true;
        }
        let value = (dir as usize) >> 4;

        let mask = if value == 8 { 9 } else { value * 3 };
        (card_index & mask) == mask
    }
}
