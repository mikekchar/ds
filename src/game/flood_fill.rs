use super::{
    layer::{Layer, Location},
    zone_map::{Zone, ZoneId},
};
use std::collections::BinaryHeap;

pub type LocationQueue = BinaryHeap<Location>;

pub fn flood_fill(location: Location, zone_id: ZoneId, layer: &mut Layer) {
    if let Some(zone) = layer.zone(location) {
        if zone == Zone::NoZone {
            layer.set_zone(location, Zone::ZoneId(zone_id));

            let mut q = LocationQueue::default();
            q.push(location);
            while !q.is_empty() {
                if let Some(location) = q.pop() {
                    for neighbour in location.surrounding_locations(layer) {
                        if let Some(zone) = layer.zone(neighbour) {
                            if zone == Zone::NoZone {
                                layer
                                    .set_zone(neighbour, Zone::ZoneId(zone_id));
                                q.push(neighbour);
                            }
                        }
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::test_setup::Setup;

    #[test]
    fn sets_new_zone() {
        let layer = &mut Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let zone_id = 42;

        assert_eq!(layer.zone(location), Some(Zone::NoZone));
        flood_fill(location, zone_id, layer);
        assert_eq!(layer.zone(location), Some(Zone::ZoneId(zone_id)));
    }

    #[test]
    fn does_not_set_zone_on_impassable() {
        let layer = &mut Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let zone_id = 42;

        layer.set_zone(location, Zone::Impassable);
        assert_eq!(layer.zone(location), Some(Zone::Impassable));
        flood_fill(location, zone_id, layer);
        assert_eq!(layer.zone(location), Some(Zone::Impassable));
    }

    #[test]
    fn does_not_set_zone_on_already_set() {
        let layer = &mut Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let zone_id = 42;

        layer.set_zone(location, Zone::ZoneId(12));
        assert_eq!(layer.zone(location), Some(Zone::ZoneId(12)));
        flood_fill(location, zone_id, layer);
        assert_eq!(layer.zone(location), Some(Zone::ZoneId(12)));
    }

    #[test]
    fn flood_fills_everything_on_empty() {
        let layer = &mut Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let zone_id = 42;

        flood_fill(location, zone_id, layer);
        for zone in &layer.zone_map.zones {
            assert_eq!(*zone, Zone::ZoneId(zone_id));
        }
    }

    #[test]
    fn flood_fill_cant_cross_line() {
        let layer = &mut Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let zone_id = 42;

        for i in 0..10 {
            layer.set_zone(Location::new(5, i), Zone::ZoneId(12));
        }

        flood_fill(location, zone_id, layer);
        assert_eq!(
            layer.zone(Location::new(2, 2)),
            Some(Zone::ZoneId(zone_id))
        );
        assert_eq!(layer.zone(Location::new(7, 7)), Some(Zone::NoZone));
    }

    #[test]
    fn flood_fill_floods_through_hole_in_wall() {
        let layer = &mut Setup::new(10, 10, 1).layer;
        let location = Location::new(0, 0);
        let zone_id = 42;

        for i in 0..10 {
            layer.set_zone(Location::new(5, i), Zone::ZoneId(12));
        }
        layer.set_zone(Location::new(5, 5), Zone::NoZone);

        flood_fill(location, zone_id, layer);
        assert_eq!(
            layer.zone(Location::new(2, 2)),
            Some(Zone::ZoneId(zone_id))
        );
        assert_eq!(
            layer.zone(Location::new(7, 7)),
            Some(Zone::ZoneId(zone_id))
        );
    }
}
