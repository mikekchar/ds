//! Represents a map on a layer

use rand::Rng as RngTrait;

use super::{
    layer,
    location,
    rng::Rng,
    symbol::Description,
    water_map::{WaterMap, WaterTile},
};

/// The types of objects that might be in a map tile
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MapTile {
    Terrain,
    Floor,
    Pool,
}

impl MapTile {
    pub fn description(&self) -> Description {
        Description::new(format!("{:?}", self))
    }
}

/// Represents the terrain on a layer
#[derive(Debug)]
pub struct Map {
    pub width: location::Coord,
    pub height: location::Coord,
    pub rng: Rng,
    pub tiles: Vec<MapTile>,
}

impl Map {
    pub fn new(
        width: impl Into<location::Coord>,
        height: impl Into<location::Coord>,
        rng: &Rng,
    ) -> Map {
        let width: location::Coord = width.into();
        let height: location::Coord = height.into();
        let num_tiles = location::coord::Internal::from(width * height);

        Map {
            width,
            height,
            rng: rng.clone(),
            tiles: vec![MapTile::Terrain; num_tiles],
        }
    }

    pub fn fill(mut self, water_map: &WaterMap) -> Self {
        self.create_caverns(water_map);
        self
    }

    pub fn create_contents(&mut self, water_map: &WaterMap) {
        let mut room_rng = self.rng.clone();
        let room_size = self.room_size(&mut room_rng);
        if let Some(room_location) =
            self.room_location(room_size, &mut room_rng)
        {
            self.add_room(room_location, room_size.0, room_size.1);
        }
        self.create_caverns(water_map);
    }

    fn room_size(&self, rng: &mut Rng) -> (location::Coord, location::Coord) {
        let room_width = rng.gen_range(2..=5);
        let room_height = rng.gen_range(2..=5);
        (room_width.into(), room_height.into())
    }

    fn room_location(
        &self,
        size: (location::Coord, location::Coord),
        rng: &mut Rng,
    ) -> Option<layer::Location> {
        if size.0 >= self.width || size.1 >= self.height {
            None
        } else {
            let max_x = self.width - size.0;
            let max_y = self.width - size.1;

            let x = rng.gen_range(1..location::coord::Internal::from(max_x));
            let y = rng.gen_range(1..location::coord::Internal::from(max_y));
            Some(layer::Location::new(x, y))
        }
    }

    pub fn add_space(
        &mut self,
        location: layer::Location,
        width: impl Into<location::Coord>,
        height: impl Into<location::Coord>,
        tile: MapTile,
    ) {
        let room_width = width.into();
        let room_height = height.into();

        // Don't add a room if it doesn't fit on the map
        for i in location::coord::Internal::from(location.x)
            ..location::coord::Internal::from(location.x + room_width)
        {
            for j in location::coord::Internal::from(location.y)
                ..location::coord::Internal::from(location.y + room_height)
            {
                self.tiles
                    [j * location::coord::Internal::from(self.width) + i] =
                    tile;
            }
        }
    }

    pub fn add_room(
        &mut self,
        location: layer::Location,
        width: impl Into<location::Coord>,
        height: impl Into<location::Coord>,
    ) {
        self.add_space(location, width, height, MapTile::Floor)
    }

    pub fn create_caverns(&mut self, water_map: &WaterMap) {
        for i in 0..water_map.len() {
            if let Some(WaterTile::Cavern(value)) = water_map.get(i) {
                if value > 110 {
                    self.tiles[i] = MapTile::Floor;
                }
                if value > 140 {
                    self.tiles[i] = MapTile::Pool;
                }
            }
        }
    }

    pub fn set_tile(
        &mut self,
        location: layer::Location,
        tile: MapTile,
    ) -> bool {
        self.index(location).map(|i| self.tiles[i] = tile).is_some()
    }

    pub fn get_tile(&self, location: layer::Location) -> Option<MapTile> {
        let index = self.index(location)?;
        self.get(index)
    }

    /// Returns the map tile for an index into the map.
    /// The index is a breadth first index into the array.
    pub fn get(&self, index: usize) -> Option<MapTile> {
        self.tiles.get(index).copied()
    }

    pub fn index(
        &self,
        location: layer::Location,
    ) -> Option<location::coord::Internal> {
        if location.x < self.width && location.y < self.height {
            Some(location.index(self.width))
        } else {
            None
        }
    }

    pub fn len(&self) -> usize {
        self.tiles.len()
    }

    pub fn is_empty(&self) -> bool {
        self.tiles.is_empty()
    }

    pub fn pathable(&self, location: layer::Location) -> bool {
        self.get_tile(location)
            .map(|tile| tile == MapTile::Floor)
            .unwrap_or(false)
    }

    pub fn pathable_locations(&self) -> Vec<layer::Location> {
        self.tiles
            .iter()
            .enumerate()
            .filter_map(|(i, t)| {
                if *t == MapTile::Floor {
                    Some(layer::Location::from_index(i, self.width))
                } else {
                    None
                }
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_map_new() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let seed_rng = Rng::new(seed);
        let map = Map::new(width, height, &seed_rng);
        assert_eq!(map.tiles.len(), 100);
    }

    #[test]
    fn test_map_room_size() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);
        let size = map.room_size(&mut map.rng.clone());

        assert_eq!(size, (location::Coord::from(5), location::Coord::from(4)));
    }

    #[test]
    fn test_map_room_location() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);
        let size = map.room_size(&mut rng);
        let room_location = map.room_location(size, &mut rng);

        assert_eq!(room_location, Some(layer::Location::new(2, 1)));

        assert_eq!(size, (5.into(), 4.into()));
        assert_eq!(
            map.room_location((11.into(), 5.into()), &mut map.rng.clone()),
            None
        );
        assert_eq!(
            map.room_location((5.into(), 11.into()), &mut map.rng.clone()),
            None
        );
    }

    #[test]
    fn test_index() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);

        assert_eq!(map.index(layer::Location::new(0, 0)), Some(0));
        assert_eq!(map.index(layer::Location::new(1, 1)), Some(11));
        assert_eq!(map.index(layer::Location::new(10, 10)), None);
        assert_eq!(map.index(layer::Location::new(10, 1)), None);
        assert_eq!(map.index(layer::Location::new(1, 10)), None);
    }

    #[test]
    fn test_get_set_tile() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        let location = layer::Location::new(1, 1);
        assert_eq!(map.get_tile(location), Some(MapTile::Terrain));
        assert_eq!(map.set_tile(location, MapTile::Floor), true);
        assert_eq!(map.get_tile(location), Some(MapTile::Floor));

        assert_eq!(
            map.set_tile(layer::Location::new(10, 10), MapTile::Floor),
            false
        );
    }

    #[test]
    fn test_get_tile_description() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        let location = layer::Location::new(1, 1);
        assert_eq!(
            map.get_tile(location).map(|tile| tile.description()),
            Some(Description::new("Terrain"))
        );
        assert_eq!(map.set_tile(location, MapTile::Floor), true);
        assert_eq!(
            map.get_tile(location).map(|tile| tile.description()),
            Some(Description::new("Floor"))
        );
        assert_eq!(
            map.get_tile(layer::Location::new(10, 10))
                .map(|tile| tile.description()),
            None
        );
    }

    #[test]
    fn test_add_room() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        map.add_room(layer::Location::new(0, 0), 2, 2);
        assert_eq!(
            map.get(layer::Location::new(0, 0).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(0, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(0, 2).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(1, 0).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(1, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(1, 2).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(2, 0).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(2, 1).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(2, 2).index(width)),
            Some(MapTile::Terrain)
        );

        map.add_room(layer::Location::new(2, 1), 2, 2);
        assert_eq!(
            map.get(layer::Location::new(2, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(2, 2).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(2, 3).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(3, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(3, 2).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(layer::Location::new(3, 3).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(4, 1).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(4, 2).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(layer::Location::new(4, 3).index(width)),
            Some(MapTile::Terrain)
        );
    }

    #[test]
    fn test_pathable() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        map.add_room(layer::Location::new(0, 0), 2, 2);

        assert_eq!(map.pathable(layer::Location::new(0, 0)), true);
        assert_eq!(map.pathable(layer::Location::new(0, 1)), true);
        assert_eq!(map.pathable(layer::Location::new(0, 2)), false);
        assert_eq!(map.pathable(layer::Location::new(1, 0)), true);
        assert_eq!(map.pathable(layer::Location::new(1, 1)), true);
        assert_eq!(map.pathable(layer::Location::new(1, 2)), false);
        assert_eq!(map.pathable(layer::Location::new(2, 0)), false);
        assert_eq!(map.pathable(layer::Location::new(2, 1)), false);
        assert_eq!(map.pathable(layer::Location::new(2, 2)), false);

        map.add_room(layer::Location::new(2, 1), 2, 2);
        assert_eq!(map.pathable(layer::Location::new(2, 1)), true);
        assert_eq!(map.pathable(layer::Location::new(2, 2)), true);
        assert_eq!(map.pathable(layer::Location::new(2, 3)), false);
        assert_eq!(map.pathable(layer::Location::new(3, 1)), true);
        assert_eq!(map.pathable(layer::Location::new(3, 2)), true);
        assert_eq!(map.pathable(layer::Location::new(3, 3)), false);
        assert_eq!(map.pathable(layer::Location::new(4, 1)), false);
        assert_eq!(map.pathable(layer::Location::new(4, 2)), false);
        assert_eq!(map.pathable(layer::Location::new(4, 3)), false);
    }
}
