//! Code for returning `Symbols` from map locations on a particular
//! map layer.

pub mod block;
mod layer_location;
mod surrounding_iter;

use rand::Rng as RngTrait;

use super::{
    config::app_config::AppConfig,
    coord::CheckedOps,
    event::Event,
    location::{self, Direction, Offset},
    map::{Map, MapTile},
    rng::Rng,
    symbol::{Description, Merge, Symbol},
    water_map::{WaterMap, WaterTile},
    zone_map::{Zone, ZoneMap},
};
use block::Block;
pub use layer_location::Location;

/// Represents a single layer of a map.
#[derive(Debug)]
pub struct Layer {
    pub height: location::Coord,
    pub width: location::Coord,
    pub map: Map,
    pub water_map: WaterMap,
    pub zone_map: ZoneMap,
    pub rngs: Vec<Rng>,
    pub seed: String,
}

fn create_rngs(
    width: location::Coord,
    height: location::Coord,
    mut rng: Rng,
) -> Vec<Rng> {
    // TODO: Return error if width * height + 1 >=
    // location::coord::Internal::max_value       or if it overflows a
    // Coord
    let num_rngs = location::coord::Internal::from(width * height);
    let mut rngs = Vec::with_capacity(num_rngs + 1);
    for _ in 0..=num_rngs {
        rng.jump();
        rngs.push(rng.clone());
    }
    rngs
}

impl Default for Layer {
    fn default() -> Self {
        Self::new(10, 10, "test_seed")
    }
}

impl Layer {
    /// Create a new layer with a specific height and width.
    pub fn new(chunks_per_side: usize, chunk_size: usize, seed: &str) -> Self {
        let rng = Rng::new(seed);
        let width = (chunks_per_side * chunk_size).into();
        let height = (chunks_per_side * chunk_size).into();
        let map = Map::new(width, height, &rng);
        let water_map = WaterMap::new(width, height, &rng);
        let zone_map = ZoneMap::new(width, height);
        Layer {
            width,
            height,
            seed: seed.to_string(),
            rngs: create_rngs(width, height, rng),
            map,
            water_map,
            zone_map,
        }
    }

    pub fn fill(mut self) -> Self {
        self.water_map = self.water_map.fill();
        self.map = self.map.fill(&self.water_map);
        self.zone_map.set_pathability(&self.map);
        self
    }

    pub fn from_config(app_config: &AppConfig) -> Self {
        let chunk_size = app_config.chunk_size;
        let chunks_per_side = app_config.chunks_per_side;
        let rng_seed = &app_config.rng_seed;
        Layer::new(chunk_size, chunks_per_side, rng_seed).fill()
    }

    /// Returns true if the map `Location` is contained in the layer.
    pub fn contains(&self, location: &Location) -> bool {
        location.x < self.width && location.y < self.height
    }

    /// Starts at a location.  Moves one square in the given
    /// direction. If there is no tile at the destination it
    /// returns None.
    pub fn try_walk(
        &self,
        location: &Location,
        dir: Direction,
    ) -> Option<Location> {
        location
            .checked_add(Offset::from(dir))
            .filter(|l| self.contains(l))
    }

    /// Returns Some(location) if it exists on the layer. Otherwise
    /// None.
    pub fn contained_location(&self, location: Location) -> Option<Location> {
        Some(location).filter(|location| self.contains(location))
    }

    pub fn exposed(&self, location: Location) -> bool {
        location.into_surrounding_iter(self).any(|l| {
            if let Some(terrain) = self.map.get_tile(l) {
                (terrain == MapTile::Floor) || (terrain == MapTile::Pool)
            } else {
                false
            }
        })
    }

    pub fn water_symbol(&self, location: Location) -> Option<Symbol> {
        let water = self.water_map.get_tile(location)?;

        match water {
            WaterTile::Cavern(level) => Some(Symbol::water(level)),
        }
    }

    pub fn zone(&self, location: Location) -> Option<Zone> {
        self.zone_map.get_tile(location)
    }

    pub fn set_zone(&mut self, location: Location, zone: Zone) {
        self.zone_map.set_tile(location, zone)
    }

    pub fn terrain_symbol(&self, location: Location) -> Option<Symbol> {
        let rng = self.rngs.get(location.index(self.width))?.clone();

        let terrain = self.map.get_tile(location)?;
        match terrain {
            MapTile::Terrain => {
                let mut tile = Symbol::undiscovered_terrain(rng);
                if self.exposed(location) {
                    tile = tile.merge(Symbol::wall(
                        Block::new(self, location).wall_index(),
                    ));
                }
                Some(tile)
            }
            MapTile::Floor => Some(Symbol::floor(rng)),
            MapTile::Pool => {
                let mut tile = Symbol::floor(rng);
                if self.exposed(location) {
                    tile = tile.merge(Symbol::pool(
                        Block::new(self, location).pool_index(),
                    ));
                }
                Some(tile)
            }
        }
    }

    pub fn terrain_description(
        &self,
        location: Location,
    ) -> Option<Description> {
        let terrain = self.map.get_tile(location)?;
        if terrain == MapTile::Terrain {
            let mut description = terrain.description();
            if self.exposed(location) {
                description = description.merge(Description::new("wall"));
            }
            Some(description)
        } else {
            Some(terrain.description())
        }
    }

    pub fn pathable(&self, location: Location) -> bool {
        self.map.pathable(location)
    }

    pub fn path_cost(&self, from: Location, to: Location) -> location::Coord {
        if self.pathable(to) {
            from.distance_to(&to)
        } else {
            location::Coord::max_value()
        }
    }

    pub fn index(&self, location: Location) -> location::coord::Internal {
        location.index(self.width) as location::coord::Internal
    }

    pub fn pathable_locations(&self) -> Vec<Location> {
        self.map.pathable_locations()
    }

    /// self is mut because we haveto update the RNG
    pub fn random_pathable_location(&mut self) -> Option<Location> {
        let locations = self.pathable_locations();
        let selection = self.map.rng.gen_range(0..=locations.len());
        locations.get(selection).copied()
    }

    /// Returns a dwarf spawn location `chance`% of the time.
    /// Otherwise None. The spawn location is randomly selected
    /// from all the pathable locations.
    pub fn dwarf_spawn_location(&mut self, chance: u32) -> Option<Location> {
        if self.map.rng.gen_range(0..100) < chance {
            self.random_pathable_location()
        } else {
            None
        }
    }

    pub fn tick(&mut self, _events: &mut Vec<Event>) -> bool {
        false
    }

    /// Returns the first pool that is next to the location
    pub fn nearest_pool(&self, location: Location) -> Option<Location> {
        location
            .into_surrounding_iter(self)
            .find(|l| self.map.get_tile(*l) == Some(MapTile::Pool))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::{
        config::Config,
        test_setup::{Setup, SEED},
    };

    #[test]
    fn test_create_rngs() {
        let seed_rng = Rng::new(SEED);
        let rngs = create_rngs(5.into(), 10.into(), seed_rng);
        assert_eq!(rngs.len(), 51);
    }

    #[test]
    fn test_layer_contains() {
        let layer = &Setup::new(10, 10, 1).layer;
        assert_eq!(layer.contains(&Location::new(0, 0)), true);
        assert_eq!(layer.contains(&Location::new(9, 9)), true);
        assert_eq!(layer.contains(&Location::new(10, 9)), false);
        assert_eq!(layer.contains(&Location::new(9, 10)), false);
        assert_eq!(layer.contains(&Location::new(10, 10)), false);
    }

    #[test]
    fn test_try_walk_from_upper_left() {
        let layer = &Setup::new(10, 10, 1).layer;
        let start = Location::new(0, 0);

        assert_eq!(layer.try_walk(&start, Direction::Left), None);
        assert_eq!(layer.try_walk(&start, Direction::UpLeft), None);
        assert_eq!(layer.try_walk(&start, Direction::Up), None);
        assert_eq!(layer.try_walk(&start, Direction::UpRight), None);
        assert_eq!(
            layer.try_walk(&start, Direction::Right),
            Some(Location::new(1, 0))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::DownRight),
            Some(Location::new(1, 1))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Down),
            Some(Location::new(0, 1))
        );
        assert_eq!(layer.try_walk(&start, Direction::DownLeft), None);
    }

    #[test]
    fn test_try_walk_from_lower_right() {
        let layer = &Setup::new(10, 10, 1).layer;
        let start = Location::new(9, 9);

        assert_eq!(
            layer.try_walk(&start, Direction::Left),
            Some(Location::new(8, 9))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::UpLeft),
            Some(Location::new(8, 8))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Up),
            Some(Location::new(9, 8))
        );
        assert_eq!(layer.try_walk(&start, Direction::UpRight), None);
        assert_eq!(layer.try_walk(&start, Direction::Right), None);
        assert_eq!(layer.try_walk(&start, Direction::DownRight), None);
        assert_eq!(layer.try_walk(&start, Direction::Down), None);
        assert_eq!(layer.try_walk(&start, Direction::DownLeft), None);
    }

    #[test]
    fn test_try_walk_from_middle() {
        let layer = &Setup::new(10, 10, 1).layer;
        let start = Location::new(2, 5);

        assert_eq!(
            layer.try_walk(&start, Direction::Left),
            Some(Location::new(1, 5))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::UpLeft),
            Some(Location::new(1, 4))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Up),
            Some(Location::new(2, 4))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::UpRight),
            Some(Location::new(3, 4))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Right),
            Some(Location::new(3, 5))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::DownRight),
            Some(Location::new(3, 6))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Down),
            Some(Location::new(2, 6))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::DownLeft),
            Some(Location::new(1, 6))
        );
    }

    #[test]
    fn test_pathable() {
        let layer = &Setup::default().layer;

        // Locations outside of the layer are not pathable
        assert_eq!(layer.pathable(Location::new(15, 15)), false)
    }

    #[test]
    fn test_terrain_symbol_and_description() {
        let layer = &mut Setup::default().layer;

        layer.map.add_room(Location::new(5, 5), 1, 1);
        let rng = layer
            .rngs
            .get(Location::new(5, 5).index(layer.width))
            .unwrap()
            .clone();

        // Room is a floor
        assert_eq!(
            layer.terrain_symbol(Location::new(5, 5)),
            Some(Symbol::floor(rng))
        );

        // The terrain surrounding the floor is a wall
        // The terrain will be in fg_index[0] and the wall in fg_index[1]
        assert_eq!(
            layer
                .terrain_symbol(Location::new(4, 4))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            44
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(4, 5))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            42
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(4, 6))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            45
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(5, 4))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            38
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(5, 6))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            31
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(6, 4))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            41
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(6, 5))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            24
        );

        assert_eq!(
            layer
                .terrain_symbol(Location::new(6, 6))
                .unwrap()
                .glyph_index[1]
                .value
                .index
                - 48,
            33
        );
    }

    #[test]
    fn at_location_outside_the_layer() {
        let layer = Layer::new(0, 0, "seed");
        assert_eq!(
            layer.contained_location(Location::new(0, 0)).is_none(),
            true
        );

        let layer = Layer::new(10, 1, "seed");
        assert_eq!(
            layer.contained_location(Location::new(11, 11)).is_none(),
            true
        );
    }

    #[test]
    fn block_wall_index() {
        let mut layer = Setup::new(10, 3, 1).layer;
        let center = Location::new(1, 1);

        for i in 0..=2 {
            for j in 0..=2 {
                layer.map.set_tile(Location::new(i, j), MapTile::Floor);
            }
        }

        layer.map.set_tile(center, MapTile::Terrain);
        assert_eq!(Block::new(&layer, center).wall_index(), 0);

        layer.map.set_tile(
            layer.try_walk(&center, Direction::Up).unwrap(),
            MapTile::Terrain,
        );
        assert_eq!(Block::new(&layer, center).wall_index(), 1);

        layer.map.set_tile(
            layer.try_walk(&center, Direction::Right).unwrap(),
            MapTile::Terrain,
        );
        assert_eq!(Block::new(&layer, center).wall_index(), 3);

        layer.map.set_tile(
            layer.try_walk(&center, Direction::Down).unwrap(),
            MapTile::Terrain,
        );
        assert_eq!(Block::new(&layer, center).wall_index(), 7);

        layer.map.set_tile(
            layer.try_walk(&center, Direction::Left).unwrap(),
            MapTile::Terrain,
        );
        assert_eq!(Block::new(&layer, center).wall_index(), 15);
    }

    #[test]
    fn pathable_locations() {
        let mut setup = Setup::default();
        setup.with_room((5, 5), 3);
        let layer = &setup.layer;
        let locations: Vec<Location> =
            layer.pathable_locations().iter().copied().collect();
        assert_eq!(
            locations,
            vec![
                Location::new(5, 5),
                Location::new(6, 5),
                Location::new(7, 5),
                Location::new(5, 6),
                Location::new(6, 6),
                Location::new(7, 6),
                Location::new(5, 7),
                Location::new(6, 7),
                Location::new(7, 7),
            ]
        );
    }
    #[test]
    fn random_pathable_location() {
        let mut layer = Layer::from_config(&Config::instance().app_config);
        assert_eq!(layer.random_pathable_location().is_some(), true);
    }
}
