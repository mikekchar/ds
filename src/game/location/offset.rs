//! Offset between 2 locations
use super::{Direction, Location};
use crate::game::coord::{CheckedOps, LossyFrom};
use std::ops::Neg;

pub type Internal = i64;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Offset {
    pub x: Internal,
    pub y: Internal,
}

impl Neg for Offset {
    type Output = Offset;

    fn neg(self) -> Self::Output {
        Offset {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl From<Direction> for Offset {
    fn from(dir: Direction) -> Self {
        match dir {
            Direction::Left => Self { x: -1, y: 0 },
            Direction::Up => Self { x: 0, y: -1 },
            Direction::Right => Self { x: 1, y: 0 },
            Direction::Down => Self { x: 0, y: 1 },
            Direction::UpLeft => Self { x: -1, y: -1 },
            Direction::UpRight => Self { x: 1, y: -1 },
            Direction::DownRight => Self { x: 1, y: 1 },
            Direction::DownLeft => Self { x: -1, y: 1 },
        }
    }
}

impl LossyFrom<Location> for Offset {
    fn lossy_from(location: Location) -> Self {
        Self {
            x: location.x.value() as Internal,
            y: location.y.value() as Internal,
        }
    }
}

/// Add and subtract Offsets from each other
/// This is the actual meat of the CheckOps for
/// Locations and Offsets.
impl CheckedOps<Offset> for Offset {
    fn checked_add(self, other: Offset) -> Option<Self> {
        self.x
            .checked_add(other.x)
            .and_then(|x| self.y.checked_add(other.y).map(|y| Self { x, y }))
    }

    fn checked_sub(self, other: Offset) -> Option<Self> {
        self.x
            .checked_sub(other.x)
            .and_then(|x| self.y.checked_sub(other.y).map(|y| Self { x, y }))
    }
}

impl Offset {
    pub fn new(x: Internal, y: Internal) -> Self {
        Self { x, y }
    }
}
