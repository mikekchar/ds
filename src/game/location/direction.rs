//! Direction from a location
use std::ops::Neg;

// So we can use Left, Right, etc in this file
use self::Direction::*;

#[derive(Clone, Copy, PartialEq, Eq, Debug, EnumIter, Hash)]
pub enum Direction {
    UpLeft = 128,
    Up = 1,
    UpRight = 16,
    Right = 2,
    DownRight = 32,
    Down = 4,
    DownLeft = 64,
    Left = 8,
}

pub const CARDINAL_DIRS: [Direction; 4] = [
    Direction::Up,
    Direction::Right,
    Direction::Down,
    Direction::Left,
];

pub const INTERCARDINAL_DIRS: [Direction; 4] = [
    Direction::UpRight,
    Direction::DownRight,
    Direction::DownLeft,
    Direction::UpLeft,
];

impl Neg for Direction {
    type Output = Self;

    fn neg(self) -> Self {
        match self {
            Left => Right,
            Right => Left,
            Up => Down,
            Down => Up,
            UpLeft => DownRight,
            DownRight => UpLeft,
            UpRight => DownLeft,
            DownLeft => UpRight,
        }
    }
}
