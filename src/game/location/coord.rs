use crate::game::client::point;
/// A newtype to hold coordinate types for locations on a grid or map
use crate::game::coord::{
    self, CheckedOps, CoordInner, LossyFrom, LossyInto, MaxValue, SaturatingOps,
};

// This *must* be usize because we use it for indexing
// vectors whose size type is usize (because that's what
// the vec! macro wants)
pub type Internal = usize;

impl SaturatingOps for Internal {
    fn saturating_add(self, x: Self) -> Self {
        self.saturating_add(x)
    }

    fn saturating_sub(self, x: Self) -> Self {
        self.saturating_sub(x)
    }

    fn saturating_mul(self, x: Self) -> Self {
        self.saturating_mul(x)
    }
}

impl CheckedOps<Internal> for Internal {
    fn checked_add(self, x: Internal) -> Option<Self> {
        self.checked_add(x)
    }

    fn checked_sub(self, x: Internal) -> Option<Self> {
        self.checked_sub(x)
    }
}

impl MaxValue for Internal {
    fn max_value() -> Internal {
        Internal::max_value()
    }
}

impl LossyFrom<f32> for Internal {
    fn lossy_from(x: f32) -> Self {
        x as Self
    }
}

impl LossyInto<f32> for Internal {
    fn lossy_into(self) -> f32 {
        self as f32
    }
}

impl CoordInner<Internal> for Internal {}

pub type Coord = coord::Coord<Internal>;

impl From<Internal> for Coord {
    fn from(x: Internal) -> Coord {
        Coord::new(x)
    }
}

impl From<Coord> for Internal {
    fn from(x: Coord) -> Internal {
        x.value()
    }
}

impl LossyFrom<Coord> for f64 {
    fn lossy_from(x: Coord) -> f64 {
        x.value() as f64
    }
}

impl LossyFrom<Coord> for point::Coord {
    fn lossy_from(x: Coord) -> point::Coord {
        point::Coord::from(x.value() as point::coord::Internal)
    }
}

impl Coord {
    pub fn num_tiles(
        total_size: impl Into<point::Coord>,
        tile_size: impl Into<point::Coord>,
    ) -> Self {
        let total = f32::lossy_from(total_size.into());
        let tile = f32::lossy_from(tile_size.into());
        Self::new(Internal::lossy_from((total / tile).floor()))
    }

    pub fn num_points(
        self,
        tile_size: impl Into<point::Coord>,
    ) -> point::Coord {
        (point::Coord::lossy_from(self)) * tile_size.into()
    }
}
