use noise::{
    Exponent,
    Multiply,
    NoiseFn,
    ScaleBias,
    ScalePoint,
    Seedable,
    SuperSimplex,
};

use crate::game::{layer, location, rng::Rng as Generator};
use rand::Rng;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum WaterTile {
    Cavern(usize),
}

/// Represents the moisture level on a layer
#[derive(Debug)]
pub struct WaterMap {
    pub width: location::Coord,
    pub height: location::Coord,
    pub rng: Generator,
    pub tiles: Vec<WaterTile>,
}

impl WaterMap {
    pub fn new(
        width: impl Into<location::Coord>,
        height: impl Into<location::Coord>,
        rng: &Generator,
    ) -> Self {
        let rng = rng.clone();
        let width: location::Coord = width.into();
        let height: location::Coord = height.into();
        let num_tiles = location::coord::Internal::from(width * height);
        let tiles = vec![WaterTile::Cavern(0); num_tiles];
        Self {
            width,
            height,
            rng,
            tiles,
        }
    }

    pub fn fill(mut self) -> Self {
        let moisture_gen = SuperSimplex::new().set_seed(self.rng.gen::<u32>());
        let moisture_freq = ScalePoint::new(&moisture_gen).set_scale(0.07);
        let moisture_zero = ScaleBias::new(&moisture_freq).set_bias(1.0);
        let moisture_norm = ScaleBias::new(&moisture_zero).set_scale(0.5);
        let moisture = Exponent::new(&moisture_norm).set_exponent(1.3);

        let cavern_gen = SuperSimplex::new().set_seed(self.rng.gen::<u32>());
        let cavern_freq = ScalePoint::new(&cavern_gen).set_scale(0.2);
        let cavern_zero = ScaleBias::new(&cavern_freq).set_bias(1.0);
        let cavern_norm = ScaleBias::new(&cavern_zero).set_scale(0.5);
        let cavern = Exponent::new(&cavern_norm).set_exponent(1.1);

        let combined = Multiply::new(&cavern, &moisture);
        let combined_norm = Exponent::new(&combined).set_exponent(0.85);

        let level_scale = ScaleBias::new(&combined_norm).set_scale(255.0);
        for x in 0..usize::from(self.width) {
            for y in 0..usize::from(self.height) {
                self.tiles[y * usize::from(self.width) + x] = WaterTile::Cavern(
                    level_scale.get([(x as f64), (y as f64)]) as usize,
                );
            }
        }
        self
    }

    pub fn index(
        &self,
        location: layer::Location,
    ) -> Option<location::coord::Internal> {
        if location.x < self.width && location.y < self.height {
            Some(location.index(self.width))
        } else {
            None
        }
    }

    /// Returns the map tile for an index into the map.
    /// The index is a breadth first index into the array.
    pub fn get(&self, index: usize) -> Option<WaterTile> {
        self.tiles.get(index).copied()
    }

    pub fn get_tile(&self, location: layer::Location) -> Option<WaterTile> {
        let l = layer::Location::new(location.x, location.y);
        let index = self.index(l)?;
        self.get(index)
    }

    pub fn len(&self) -> usize {
        self.tiles.len()
    }

    pub fn is_empty(&self) -> bool {
        self.tiles.is_empty()
    }
}
