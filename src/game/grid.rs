//! Code for dealing the with the map display.
mod grid_iter;
mod grid_location;

use ggez::{
    graphics::{Color, Mesh, MeshBuilder},
    Context,
    GameResult,
};

use crate::game::{
    client::{
        font::Font,
        point::{self, Point},
        rect::Rect,
    },
    coord::CheckedOps,
    layer,
    location,
};
use grid_iter::Iter;
pub use grid_location::Location;

/// Object for describing the map grid.
/// It doesn't actually contain anything in the grid.
/// It just describes the size of the grid, font and spacing used.
/// Includes an iterator for actually drawing the grid.
#[derive(Clone, Copy, Debug)]
pub struct Grid {
    pub width: location::Coord,
    pub height: location::Coord,
    pub rect: Rect,
    pub font: Font,
    pub padding: f32,
    pub cursor_location: Option<Location>,
    pub layer_location: Option<layer::Location>,
}

impl Grid {
    /// Describe a new grid
    pub fn zero(rect: Rect, font: Font, padding: f32) -> Grid {
        let tile_size = font.size.scale_by(padding);
        let width = location::Coord::num_tiles(rect.width(), tile_size);
        let height = location::Coord::num_tiles(rect.height(), tile_size);

        Grid {
            width,
            height,
            rect,
            font,
            padding,
            cursor_location: None,
            layer_location: None,
        }
    }

    pub fn new(rect: Rect, font: Font, padding: f32) -> Grid {
        let mut grid = Self::zero(rect, font, padding);
        grid.width = grid.width + 1;
        grid.height = grid.height + 1;
        grid
    }

    /// Returns the size of a square in the grid in pixels.
    pub fn tile_size(&self) -> point::Coord {
        self.font.size.scale_by(self.padding)
    }

    /// Returns the size of the grid in pixels.
    pub fn size(&self) -> Rect {
        self.rect
    }

    pub fn destination(&self) -> Point {
        self.rect.destination()
    }

    /// Returns a point on the screen for the upper left hand corner
    /// of a location.  The point is relative to the upper left
    /// hand corner of the grid.
    pub fn point(&self, location: Location) -> Point {
        let x = location.x.num_points(self.tile_size());
        let y = location.y.num_points(self.tile_size());
        Point::new(x, y)
    }

    /// Returns the grid location of a point that is relative to the
    /// upper left hand corner of the grid.
    pub fn location_from_point(&self, point: Point) -> Option<Location> {
        Some(point)
            .filter(|point| point.is_positive())
            .map(|point| Location::from_point(point, self.tile_size()))
            .filter(|location| {
                location.x < self.width && location.y < self.height
            })
    }

    pub fn move_to(&mut self, location: Option<layer::Location>) {
        self.layer_location = location;
    }

    pub fn cursor_to(&mut self, location: Option<layer::Location>) {
        self.cursor_location = self
            .layer_location
            .and_then(|center| {
                location.and_then(|cursor| cursor.offset_from(&center))
            })
            .and_then(|offset| self.center().checked_add(offset))
    }

    /// Returns a `Mesh` for the grid that can be drawn.
    /// Does not include the contents of the grid.
    pub fn mesh(&self, ctx: &mut Context, color: Color) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();
        for x in 0..=self.width.into() {
            let top = Location::new(x, 0);
            let bottom = Location::new(x, self.height);
            // Make every 5th line thicker
            let width = if x % 5 == 0 { 2.0 } else { 1.0 };

            mb.line(&[self.point(top), self.point(bottom)], width, color)?;
        }
        for y in 0..=self.height.into() {
            let left = Location::new(0, y);
            let right = Location::new(self.width, y);
            let width = if y % 5 == 0 { 2.0 } else { 1.0 };
            mb.line(&[self.point(left), self.point(right)], width, color)?;
        }
        mb.build(ctx)
    }

    /// Returns the `Location` of the center of the `Grid`.
    pub fn center(&self) -> Location {
        // -1 because we want to prefer centering on locations
        // that are displayed on the screen.  The grid overflows
        // the drawable rect by 1 square so that it can render
        // partial tiles that don't quite fit.
        Location::new((self.width / 2) - 1, (self.height / 2) - 1)
    }
}

impl<'a> IntoIterator for &'a Grid {
    type Item = Location;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        Iter::new(self, Location::new(0, 0))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::client::font::Font;

    #[test]
    fn test_size() {
        let font_size = point::Coord::from(10);
        let padding = 1.5;
        let width = 10;
        let height = 5;
        let grid = Grid::new(
            Rect::new(
                0,
                0,
                (font_size * width).scale_by(padding),
                (font_size * height).scale_by(padding),
            ),
            Font::new(None, None, font_size),
            padding,
        );

        assert_eq!(grid.size(), Rect::new(0, 0, 150, 75));
    }

    #[test]
    fn test_point() {
        let font_size = point::Coord::from(10);
        let padding = 1.5;
        let width = 10;
        let height = 10;
        let grid = Grid::new(
            Rect::new(
                0,
                0,
                (font_size * width).scale_by(padding),
                (font_size * height).scale_by(padding),
            ),
            Font::new(None, None, font_size),
            padding,
        );

        assert_eq!(grid.point(Location::new(0, 0)), Point::new(0, 0));
        assert_eq!(grid.point(Location::new(1, 0)), Point::new(15, 0));
        assert_eq!(grid.point(Location::new(1, 1)), Point::new(15, 15));
        assert_eq!(grid.point(Location::new(2, 2)), Point::new(30, 30));
        assert_eq!(grid.point(Location::new(3, 3)), Point::new(45, 45));
        assert_eq!(grid.point(Location::new(5, 5)), Point::new(75, 75));
    }

    #[test]
    fn test_location() {
        let font_size = point::Coord::from(10);
        let padding = 1.5;
        let width = 9;
        let height = 4;
        let grid = Grid::new(
            Rect::new(
                15,
                15,
                (font_size * width).scale_by(padding),
                (font_size * height).scale_by(padding),
            ),
            Font::new(None, None, font_size),
            padding,
        );

        assert_eq!(
            grid.location_from_point(Point::new(0, 0)),
            Some(Location::new(0, 0))
        );
        assert_eq!(
            grid.location_from_point(Point::new(15, 15)),
            Some(Location::new(1, 1))
        );
        assert_eq!(
            grid.location_from_point(Point::new(30, 30)),
            Some(Location::new(2, 2))
        );
        // Legal size is width/height + 1 in order to allow partial
        // tiles to render on the left/bottom
        assert_eq!(
            grid.location_from_point(Point::new(149, 74)),
            Some(Location::new(9, 4))
        );

        assert_eq!(grid.location_from_point(Point::new(150, 0)), None);

        assert_eq!(grid.location_from_point(Point::new(0, 75)), None);
    }
}
