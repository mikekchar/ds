use super::{
    layer,
    location,
    map::{Map, MapTile},
};

pub type ZoneId = u32;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Zone {
    Impassable,
    NoZone,
    ZoneId(ZoneId),
}

#[derive(Debug)]
pub struct ZoneMap {
    pub width: location::Coord,
    pub height: location::Coord,
    pub zones: Vec<Zone>,
}

impl ZoneMap {
    pub fn new(
        width: impl Into<location::Coord>,
        height: impl Into<location::Coord>,
    ) -> Self {
        let width = width.into();
        let height = height.into();
        let num_tiles = location::coord::Internal::from(width * height);
        let tiles = vec![Zone::NoZone; num_tiles];
        Self {
            width,
            height,
            zones: tiles,
        }
    }

    pub fn index(
        &self,
        location: layer::Location,
    ) -> Option<location::coord::Internal> {
        if location.x < self.width && location.y < self.height {
            Some(location.index(self.width))
        } else {
            None
        }
    }

    /// Returns the map tile for an index into the map.
    /// The index is a breadth first index into the array.
    pub fn get(&self, index: usize) -> Option<Zone> {
        self.zones.get(index).copied()
    }

    pub fn set(&mut self, index: usize, zone: Zone) {
        self.zones[index] = zone;
    }

    pub fn set_tile(&mut self, location: layer::Location, zone: Zone) {
        let l = layer::Location::new(location.x, location.y);
        if let Some(index) = self.index(l) {
            self.set(index, zone);
        }
    }

    pub fn get_tile(&self, location: layer::Location) -> Option<Zone> {
        let l = layer::Location::new(location.x, location.y);
        let index = self.index(l)?;
        self.get(index)
    }

    pub fn len(&self) -> usize {
        self.zones.len()
    }

    pub fn is_empty(&self) -> bool {
        self.zones.is_empty()
    }

    pub fn set_pathability(&mut self, map: &Map) {
        for i in 0..map.len() {
            let pathable = if let Some(tile) = map.get(i) {
                tile == MapTile::Floor
            } else {
                false
            };
            self.zones[i] = if pathable {
                Zone::NoZone
            } else {
                Zone::Impassable
            }
        }
    }
}
