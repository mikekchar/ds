use super::{Grid, Location};

/// The location on a specific grid
#[derive(Debug)]
pub struct Iter<'a> {
    grid: &'a Grid,
    current: Location,
}

impl<'a> Iter<'a> {
    pub fn new(grid: &'a Grid, current: Location) -> Self {
        Iter { grid, current }
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = Location;

    /// Returns the next `Grid` `Location` in breadth-wise order.
    /// If there are no more locations on the grid, returns `None`.
    fn next(&mut self) -> Option<Location> {
        let x = self.current.x;
        let y = self.current.y;

        if y >= self.grid.height || x >= self.grid.width {
            None
        } else {
            // We always want to return the current location now
            // and increment the iterator for next time.  This makes
            // a copy.
            let result = self.current;

            if x + 1 < self.grid.width {
                self.current = Location::new(x + 1, y);
            } else {
                self.current = Location::new(0, y + 1);
            }

            Some(result)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::{
        client::{font::Font, point, rect::Rect},
        location,
    };

    const FONT_SIZE: point::coord::Internal = 10;
    const PADDING: f32 = 1.5;

    struct Setup {
        grid: Grid,
    }

    impl Setup {
        fn new(width: location::Coord, height: location::Coord) -> Self {
            let rect = Rect::new(
                0,
                0,
                width.num_points(
                    point::Coord::from(FONT_SIZE).scale_by(PADDING),
                ),
                height.num_points(
                    point::Coord::from(FONT_SIZE).scale_by(PADDING),
                ),
            );
            let font = Font::new(None, None, FONT_SIZE);
            Setup {
                grid: Grid::zero(rect, font, PADDING),
            }
        }
    }

    #[test]
    fn zero_size_grid() {
        let setup = Setup::new(0.into(), 0.into());
        let mut gl = Iter::new(&setup.grid, Location::new(0, 0));
        assert_eq!(gl.next(), None);
    }

    #[test]
    fn iterating() {
        let setup = Setup::new(2.into(), 3.into());
        let mut gl = Iter::new(&setup.grid, Location::new(0, 0));
        assert_eq!(gl.next(), Some(Location::new(0, 0)));
        assert_eq!(gl.next(), Some(Location::new(1, 0)));
        assert_eq!(gl.next(), Some(Location::new(0, 1)));
        assert_eq!(gl.next(), Some(Location::new(1, 1)));
        assert_eq!(gl.next(), Some(Location::new(0, 2)));
        assert_eq!(gl.next(), Some(Location::new(1, 2)));
        assert_eq!(gl.next(), None);
    }
}
