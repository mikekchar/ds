//! Locations on the Grid

use crate::game::{
    client::point::{self, Point},
    coord::CheckedOps,
    location::{self, Coord, Offset},
};
use std::ops::Deref;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone, Copy)]
pub struct Location(location::Location);

// You can only add Offsets to Locations
// If you want to add or subtract Locations, use
// Offset::lossy_from(my_location)
impl CheckedOps<Offset> for Location {
    /// Try to add an offset to a location.
    /// Returns `None` if the result would overflow.
    fn checked_add(self, other: Offset) -> Option<Self> {
        self.0.checked_add(other).map(Self)
    }

    /// Try to subtract an offset from a location.
    /// Returns `None` if the result would be less than zero.
    fn checked_sub(self, other: Offset) -> Option<Self> {
        self.0.checked_sub(other).map(Self)
    }
}

impl Deref for Location {
    type Target = location::Location;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<Location> for location::Location {
    fn from(x: Location) -> Self {
        x.0
    }
}

impl Location {
    /// Create a `Location` for the `Grid` display
    pub fn new(x: impl Into<Coord>, y: impl Into<Coord>) -> Self {
        Self(location::Location::new(x, y))
    }

    pub fn from_point(point: Point, tile_size: point::Coord) -> Self {
        Location::new(
            location::Coord::num_tiles(point.x(), tile_size),
            location::Coord::num_tiles(point.y(), tile_size),
        )
    }
}
