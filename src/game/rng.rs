//! A wrapper for whatever random number generator I'll be using

use rand::SeedableRng;
use rand_xoshiro::Xoshiro256StarStar;
use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    ops::{Deref, DerefMut},
};

#[derive(Clone, Debug)]
pub struct Rng(Xoshiro256StarStar);

fn hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

fn seed_from_string(seed_string: &str) -> u64 {
    hash(&seed_string.to_string())
}

impl Rng {
    /// Create a new random number generator with a seed
    pub fn new(seed_string: &str) -> Self {
        Rng(Xoshiro256StarStar::seed_from_u64(seed_from_string(
            seed_string,
        )))
    }
}

impl Deref for Rng {
    type Target = Xoshiro256StarStar;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Rng {
    fn deref_mut(&mut self) -> &mut Xoshiro256StarStar {
        &mut self.0
    }
}

#[cfg(test)]
mod tests {
    use rand::Rng as RngTrait;

    use super::*;

    #[test]
    fn hash_seed_string() {
        let seed = seed_from_string("hello");
        assert_eq!(seed, 16156531084128653017);
    }

    #[test]
    fn gen_random_number() {
        let mut rng = Rng::new("hello");
        assert_eq!(rng.gen_range(1..=6), 3);
        assert_eq!(rng.gen_range(1..=6), 4);
        assert_eq!(rng.gen_range(1..=6), 5);
    }
}
