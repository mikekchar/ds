mod candidate_queue;
mod distance_map;
mod node;
mod state;

use super::{
    layer::{self, Layer},
    location,
    trail::{self, Trail},
};
use state::State;

pub fn iter(
    start: layer::Location,
    goal: layer::Location,
    layer: &Layer,
) -> State {
    let mut state = State::new(goal, layer);
    state.start(start);
    state
}

pub fn create_trail(
    start: layer::Location,
    goal: layer::Location,
    layer: &Layer,
) -> Trail {
    let mut i = iter(start, goal, layer);
    while i.next() != None {}
    i.build_trail()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_a_star() {
        let seed = "test_seed";
        let chunk_size = 10;
        let chunks_per_side = 1;
        let mut layer = Layer::new(chunk_size, chunks_per_side, seed);
        layer.map.add_room(layer::Location::new(1, 1), 8, 8);
        assert_eq!(
            create_trail(
                layer::Location::new(1, 1),
                layer::Location::new(1, 1),
                &layer
            )
            .len(),
            0
        );

        assert_eq!(
            create_trail(
                layer::Location::new(1, 1),
                layer::Location::new(1, 2),
                &layer
            )
            .len(),
            1
        );

        assert_eq!(
            create_trail(
                layer::Location::new(1, 1),
                layer::Location::new(2, 3),
                &layer
            )
            .len(),
            2
        );

        assert_eq!(
            create_trail(
                layer::Location::new(1, 1),
                layer::Location::new(8, 8),
                &layer
            )
            .len(),
            7
        );

        // This is in the wall so it should not be pathable
        assert_eq!(
            create_trail(
                layer::Location::new(1, 1),
                layer::Location::new(9, 9),
                &layer
            )
            .len(),
            0
        );
    }
}
