//! Game Events for the game

use crate::game::{dwarf, player::Power};

#[derive(Debug, PartialEq, Eq)]
pub enum Event {
    DistributePower(Power),
    Death(dwarf::Id),
}
