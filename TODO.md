# Chibi Shiro TODO

Most technical TODOs are in the journal directory.
There is one file for each active development branch.

## Current Development Branch
challenge_1 (The water challenge)

## TODO

### TODO The water challenge
    A dwarf appears in the only room.  Over time the dwarf gets
    dehydrated.  When the dwarf is too dehydrated it dies.  We
    have to help it find water.

### TODO The faith challenge
    The dwarf has faith.  The amount of power the player gets depends
    on the amount of faith the dwarf has.  As the dwarf uses the powers
    of the player and gets favourable results, faith goes up.  If the
    the results are not favourable, faith goes down.

### TODO The light challenge
    Water will attract a certain insect which is similar to
    https://en.wikipedia.org/wiki/Arachnocampa_luminosa
    The dwarf must survive long enough for the insect to lay eggs
    in the water.  The eggs will hatch into larva which glow.
    The larva will eventually pupate and make something like:
    https://en.wikipedia.org/wiki/Waitomo_Glowworm_Caves
    If the dwarf does not get light in enough time, it is likely
    to be eaten by a grue!

### TODO The cheese challenge
    The glow worm pupa will drip a glowing "milk".  The dwarf
    must collect the milk.  The milk itself is not edible, but
    the dwarf can make cheese from it and eat the cheese.  If the
    dwarf does not eat in enough time, they will die of starvation.
    Cheese gnomes may come to try to steal the cheese (for profit???)

### TODO The wumpus challenge
    Dwarf can not live on glowing cheese alone!  They must hunt the
    wumpus.  They can do this by listening for it.  To protect themselves
    from grues (which are practically invincible in this stage of the
    game), they must take glowing cheeses for light.  The light will
    scare off the grues.  They can do battle with a wumpus and then take
    it back to their lair and butcher it to produce skin, bones and meat.


